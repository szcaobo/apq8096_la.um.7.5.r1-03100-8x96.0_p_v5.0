/* Copyright (c) 2014-2018 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, nit
 * PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <debug.h>
#include <platform/iomap.h>
#include <platform/irqs.h>
#include <platform/gpio.h>
#include <reg.h>
#include <target.h>
#include <platform.h>
#include <dload_util.h>
#include <uart_dm.h>
#include <mmc.h>
#include <spmi.h>
#include <board.h>
#include <smem.h>
#include <baseband.h>
#include <regulator.h>
#include <dev/keys.h>
#include <pm8x41.h>
#include <pm8x41_hw.h>
#include <crypto5_wrapper.h>
#include <clock.h>
#include <partition_parser.h>
#include <scm.h>
#include <platform/clock.h>
#include <platform/gpio.h>
#include <platform/timer.h>
#include <stdlib.h>
#include <ufs.h>
#include <boot_device.h>
#include <qmp_phy.h>
#include <sdhci_msm.h>
#include <qusb2_phy.h>
#include <secapp_loader.h>
#include <rpmb.h>
#include <rpm-glink.h>
#if ENABLE_WBC
#include <pm_app_smbchg.h>
#endif

#if LONG_PRESS_POWER_ON
#include <shutdown_detect.h>
#endif

#if PON_VIB_SUPPORT
#include <vibrator.h>
#define VIBRATE_TIME 250
#endif

#include <pm_smbchg_usb_chgpth.h>

#if MOUNT_EMMC_LE
       #define ROOTFS_EMMC_PATH " root=/dev/mmcblk0p"
#else
       #define ROOTFS_EMMC_PATH " root=/dev/mmcblock0p"
#endif

#define CE_INSTANCE             1
#define CE_EE                   0
#define CE_FIFO_SIZE            64
#define CE_READ_PIPE            3
#define CE_WRITE_PIPE           2
#define CE_READ_PIPE_LOCK_GRP   0
#define CE_WRITE_PIPE_LOCK_GRP  0
#define CE_ARRAY_SIZE           20

#define PMIC_ARB_CHANNEL_NUM    0
#define PMIC_ARB_OWNER_ID       0

#define SMBCHG_USB_RT_STS 0x21310
#define SMBCHG_DC_RT_STS 0x21410
#define USBIN_UV_RT_STS BIT(0)
#define USBIN_OV_RT_STS BIT(1)
#define DCIN_UV_RT_STS  BIT(0)
#define DCIN_OV_RT_STS  BIT(1)

enum
{
	FUSION_I2S_MTP = 1,
	FUSION_SLIMBUS = 2,
} mtp_subtype;

enum
{
	FUSION_I2S_CDP = 2,
} cdp_subtype;

static uint8_t flash_memory_slot = 0;
static void set_sdc_power_ctrl();
static uint32_t mmc_pwrctl_base[] =
	{ MSM_SDC1_BASE, MSM_SDC2_BASE };

static uint32_t mmc_sdhci_base[] =
	{ MSM_SDC1_SDHCI_BASE, MSM_SDC2_SDHCI_BASE };

static uint32_t  mmc_sdc_pwrctl_irq[] =
	{ SDCC1_PWRCTL_IRQ, SDCC2_PWRCTL_IRQ };

struct mmc_device *dev;
struct ufs_dev ufs_device;

/* For board info store in boot partition */
#define MAGIC_NO_KEY   "!!Intrinsyc!!"
#define MAGIC_NO_LEN   13
#define SERIAL_NO_LEN  24
#define MAC_ADDR_LEN   20
#define SIG_INFO_LEN   16

struct  __attribute__ ((packed)) cpuboard_info {
	char magic_no[MAGIC_NO_LEN];
	char serial_no[SERIAL_NO_LEN];
	char mac_addr[MAC_ADDR_LEN];
	char timestamp[28];
	char version;
	unsigned int crc32;
};

struct  __attribute__ ((packed)) sig_info {
       char magic_no[MAGIC_NO_LEN];
       char siginfo[SIG_INFO_LEN];
       char timestamp[28];
       char version;
       unsigned int crc32;
};


struct itc_boardinfo {
	unsigned char serial_no[SERIAL_NO_LEN+1];
	unsigned char mac_addr[MAC_ADDR_LEN];
};

struct itc_boardinfo boardinfo;
static unsigned char siginfo[SIG_INFO_LEN+1];

const uint32_t crc32_table[256] = {
       0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L,
       0x706af48fL, 0xe963a535L, 0x9e6495a3L, 0x0edb8832L, 0x79dcb8a4L,
       0xe0d5e91eL, 0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L,
       0x90bf1d91L, 0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
       0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L, 0x136c9856L,
       0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL, 0x63066cd9L,
       0xfa0f3d63L, 0x8d080df5L, 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L,
       0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
       0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L,
       0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L, 0x26d930acL, 0x51de003aL,
       0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L,
       0xb8bda50fL, 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
       0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL, 0x76dc4190L,
       0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL,
       0x9fbfe4a5L, 0xe8b8d433L, 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL,
       0xe10e9818L, 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
       0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL, 0x6c0695edL,
       0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L, 0x65b0d9c6L, 0x12b7e950L,
       0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L,
       0xfbd44c65L, 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
       0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL, 0x4369e96aL,
       0x346ed9fcL, 0xad678846L, 0xda60b8d0L, 0x44042d73L, 0x33031de5L,
       0xaa0a4c5fL, 0xdd0d7cc9L, 0x5005713cL, 0x270241aaL, 0xbe0b1010L,
       0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
       0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L,
       0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL, 0xedb88320L, 0x9abfb3b6L,
       0x03b6e20cL, 0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L,
       0x73dc1683L, 0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
       0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L, 0xf00f9344L,
       0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL, 0x806567cbL,
       0x196c3671L, 0x6e6b06e7L, 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL,
       0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
       0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L,
       0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL, 0xd80d2bdaL, 0xaf0a1b4cL,
       0x36034af6L, 0x41047a60L, 0xdf60efc3L, 0xa867df55L, 0x316e8eefL,
       0x4669be79L, 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
       0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL, 0xc5ba3bbeL,
       0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L,
       0x2cd99e8bL, 0x5bdeae1dL, 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL,
       0x026d930aL, 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
       0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L, 0x92d28e9bL,
       0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L, 0x86d3d2d4L, 0xf1d4e242L,
       0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L,
       0x18b74777L, 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
       0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L, 0xa00ae278L,
       0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L, 0xa7672661L, 0xd06016f7L,
       0x4969474dL, 0x3e6e77dbL, 0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L,
       0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
       0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L,
       0xcdd70693L, 0x54de5729L, 0x23d967bfL, 0xb3667a2eL, 0xc4614ab8L,
       0x5d681b02L, 0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL,
       0x2d02ef8dL
};


/* Return a 32-bit CRC of the contents of the buffer. */
static uint32_t crc32(uint32_t val, const void *ss, int len)
{
       const unsigned char *s = ss;
       while (--len >= 0)
               val = crc32_table[(val ^ *s++) & 0xff] ^ (val >> 8);
       return val;
}

static unsigned char *get_board_serialno(void)
{
       return (unsigned char *)boardinfo.serial_no;
}

static unsigned char *get_board_wifi_mac(void)
{
       return (unsigned char *)boardinfo.mac_addr;
}

static unsigned char *get_sig_info(void)
{
       return (unsigned char *)siginfo;
}

/*
 * Function to read the itc partition parameter
 */
static unsigned int read_itc_part_info(void)
{
       int i, j = 0;
       struct cpuboard_info *cinfo;
       uint32_t *buffer;
       int index = INVALID_PTN;
       unsigned long long ptn = 0;

       memset(&boardinfo, 0x00, sizeof(struct itc_boardinfo));

       index = partition_get_index("itc");
       if (index == INVALID_PTN) {
               dprintf(INFO, "Unable to locate itc partition\n");
               return -1;
       }
       ptn = partition_get_offset(index);
       if(!ptn) {
               dprintf(INFO, "partition itc doesn't exist\n");
               return -1;
       }
       mmc_set_lun(partition_get_lun(index));

       buffer = memalign(CACHE_LINE, ROUNDUP(ptn, CACHE_LINE));

       if (mmc_read(ptn, buffer, ptn)) {
               dprintf(INFO, "mmc read failure itc part\n");
	       return -1;
       }

       cinfo = (struct cpuboard_info *)&buffer[0];

       /* validate the MAGIC NO if corrupted */
       if (memcmp(cinfo->magic_no, MAGIC_NO_KEY ,MAGIC_NO_LEN) != 0) {
               dprintf(INFO, "MAGIC NO mismatched\n");
               return -1;
       }
       /* Now check CRC */
       if(crc32(0, cinfo, sizeof(struct cpuboard_info) - sizeof(unsigned int)) != cinfo->crc32) {
               dprintf(INFO, "CRC mismatch\n");
               return -1;
       }

       for (i = 0, j = 0; i < SERIAL_NO_LEN; i++) {
               // Filter out numeric
               if ((cinfo->serial_no[i] >= 0x30 && cinfo->serial_no[i] <=0x39) ||
               (cinfo->serial_no[i] >= 0x41 && cinfo->serial_no[i] <= 0x46) ||
               (cinfo->serial_no[i] >= 0x61 && cinfo->serial_no[i] <= 0x66))
                       boardinfo.serial_no[j++] = cinfo->serial_no[i];
       }
       boardinfo.serial_no[j] = 0x00;
       dprintf(INFO, "Serial No: %s\n", boardinfo.serial_no);
       for (i = 0, j = 0; i < MAC_ADDR_LEN; i++) {
               // Filter out numeric
               if ((cinfo->mac_addr[i] >= 0x30 && cinfo->mac_addr[i] <=0x39) ||
               (cinfo->mac_addr[i] >= 0x41 && cinfo->mac_addr[i] <= 0x46) ||
               (cinfo->mac_addr[i] >= 0x61 && cinfo->mac_addr[i] <= 0x66))
                       boardinfo.mac_addr[j++] = cinfo->mac_addr[i];
       }
       boardinfo.mac_addr[j] = 0x00;
       dprintf(INFO, "MAC Address: %s\n", boardinfo.mac_addr);

       return 0;
}

static unsigned int read_sig_info(void)
{
       int i, j = 0;
       struct sig_info *pSig_info;
       uint32_t *buffer;
       int index = INVALID_PTN;
       unsigned long long ptn = 0;

       memset(&siginfo, 0x00, sizeof(siginfo));

       index = partition_get_index("reserve");
       if (index == INVALID_PTN) {
               dprintf(INFO, "Unable to locate reserve partition\n");
               return -1;
       }
       ptn = partition_get_offset(index);
       if(!ptn) {
               dprintf(INFO, "partition reserve doesn't exist\n");
               return -1;
       }
       mmc_set_lun(partition_get_lun(index));

       buffer = memalign(CACHE_LINE, ROUNDUP(ptn, CACHE_LINE));

       if (mmc_read(ptn, buffer, ptn)) {
               dprintf(INFO, "mmc read failure reserve part\n");
               return -1;
       }

       pSig_info = (struct sig_info *)&buffer[0];

       /* validate the siginfo is available */
       if (memcmp(pSig_info->magic_no, MAGIC_NO_KEY ,MAGIC_NO_LEN) != 0) {
               return -1;
       }
       /* Now check CRC */
       if(crc32(0, pSig_info, sizeof(struct sig_info) - sizeof(unsigned int)) != pSig_info->crc32) {
               dprintf(INFO, "CRC mismatch for siginfo\n");
               return -1;
       }

       for (i = 0, j = 0; i < SIG_INFO_LEN; i++) {
               // Filter out numeric
               if ((pSig_info->siginfo[i] >= 0x30 && pSig_info->siginfo[i] <=0x39) ||
               (pSig_info->siginfo[i] >= 0x41 && pSig_info->siginfo[i] <= 0x46) ||
               (pSig_info->siginfo[i] >= 0x61 && pSig_info->siginfo[i] <= 0x66))
                       siginfo[j++] = pSig_info->siginfo[i];
       }
       siginfo[j] = 0x00;

       return 0;
}

void target_early_init(void)
{
#if WITH_DEBUG_UART
	uart_dm_init(8, 0, BLSP2_UART1_BASE);
#endif
}

/* Return 1 if vol_up pressed */
int target_volume_up()
{
	static uint8_t first_time = 0;
	uint8_t status = 0;
	struct pm8x41_gpio gpio;

	if (!first_time) {
		/* Configure the GPIO */
		gpio.direction = PM_GPIO_DIR_IN;
		gpio.function  = 0;
		gpio.pull      = PM_GPIO_PULL_UP_30;
		gpio.vin_sel   = 2;

		pm8x41_gpio_config(2, &gpio);

		/* Wait for the pmic gpio config to take effect */
		udelay(10000);

		first_time = 1;
	}

	/* Get status of P_GPIO_5 */
	pm8x41_gpio_get(2, &status);

	return !status; /* active low */
}

/* Return 1 if vol_down pressed */
uint32_t target_volume_down()
{
	return pm8x41_resin_status();
}

static void target_keystatus()
{
	keys_init();

	if(target_volume_down())
		keys_post_event(KEY_VOLUMEDOWN, 1);

	if(target_volume_up())
		keys_post_event(KEY_VOLUMEUP, 1);
}

void target_uninit(void)
{
	if (platform_boot_dev_isemmc())
	{
		mmc_put_card_to_sleep(dev);
	}

#if VERIFIED_BOOT
	if (target_get_vb_version() >= VB_M &&
		is_sec_app_loaded())
	{
		if (send_milestone_call_to_tz() < 0)
		{
			dprintf(CRITICAL, "Failed to unload App for rpmb\n");
			ASSERT(0);
		}
	}
#endif

#if ENABLE_WBC
	if (board_hardware_id() == HW_PLATFORM_MTP)
		pm_appsbl_set_dcin_suspend(1);
#endif


	if (crypto_initialized())
	{
		crypto_eng_cleanup();
		clock_ce_disable(CE_INSTANCE);
	}

	/* Tear down glink channels */
	rpm_glink_uninit();

#if VERIFIED_BOOT
	if (target_get_vb_version() >= VB_M)
	{
		if (rpmb_uninit() < 0)
		{
			dprintf(CRITICAL, "RPMB uninit failed\n");
			ASSERT(0);
		}
	}
#endif

}

static void set_sdc_power_ctrl()
{
	uint32_t reg = 0;
	uint8_t clk = 0;
	uint8_t cmd = 0;
	uint8_t dat = 0;

	if (flash_memory_slot == 0x1)
	{
		clk = TLMM_CUR_VAL_10MA;
		cmd = TLMM_CUR_VAL_8MA;
		dat = TLMM_CUR_VAL_8MA;
		reg = SDC1_HDRV_PULL_CTL;
	}
	else if (flash_memory_slot == 0x2)
	{
		clk = TLMM_CUR_VAL_16MA;
		cmd = TLMM_CUR_VAL_10MA;
		dat = TLMM_CUR_VAL_10MA;
		reg = SDC2_HDRV_PULL_CTL;
	}

	/* Drive strength configs for sdc pins */
	struct tlmm_cfgs sdc1_hdrv_cfg[] =
	{
		{ SDC1_CLK_HDRV_CTL_OFF,  clk, TLMM_HDRV_MASK, reg },
		{ SDC1_CMD_HDRV_CTL_OFF,  cmd, TLMM_HDRV_MASK, reg },
		{ SDC1_DATA_HDRV_CTL_OFF, dat, TLMM_HDRV_MASK, reg },
	};

	/* Pull configs for sdc pins */
	struct tlmm_cfgs sdc1_pull_cfg[] =
	{
		{ SDC1_CLK_PULL_CTL_OFF,  TLMM_NO_PULL, TLMM_PULL_MASK, reg },
		{ SDC1_CMD_PULL_CTL_OFF,  TLMM_PULL_UP, TLMM_PULL_MASK, reg },
		{ SDC1_DATA_PULL_CTL_OFF, TLMM_PULL_UP, TLMM_PULL_MASK, reg },
	};

	struct tlmm_cfgs sdc1_rclk_cfg[] =
	{
		{ SDC1_RCLK_PULL_CTL_OFF, TLMM_PULL_DOWN, TLMM_PULL_MASK, reg },
	};

	/* Set the drive strength & pull control values */
	tlmm_set_hdrive_ctrl(sdc1_hdrv_cfg, ARRAY_SIZE(sdc1_hdrv_cfg));
	tlmm_set_pull_ctrl(sdc1_pull_cfg, ARRAY_SIZE(sdc1_pull_cfg));
	tlmm_set_pull_ctrl(sdc1_rclk_cfg, ARRAY_SIZE(sdc1_rclk_cfg));
}

uint32_t target_is_pwrkey_pon_reason()
{
	uint8_t pon_reason = pm8950_get_pon_reason();

	if (pm8x41_get_is_cold_boot() && ((pon_reason == KPDPWR_N) || (pon_reason == (KPDPWR_N|PON1))))
		return 1;
	else if (pon_reason == PON1)
	{
		/* DC charger is present or USB charger is present */
		if (((USBIN_UV_RT_STS | USBIN_OV_RT_STS) & pm8x41_reg_read(SMBCHG_USB_RT_STS)) == 0 ||
			((DCIN_UV_RT_STS | DCIN_OV_RT_STS) & pm8x41_reg_read(SMBCHG_DC_RT_STS)) == 0)
			return 0;
		else
			return 1;
	}
	else
		return 0;
}


void target_sdc_init()
{
	struct mmc_config_data config = {0};

	config.bus_width = DATA_BUS_WIDTH_8BIT;
	config.max_clk_rate = MMC_CLK_192MHZ;
	config.hs400_support = 1;

	/* Try slot 1*/
	flash_memory_slot = 1;
	config.slot = 1;
	config.sdhc_base = mmc_sdhci_base[config.slot - 1];
	config.pwrctl_base = mmc_pwrctl_base[config.slot - 1];
	config.pwr_irq     = mmc_sdc_pwrctl_irq[config.slot - 1];

	/* Set drive strength & pull ctrl values */
	set_sdc_power_ctrl();

	if (!(dev = mmc_init(&config)))
	{
		/* Try slot 2 */
		flash_memory_slot = 2;
		config.slot = 2;
		config.max_clk_rate = MMC_CLK_200MHZ;
		config.sdhc_base = mmc_sdhci_base[config.slot - 1];
		config.pwrctl_base = mmc_pwrctl_base[config.slot - 1];
		config.pwr_irq     = mmc_sdc_pwrctl_irq[config.slot - 1];

		/* Set drive strength & pull ctrl values */
		set_sdc_power_ctrl();

		if (!(dev = mmc_init(&config)))
		{
			dprintf(CRITICAL, "mmc init failed!");
			ASSERT(0);
		}
	}
}

void *target_mmc_device()
{
	if (platform_boot_dev_isemmc())
		return (void *) dev;
	else
		return (void *) &ufs_device;
}

void target_init(void)
{
	dprintf(INFO, "target_init()\n");

	pmic_info_populate();

	spmi_init(PMIC_ARB_CHANNEL_NUM, PMIC_ARB_OWNER_ID);

	/* Initialize Glink */
	rpm_glink_init();

	target_keystatus();

#if defined(LONG_PRESS_POWER_ON) || defined(PON_VIB_SUPPORT)
	switch(board_hardware_id())
	{
		case HW_PLATFORM_QRD:
#if LONG_PRESS_POWER_ON
			shutdown_detect();
#endif
#if PON_VIB_SUPPORT
			vib_timed_turn_on(VIBRATE_TIME);
#endif
			break;
	}
#endif

	if (target_use_signed_kernel())
		target_crypto_init_params();

	platform_read_boot_config();

#ifdef MMC_SDHCI_SUPPORT
	if (platform_boot_dev_isemmc())
	{
		target_sdc_init();
	}
#endif
#ifdef UFS_SUPPORT
	if (!platform_boot_dev_isemmc())
	{
		ufs_device.base = UFS_BASE;
		ufs_init(&ufs_device);
	}
#endif

	/* Storage initialization is complete, read the partition table info */
	mmc_read_partition_table(0);

	read_itc_part_info();

	read_sig_info();

#if ENABLE_WBC
	/* Look for battery voltage and make sure we have enough to bootup
	 * Otherwise initiate battery charging
	 * Charging should happen as early as possible, any other driver
	 * initialization before this should consider the power impact
	 */
	switch(board_hardware_id())
	{
		case HW_PLATFORM_MTP:
		case HW_PLATFORM_FLUID:
		case HW_PLATFORM_QRD:
			if(target_is_pmi_enabled())
				pm_appsbl_chg_check_weak_battery_status(1);
			break;
		default:
			/* Charging not supported */
			break;
	};
#endif

#if VERIFIED_BOOT
	if (VB_M <= target_get_vb_version())
	{
		/* Initialize Qseecom */
		if (qseecom_init() < 0)
		{
			dprintf(CRITICAL, "Failed to initialize qseecom\n");
			ASSERT(0);
		}

		/* Start Qseecom */
		if (qseecom_tz_init() < 0)
		{
			dprintf(CRITICAL, "Failed to start qseecom\n");
			ASSERT(0);
		}

		if (rpmb_init() < 0)
		{
			dprintf(CRITICAL, "RPMB init failed\n");
			ASSERT(0);
		}

		/*
		 * Load the sec app for first time
		 */
		if (load_sec_app() < 0)
		{
			dprintf(CRITICAL, "Failed to load App for verified\n");
			ASSERT(0);
		}
	}
#endif
}

unsigned board_machtype(void)
{
	return LINUX_MACHTYPE_UNKNOWN;
}

/* Detect the target type */
void target_detect(struct board_data *board)
{
	/* This is filled from board.c */
}

static uint8_t splash_override;
/* Returns 1 if target supports continuous splash screen. */
int target_cont_splash_screen()
{
	uint8_t splash_screen = 0;
	if(!splash_override && !pm_appsbl_charging_in_progress()) {
		switch(board_hardware_id())
		{
			case HW_PLATFORM_SURF:
			case HW_PLATFORM_MTP:
			case HW_PLATFORM_FLUID:
			case HW_PLATFORM_QRD:
			case HW_PLATFORM_LIQUID:
			case HW_PLATFORM_DRAGON:
			case HW_PLATFORM_ADP:
				dprintf(SPEW, "Target_cont_splash=1\n");
				splash_screen = 1;
				break;
			default:
				dprintf(SPEW, "Target_cont_splash=0\n");
				splash_screen = 0;
		}
	}
	return splash_screen;
}

void target_force_cont_splash_disable(uint8_t override)
{
        splash_override = override;
}

/* Detect the modem type */
void target_baseband_detect(struct board_data *board)
{
	uint32_t platform;
	uint32_t platform_hardware;
	uint32_t platform_subtype;

	platform = board->platform;
	platform_hardware = board->platform_hw;
	platform_subtype = board->platform_subtype;

	if (platform_hardware == HW_PLATFORM_SURF)
	{
		if (platform_subtype == FUSION_I2S_CDP)
			board->baseband = BASEBAND_MDM;
	}
	else if (platform_hardware == HW_PLATFORM_MTP)
	{
		if (platform_subtype == FUSION_I2S_MTP ||
			platform_subtype == FUSION_SLIMBUS)
			board->baseband = BASEBAND_MDM;
	}
	/*
	 * Special case if MDM is not set look for chip info to decide
	 * platform subtype
	 */
	if (board->baseband != BASEBAND_MDM)
	{
		switch(platform) {
		case APQ8096:
		case APQ8096AU:
		case APQ8096SG:
			board->baseband = BASEBAND_APQ;
			break;
		case MSM8996:
		case MSM8996SG:
		case MSM8996AU:
		case MSM8996L:
			board->baseband = BASEBAND_MSM;
			break;
		default:
			dprintf(CRITICAL, "Platform type: %u is not supported\n",platform);
			ASSERT(0);
		};
	}
}

unsigned target_baseband()
{
	return board_baseband();
}

void target_serialno(unsigned char *buf)
{
       unsigned char *tmp;

       tmp = get_board_serialno();
       if (tmp != NULL)
       {
               memcpy(buf, tmp, strlen((char *)tmp));
               *(buf+strlen((char *)tmp)) = 0x00;
       }

}

unsigned char *target_wifi_mac(unsigned char *buf)
{
       unsigned char *tmp;
       tmp = get_board_wifi_mac();
       if (tmp != NULL) {
               memcpy(buf, tmp, strlen((char *)tmp));
               *(buf+strlen((char *)tmp)) = 0x00;
               return buf;
        }
       return NULL;
}

unsigned char *target_sig_info(unsigned char *buf)
{
       unsigned char *tmp;
       tmp = get_sig_info();
       if (tmp != NULL) {
               memcpy(buf, tmp, strlen((char *)tmp));
               *(buf+strlen((char *)tmp)) = 0x00;
               return buf;
       }
       return NULL;
}

bool target_is_correct_itc(void *data)
{
       return (memcmp((void *)data, MAGIC_NO_KEY, MAGIC_NO_LEN) ? false : true);
}


int emmc_recovery_init(void)
{
	return _emmc_recovery_init();
}

void target_usb_phy_reset()
{
	usb30_qmp_phy_reset();
	qusb2_phy_reset();
}

void target_usb_phy_sec_reset()
{
	qusb2_phy_reset();
}

target_usb_iface_t* target_usb30_init()
{
	target_usb_iface_t *t_usb_iface;

	t_usb_iface = calloc(1, sizeof(target_usb_iface_t));
	ASSERT(t_usb_iface);


	/* for SBC we use secondary port */
	if (board_hardware_id() == HW_PLATFORM_SBC)
	{
		/* secondary port have no QMP phy,use only QUSB2 phy that have only reset */
		t_usb_iface->phy_init   = NULL;
		t_usb_iface->phy_reset  = target_usb_phy_sec_reset;
		t_usb_iface->clock_init = clock_usb20_init;
	} else {
		if(platform_is_apq8096_microsom820()) {
			t_usb_iface->phy_init   = NULL;
	                t_usb_iface->phy_reset  = target_usb_phy_sec_reset;
		} else {
			t_usb_iface->phy_init   = usb30_qmp_phy_init;
			t_usb_iface->phy_reset  = target_usb_phy_reset;
		}
		t_usb_iface->clock_init = clock_usb30_init;
	}

	t_usb_iface->vbus_override = 1;

	return t_usb_iface;
}

/* identify the usb controller to be used for the target */
const char * target_usb_controller()
{
	return "dwc";
}

uint32_t target_override_pll()
{
	if (board_soc_version() >= 0x20000)
		return 0;
	else
		return 1;
}

crypto_engine_type board_ce_type(void)
{
	return CRYPTO_ENGINE_TYPE_HW;
}

/* Set up params for h/w CE. */
void target_crypto_init_params()
{
	struct crypto_init_params ce_params;

	/* Set up base addresses and instance. */
	ce_params.crypto_instance  = CE_INSTANCE;
	ce_params.crypto_base      = MSM_CE_BASE;
	ce_params.bam_base         = MSM_CE_BAM_BASE;

	/* Set up BAM config. */
	ce_params.bam_ee               = CE_EE;
	ce_params.pipes.read_pipe      = CE_READ_PIPE;
	ce_params.pipes.write_pipe     = CE_WRITE_PIPE;
	ce_params.pipes.read_pipe_grp  = CE_READ_PIPE_LOCK_GRP;
	ce_params.pipes.write_pipe_grp = CE_WRITE_PIPE_LOCK_GRP;

	/* Assign buffer sizes. */
	ce_params.num_ce           = CE_ARRAY_SIZE;
	ce_params.read_fifo_size   = CE_FIFO_SIZE;
	ce_params.write_fifo_size  = CE_FIFO_SIZE;

	/* BAM is initialized by TZ for this platform.
	 * Do not do it again as the initialization address space
	 * is locked.
	 */
	ce_params.do_bam_init      = 0;

	crypto_init_params(&ce_params);
}

unsigned target_pause_for_battery_charge(void)
{
	uint8_t pon_reason = pm8x41_get_pon_reason();
	uint8_t is_cold_boot = pm8x41_get_is_cold_boot();
	pm_smbchg_usb_chgpth_pwr_pth_type charger_path = PM_SMBCHG_USB_CHGPTH_PWR_PATH__INVALID;
	dprintf(INFO, "%s : pon_reason is %d cold_boot:%d charger path: %d\n", __func__,
		pon_reason, is_cold_boot, charger_path);
	/* In case of fastboot reboot,adb reboot or if we see the power key
	* pressed we do not want go into charger mode.
	* fastboot reboot is warm boot with PON hard reset bit not set
	* adb reboot is a cold boot with PON hard reset bit set
	*/
	pm_smbchg_get_charger_path(1, &charger_path);
	if (is_cold_boot &&
			(!(pon_reason & HARD_RST)) &&
			(!(pon_reason & KPDPWR_N)) &&
			((pon_reason & PON1)) &&
			((charger_path == PM_SMBCHG_USB_CHGPTH_PWR_PATH__DC_CHARGER) ||
			(charger_path == PM_SMBCHG_USB_CHGPTH_PWR_PATH__USB_CHARGER)))

		return 1;
	else
		return 0;
}

int set_download_mode(enum reboot_reason mode)
{
	int ret = 0;
	ret = scm_dload_mode(mode);

	return ret;
}

void pmic_reset_configure(uint8_t reset_type)
{
	uint8_t sec_pmic_reset_type = reset_type;

	/* use shutdown for non-core pmic's on hard_reset */
	if (reset_type == PON_PSHOLD_HARD_RESET)
		sec_pmic_reset_type = PON_PSHOLD_SHUTDOWN;

	/* Confiure primary pmic PM8996 */
	pm8996_reset_configure(0, reset_type);

	/* Confiure secondary pmic PMI8996 */
	pm8996_reset_configure(2, sec_pmic_reset_type);

	/* Check if third pmic PM8004 present */
	if ((board_pmic_target(2) & 0xff) == 0xC)
	{
		/* Confiure PM8004 */
		pm8996_reset_configure(4, reset_type);

		/* Check if fourth pmic PMK8001 present */
		if ((board_pmic_target(3) & 0xff) == 0x12)
			pm8996_reset_configure(6, sec_pmic_reset_type);
	}
	else if ((board_pmic_target(2) & 0xff) == 0x12)
	{
		/* check and configure if third pmic is PMK8001 */
		pm8996_reset_configure(6, sec_pmic_reset_type);
	}
}

uint32_t target_get_pmic()
{
	return PMIC_IS_PMI8996;
}

int target_update_cmdline(char *cmdline)
{
	uint32_t platform_id = board_platform_id();
	int len = 0;
	if (platform_id == APQ8096SG || platform_id == MSM8996SG)
	{
		strlcpy(cmdline, " fpsimd.fpsimd_settings=0", TARGET_MAX_CMDLNBUF);
		len = strlen (cmdline);

		/* App settings are not required for other than v1.0 SoC */
		if (board_soc_version() > 0x10000) {
			strlcpy(cmdline + len, " app_setting.use_app_setting=0", TARGET_MAX_CMDLNBUF - len);
			len = strlen (cmdline);
		}
	}

	return len;
}

#if _APPEND_CMDLINE
int get_target_boot_params(const char *cmdline, const char *part, char **buf)
{
	int system_ptn_index = -1;
	unsigned int lun = 0;
	char lun_char_base = 'a', lun_char_limit = 'h';

	/*allocate buflen for largest possible string*/
	uint32_t buflen = strlen(ROOTFS_EMMC_PATH) + sizeof(int) + 1; /*1 character for null termination*/

	if (!cmdline || !part ) {
		dprintf(CRITICAL, "WARN: Invalid input param\n");
		return -1;
	}

	system_ptn_index = partition_get_index(part);
	if (system_ptn_index == -1)
	{
		dprintf(CRITICAL,"Unable to find partition %s\n",part);
		return -1;
	}

	*buf = (char *)malloc(buflen);
	if(!(*buf)) {
		dprintf(CRITICAL,"Unable to allocate memory for boot params\n");
		return -1;
	}

	/*
	 * check if cmdline contains "root="/"" at the beginning of buffer or
	 * " root="/"ubi.mtd" in the middle of buffer.
	 */
	if ((strncmp(cmdline," root=",strlen(" root=")) == 0) ||
		strstr(cmdline, " root="))
		dprintf(DEBUG, "DEBUG: cmdline has root=\n");
	else
	{
		if (platform_boot_dev_isemmc()) {
			snprintf(*buf, buflen, ROOTFS_EMMC_PATH"%d",
					system_ptn_index + 1);
		} else {
			lun = partition_get_lun(system_ptn_index);
			if ((lun_char_base + lun) > lun_char_limit) {
				dprintf(CRITICAL, "lun value exceeds limit\n");
				return -1;
			}
			snprintf(*buf, buflen, " root=/dev/sd%c%d",
					lun_char_base + lun,
					partition_get_index_in_lun(part, lun));
		}
	}
	/*in success case buf will be freed in the calling function of this*/
	return 0;
}
#endif
