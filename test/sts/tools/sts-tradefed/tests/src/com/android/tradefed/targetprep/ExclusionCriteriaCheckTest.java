package com.android.tradefed.targetprep;

import com.android.tradefed.build.DeviceBuildInfo;
import com.android.tradefed.config.OptionSetter;
import com.android.tradefed.device.ITestDevice;

import junit.framework.TestCase;

import org.easymock.EasyMock;

public class ExclusionCriteriaCheckTest extends TestCase {
    private static final String OPTION_PROPERTY_NAME = "property-name";
    private static final String OPTION_EXCLUSION_VALUE = "exclusion-value";
    private static final String OPTION_THROW_ERROR = "throw-error";
    private static final String PROPERTY_NAME = "ro.mock.property";

    public ExclusionCriteriaCheck mExclusionCriteriaCheck;
    private ITestDevice mMockDevice;
    private DeviceBuildInfo mMockBuildInfo;
    private OptionSetter mOptionSetter;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mExclusionCriteriaCheck = new ExclusionCriteriaCheck();
        mMockDevice = EasyMock.createMock(ITestDevice.class);
        mMockBuildInfo = new DeviceBuildInfo("0", "", "");
        mOptionSetter = new OptionSetter(mExclusionCriteriaCheck);
        EasyMock.expect(mMockDevice.getSerialNumber()).andReturn("12345").anyTimes();
    }

    public void testPreparer_whenDeviceMeetsExclusionCriteria_throwsException() throws Exception {
        String dummyValue = "dummyValue";
        mOptionSetter.setOptionValue(OPTION_PROPERTY_NAME, PROPERTY_NAME);
        mOptionSetter.setOptionValue(OPTION_EXCLUSION_VALUE, dummyValue);
        EasyMock.expect(mMockDevice.getProperty(PROPERTY_NAME)).andReturn(dummyValue).times(1);
        EasyMock.replay(mMockDevice);

        try {
            mExclusionCriteriaCheck.run(mMockDevice, mMockBuildInfo);
            fail("Target Setup Error expected");
        } catch (TargetSetupError e) {
            // expected
        }
    }

    public void testPreparer_whenExclusionCriteriaNotMetAndInDebugMode_NoErrorThrown() throws Exception {
        String dummyValue = "dummyValue";
        mOptionSetter.setOptionValue(OPTION_PROPERTY_NAME, PROPERTY_NAME);
        mOptionSetter.setOptionValue(OPTION_EXCLUSION_VALUE, dummyValue);
        mOptionSetter.setOptionValue(OPTION_THROW_ERROR, "false");
        EasyMock.expect(mMockDevice.getProperty(PROPERTY_NAME)).andReturn(dummyValue).times(1);
        EasyMock.replay(mMockDevice);

        mExclusionCriteriaCheck.run(mMockDevice, mMockBuildInfo);
    }

    public void testPreparer_whenExclusionCriteriaNotMet_passesChecks() throws Exception {
        mOptionSetter.setOptionValue(OPTION_PROPERTY_NAME, PROPERTY_NAME);
        mOptionSetter.setOptionValue(OPTION_EXCLUSION_VALUE, "value1");
        EasyMock.expect(mMockDevice.getProperty(PROPERTY_NAME)).andReturn("value2").times(1);
        EasyMock.replay(mMockDevice);

        mExclusionCriteriaCheck.run(mMockDevice, mMockBuildInfo);
    }

    public void testPreparer_whenExclusionCriteriaUnavailable_passesChecks() throws Exception {
        mOptionSetter.setOptionValue(OPTION_PROPERTY_NAME, PROPERTY_NAME);
        mOptionSetter.setOptionValue(OPTION_EXCLUSION_VALUE, "value1");
        EasyMock.expect(mMockDevice.getProperty(PROPERTY_NAME)).andReturn(null).times(1);
        EasyMock.replay(mMockDevice);

        mExclusionCriteriaCheck.run(mMockDevice, mMockBuildInfo);
    }

}
