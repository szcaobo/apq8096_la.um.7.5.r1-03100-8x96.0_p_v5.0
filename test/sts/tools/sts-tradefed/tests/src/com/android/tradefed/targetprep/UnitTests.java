package com.android.tradefed.targetprep;

import com.android.compatibility.tradefed.StsTradefedTest;

import junit.framework.Test;
import junit.framework.TestSuite;

public class UnitTests extends TestSuite {

    public UnitTests() {
        super();
        addTestSuite(ExclusionCriteriaCheckTest.class);
        addTestSuite(StsTradefedTest.class);
    }

    public static Test suite() {
        return new UnitTests();
    }
}
