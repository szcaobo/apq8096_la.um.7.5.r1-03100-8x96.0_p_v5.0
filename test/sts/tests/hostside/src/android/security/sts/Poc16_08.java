/**
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc16_08 extends SecurityTestCase {
    /**
     *  b/27424603
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_2474() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2016-2474", getDevice(), 60);
    }

    /**
     *  b/28751152
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2014_9874() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/msm_aac_in")) {
            AdbUtils.runCommandLine("logcat -c" ,getDevice());
            AdbUtils.runPoc("CVE-2014-9874", getDevice(), 60);
            String logcat = AdbUtils.runCommandLine("logcat -d", getDevice());
            assertMatches("[\\s\\n\\S]*CVE-2014-9874 write error" +
                        "[\\s\\n\\S]*", logcat);
        }
    }

    /**
     *  b/28814690
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2014_9898() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/usf1")) {
            AdbUtils.runCommandLine("logcat -c" , getDevice());
            AdbUtils.runPoc("CVE-2014-9898", getDevice(), 60);
            String logcat = AdbUtils.runCommandLine("logcat -d",getDevice());
            assertNotMatches("[\\s\\n\\S]*US_GET_VERSION MEM LEAK,NOT VALIDATED " +
                             "INPUT LEN[\\s\\n\\S]*", logcat);
        }
    }

    /**
     *  b/28592402
     */
    @RootPermissionTest(minPatchLevel = "2016-08")
    @Test
    public void testPocCVE_2016_3836() throws Exception {
        enableAdbRoot(getDevice());

        AdbUtils.runCommandLine("logcat -c" , getDevice());
        AdbUtils.runPoc("CVE-2016-3836", getDevice(), 60);

        String logcat =  AdbUtils.runCommandLine("logcat -d", getDevice());
        assertMatches("[\\s\\n\\S]*leaked stats2: 0x0[\\s\\n\\S]*", logcat);
    }
}
