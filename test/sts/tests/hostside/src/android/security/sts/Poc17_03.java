/**
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc17_03 extends SecurityTestCase {

    /**
     *  b/33277611
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0463() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPocNoOutput("CVE-2017-0463", getDevice(), 30);
        // CTS begins the next test before device finishes rebooting,
        // sleep to allow time for device to reboot.
        Thread.sleep(30000);
    }

    /**
     *  b/32372915
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0519() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/qbt1000")) {
            AdbUtils.runPocNoOutput("CVE-2017-0519", getDevice(), 30);
        }
    }

    /**
     *  b/31750232
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0520() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/qce")) {
            AdbUtils.runPocNoOutput("CVE-2017-0520", getDevice(), 30);
            // CTS begins the next test before device finishes rebooting,
            // sleep to allow time for device to reboot.
            Thread.sleep(60000);
        }
    }

    /**
     *  b/31695439
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0457() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/adsprpc-smd")) {
            AdbUtils.runPocNoOutput("CVE-2017-0457", getDevice(), 30);
            // CTS begins the next test before device finishes rebooting,
            // sleep to allow time for device to reboot.
            Thread.sleep(60000);
        }
    }

    /**
     *  b/31252965
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0460() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPocNoOutput("CVE-2017-0460", getDevice(), 60);
    }

    /**
     *  b/33106520
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0456() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/ipa")) {
            AdbUtils.runPocNoOutput("CVE-2017-0456", getDevice(), 30);
            // CTS begins the next test before device finishes rebooting,
            // sleep to allow time for device to reboot.
            Thread.sleep(60000);
        }
    }

    /**
     *  b/32919951
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0521() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/ion")) {
            AdbUtils.runPocNoOutput("CVE-2017-0521", getDevice(), 30);
        }
    }

    /**
     *  b/33979145
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0453() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPocNoOutput("CVE-2017-0453", getDevice(), 30);
        // Device takes up to 90 seconds to crash after PoC run.
        Thread.sleep(90000);
    }

    /**
     *  b/32709702
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8413() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/v4l-subdev12")) {
            AdbUtils.runCommandLine("dmesg -c", getDevice());
            AdbUtils.runPoc("CVE-2016-8413", getDevice(), 60);
            String out = AdbUtils.runCommandLine("dmesg", getDevice());
            assertNotMatches("[\\n\\s\\S]*error finding buffer queue entry " +
                             "for identity:[0-9]+[\\n\\s\\S]*", out);
        }
    }

    /**
     * b/33745862
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8483() throws Exception {
      if(containsDriver(getDevice(), "/dev/pta")){
        enableAdbRoot(getDevice());
        AdbUtils.runPocNoOutput("CVE-2016-8483", getDevice(), 30);
      }
    }

    /**
     * b/32877245
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0531() throws Exception {
      enableAdbRoot(getDevice());
      if(containsDriver(getDevice(), "/dev/snd/pcmC0D30c")){
           AdbUtils.runPocNoOutput("CVE-2017-0531", getDevice(), 30);
       }
    }

    /**
     *  b/33979145
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0533() throws Exception {
      if(containsDriver(getDevice(), "/dev/graphics/fb1")){
        enableAdbRoot(getDevice());
        String out = AdbUtils.runPoc("CVE-2017-0533", getDevice(), 30);
        assertNotMatchesMultiLine(".*data\\[\\d+\\]: 0x9{8,16}.*data\\[\\d+\\]: 0x(?!9{8,16})" +
                                  "[0-9a-fA-F]{8,16}.*data\\[\\d+\\]: 0x9{8,16}.*",out);
      }
    }

    /**
     *  b/32644895
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0459() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/wwan_ioctl")) {
            AdbUtils.runPocNoOutput("CVE-2017-0459", getDevice(), 30);
            // CTS begins the next test before device finishes rebooting,
            // Device takes longer than usual to reboot
            getDevice().waitForDeviceAvailable(90000);
        }
    }

    /**
     *  b/32940193
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0464() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runCommandLine("dmesg -c", getDevice());
        AdbUtils.runPoc("CVE-2017-0464", getDevice(), 60);
        String out = AdbUtils.runCommandLine("dmesg", getDevice());
        assertNotMatchesMultiLine(".*__wlan_hdd_cfg80211_extscan_set_ssid_hotlist:" +
                                  "[\\s]*[0-9]+:[\\s]attr band failed.*", out);
    }

    /**
     *  b/33897738
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0526() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/sys/devices/virtual/htc_sensorhub/sensor_hub/enable")) {
            assertFalse("VULNERABLE EXIT CODE FOUND",
                        AdbUtils.runPocCheckExitCode("CVE-2017-0526", getDevice(), 30));
        }
    }

    /*
     *  b/32073794
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0461() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runCommandLine("dmesg -c", getDevice());
        AdbUtils.runPocNoOutput("CVE-2017-0461", getDevice(), 60);
        String out = AdbUtils.runCommandLine("dmesg", getDevice());
        assertNotMatchesMultiLine(".*__iw_set_genie Set UNKNOWN IE [a-fA-F0-9]{1,2}.*", out);
    }

    /**
     *  b/33547247
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0535() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/rt5506")) {
          //All vulnerable devices have this audio file
          String command1 = "am start -a android.intent.action.VIEW -d " +
                            "file:///system/media/audio/ringtones/Atria.ogg " +
                            "-t audio/ogg";
          String command2 = "input keyevent 4";
          AdbUtils.runCommandLine(command1, getDevice());
          AdbUtils.runPocNoOutput("CVE-2017-0535", getDevice(), 60);
          AdbUtils.runCommandLine(command2, getDevice());
        }
    }
}
