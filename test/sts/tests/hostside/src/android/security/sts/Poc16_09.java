/**
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc16_09 extends SecurityTestCase {

    //Highs
    /**
     *  b/31243641
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3868() throws Exception {
        enableAdbRoot(getDevice());
        String command =
            "echo '1 1 1 1 4702111234474983745' > /d/msm_core/ptable";
        AdbUtils.runCommandLine(command, getDevice());
    }

    /**
     *  b/28675151
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3858() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/sys/kernel/debug/msm_subsys/venus")) {
             AdbUtils.runPoc("CVE-2016-3858", getDevice(), 60);
        }
    }

    /**
     *  b/29009982
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3869() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2016-3869", getDevice(), 60);
    }

    //High
    /**
     *  b/28919863
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3867() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/ipa")) {
            AdbUtils.runPoc("CVE-2016-3867", getDevice(), 60);
        }
    }

    /**
     *  b/29944562
     */
    @RootPermissionTest(minPatchLevel = "2016-09")
    @Test
    public void testPocCVE_2016_3874() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2016-3874", getDevice(), 60);
    }

    /**
     *  b/28815326
     */
    @RootPermissionTest(minPatchLevel = "2016-09")
    @Test
    public void testPocCVE_2016_3859() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/v4l-subdev15")) {
            AdbUtils.runPoc("CVE-2016-3859", getDevice(), 60);
        }
    }

    /**
     *  b/28868303
     */
    @RootPermissionTest(minPatchLevel = "2016-09")
    @Test
    public void testPocCVE_2016_3866() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/snd/pcmC0D19c") ||
           containsDriver(getDevice(), "dev/snd/pcmC0D19p")) {
            AdbUtils.runPoc("CVE-2016-3866", getDevice(), 60);
        }
    }

    /**
     *  b/29618014
     */
    @RootPermissionTest(minPatchLevel = "2016-09")
    @Test
    public void testPocCVE_2016_3894() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/msmdma")) {
           AdbUtils.runCommandLine("logcat -c" , getDevice());
           AdbUtils.runPoc("CVE-2016-3894", getDevice(), 60);
           String logcat =  AdbUtils.runCommandLine("logcat -d", getDevice());
           assertNotMatches("[\\s\\n\\S]*CVE-2016-3894 Failed[\\s\\n\\S]*", logcat);
        }
    }
}
