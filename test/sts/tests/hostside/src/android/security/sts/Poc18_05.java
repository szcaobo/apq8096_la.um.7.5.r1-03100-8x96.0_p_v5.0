/**
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc18_05 extends SecurityTestCase {

    /**
     * b/70986337
     */
    @RootPermissionTest
    @Test
    public void testPocBug_70986337() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runCommandLine("logcat -c", getDevice());
        AdbUtils.runPocNoOutput("Bug-70986337", getDevice(), 30);
        String logcat = AdbUtils.runCommandLine("logcat -d -s SensorService", getDevice());
        // The check associated with this log would use data from an OOB read
        assertNotMatches("[\\s\\n\\S]*Ashmem direct channel size 70986337 greater than shared " +
                         "memory size[\\s\\n\\S]*", logcat);
    }
}
