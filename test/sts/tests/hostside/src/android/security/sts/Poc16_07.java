/**
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc16_07 extends SecurityTestCase {

  /**
   * b/29007611
   */
  @RootPermissionTest
  @Test
  public void testPocCVE_2017_3771_1() throws Exception {
    enableAdbRoot(getDevice());
    if(containsDriver(getDevice(), "/proc/emifreq/emifreq_debug"))
      AdbUtils.runCommandLine("cat /proc/emifreq/emifreq_debug > /dev/null", getDevice());
    // PoC causes kernel panic, wait for device to reboot
    Thread.sleep(12000);
  }

  /**
   * b/29007611
   */
  @RootPermissionTest
  @Test
  public void testPocCVE_2017_3771_2() throws Exception {
    enableAdbRoot(getDevice());
    if(containsDriver(getDevice(), "/proc/emifreq/emifreq_state"))
      AdbUtils.runCommandLine("cat /proc/emifreq/emifreq_state > /dev/null", getDevice());
    // PoC causes kernel panic, wait for device to reboot
    Thread.sleep(12000);
  }

  /**
   * b/29008188
   */
  @RootPermissionTest
  @Test
  public void testPocCVE_2017_3772() throws Exception {
    enableAdbRoot(getDevice());
    if(containsDriver(getDevice(), "/proc/driver/isp_reg"))
      AdbUtils.runCommandLine("cat /proc/driver/isp_reg > /dev/null", getDevice());
    // PoC causes kernel panic, wait for device to reboot
    Thread.sleep(12000);
  }

  /**
   * b/29008609
   */
  @RootPermissionTest
  @Test
  public void testPocCVE_2017_3774() throws Exception {
    enableAdbRoot(getDevice());
    if(containsDriver(getDevice(), "/proc/sysram"))
      AdbUtils.runCommandLine("cat /proc/sysram > /dev/null", getDevice());
    // PoC causes kernel panic, wait for device to reboot
    Thread.sleep(12000);
  }

  /**
   * b/28346752
   */
  @RootPermissionTest
  @Test
  public void testPocCVE_2017_3770() throws Exception {
    enableAdbRoot(getDevice());
    if(containsDriver(getDevice(), "/proc/driver/camio_reg"))
      AdbUtils.runCommandLine("cat /proc/driver/camio_reg > /dev/null", getDevice());
    // PoC causes kernel panic, wait for device to reboot
    Thread.sleep(12000);
  }

  /**
   * b/29008363
   */
  @RootPermissionTest
  @Test
  public void testPocCVE_2017_3773() throws Exception {
    enableAdbRoot(getDevice());
    if(containsDriver(getDevice(), "/proc/mtkfb_size"))
      AdbUtils.runCommandLine("cat /proc/mtkfb_size > /dev/null", getDevice());
    // PoC causes kernel panic, wait for device to reboot
    Thread.sleep(12000);
  }

  /**
   *  b/28402196
   */
  @RootPermissionTest
  @Test
  public void testPocCVE_2016_3807() throws Exception {
    enableAdbRoot(getDevice());
    if (containsDriver(getDevice(), "/dev/spich")) {
        AdbUtils.runPoc("CVE-2016-3807", getDevice(), 60);
    }
  }

  /**
   *  b/28085680
   */
  @RootPermissionTest
  @Test
  public void testPocCVE_2016_3797() throws Exception {
      enableAdbRoot(getDevice());
      AdbUtils.runPoc("CVE-2016-3797", getDevice(), 60);
  }

  /**
   * b/28172137
   */
  @RootPermissionTest
  @Test
  public void testPocCVE_2016_3768() throws Exception {
    enableAdbRoot(getDevice());
    AdbUtils.runPoc("CVE-2016-3768", getDevice(), 60);
  }

    /**
     *  b/28172322
     */
    @RootPermissionTest(minPatchLevel = "2016-07")
    @Test
    public void testPocCVE_2016_3813() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/d/f9200000.dwc3")){
           AdbUtils.runCommandLine("echo 0 9999999 > /d/f9200000.dwc3/requests", getDevice());
           String result = AdbUtils.runCommandLine("cat /d/f9200000.dwc3/requests", getDevice());
           assertNotMatches("ep0out request list: flags: 0x11",result);
        }
    }
}
