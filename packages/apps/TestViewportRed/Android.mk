LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)


LOCAL_MODULE_TAGS := optional
LOCAL_SDK_VERSION := current

LOCAL_PACKAGE_NAME := TestViewportRed

LOCAL_SRC_FILES := $(call all-subdir-java-files)


include $(BUILD_PACKAGE)
