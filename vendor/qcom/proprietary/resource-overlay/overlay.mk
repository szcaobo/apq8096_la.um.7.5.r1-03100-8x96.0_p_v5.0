#include common overlays
PRODUCT_PACKAGES += \
    FrameworksResCommon \
    BluetoothResCommon \
    CarrierConfigResCommon \
    CellBroadcastReceiverResCommon \
    DialerResCommon \
    SystemUIResCommon \
    TelecommResCommon \
    TelephonyResCommon

#include target specific overlays
OVERLAY_PATH := vendor/qcom/proprietary/resource-overlay
-include $(OVERLAY_PATH)/$(TARGET_PRODUCT)/OverlayBoardConfig.mk
