LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_preview_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_preview_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_default_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_default_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libtzplayready_customer
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := qcdrm/playready/lib
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/qcdrm/playready/lib/libtzplayready_customer.so
LOCAL_SHARED_LIBRARIES := libc++ libcrypto libcppf libm libc libutils libdl libcutils liblog libdrmfs libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := MTP_General_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/MTP
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/MTP/MTP_General_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_hfr_90_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_hfr_90_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_4k_preview_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_4k_preview_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QPerformance
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/QPerformance.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_demosaic47
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_demosaic47.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov5645
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov5645.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_hfr_120_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_hfr_120_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_60_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_60_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libslimclient_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/slim/client
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx230_qc2002_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx230_qc2002_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_abf47
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_abf47.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_default_preview_dw9763
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_default_preview_dw9763.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := SnapdragonSVA
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/SnapdragonSVA/SnapdragonSVA.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfduibcsink
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfduibcsink.so
LOCAL_SHARED_LIBRARIES := libc++ libFileMux libm libc libutils libdl liblog libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_camera_front.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_camera_front.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.ims.rcsconfig-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.ims.rcsconfig-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a300_pfp.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a300_pfp.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_s5k2l7
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_s5k2l7.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.izattools.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qualcomm.qti.izattools.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := hci_qcomm_init
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hci_qcomm_init
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils libbtnv
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := librpmbStatic
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 32
LOCAL_SRC_FILES     := obj_arm/STATIC_LIBRARIES/librpmbStatic_intermediates/librpmbStatic.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libCommandSvc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib64/libCommandSvc.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libbinder libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_postproc_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_postproc_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov5695_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov5695_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := test_smgr_buf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/test_smgr_buf
LOCAL_SHARED_LIBRARIES := libc++ libm libc libsensor1 libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libwfdhdcp_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/wfd/mm/hdcp40/common
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_gyroscope.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_gyroscope.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := diag_callback_sample
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/diag_callback_sample
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-qomx-idec-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-qomx-idec-test
LOCAL_SHARED_LIBRARIES := libqomx_core libc++ libm libc libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libssdStatic
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 64
LOCAL_SRC_FILES     := obj/STATIC_LIBRARIES/libssdStatic_intermediates/libssdStatic.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_zsl_video_dw9763
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_zsl_video_dw9763.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_default_video_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_default_video_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov13850_q13v06k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov13850_q13v06k.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cneapiclient.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/cneapiclient.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_video_16M_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_video_16M_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := SSGTelemetryService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := vendor/app/SSGTelemetryService/SSGTelemetryService.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qjpeg-dma-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qjpeg-dma-test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmqjpegdma
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := strings.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/strings.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hdr_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hdr_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_paaf_lib
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_paaf_lib.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_frame_algorithm libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsmwrapper
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libsmwrapper.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a225p5_pm4.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a225p5_pm4.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_snapshot_upscale
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_snapshot_upscale.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_zsl_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_zsl_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_zsl_video_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_zsl_video_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := oem-services
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/oem-services.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.factory-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.factory-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_default_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_default_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx318_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx318_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_demux40
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_demux40.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := netmgr_config.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := data
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/data/netmgr_config.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx230_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx230_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libprdrmdecrypt_customer
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := qcdrm/playready/lib
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/qcdrm/playready/lib/libprdrmdecrypt_customer.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libbinder libutils libz libcutils liblog libdl libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.uceservice-V2.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.qualcomm.qti.uceservice-V2.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_snapshot_upscale_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_snapshot_upscale_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_a3_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_a3_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wfdservice.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/init/wfdservice.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_ds_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_ds_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.esepowermanager@1.0-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.qti.esepowermanager@1.0-service
LOCAL_INIT_RC       := vendor/etc/init/vendor.qti.esepowermanager@1.0-service.rc
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libc++ libm libhardware_legacy libc libhwbinder libhidltransport vendor.qti.esepowermanager@1.0 libutils libdl libcutils liblog libQSEEComAPI libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imglib
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imglib.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libm libfastcvopt libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_dw9718
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_dw9718.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_default_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_default_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hdr_snapshot_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hdr_snapshot_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_a3_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_a3_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libcneapiclient_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/cne/api
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := ipacm-diag
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/ipacm-diag
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libdl libcutils liblog libdsutils
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_default_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_default_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hdr_snapshot_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hdr_snapshot_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libslimcommon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 64
LOCAL_SRC_FILES     := obj/STATIC_LIBRARIES/libslimcommon_intermediates/libslimcommon.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_bu64244gwz
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_bu64244gwz.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.imscmservice-V2.0-java.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qualcomm.qti.imscmservice-V2.0-java.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := uimremoteclient
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/uimremoteclient/uimremoteclient.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqomx_jpegdec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libqomx_jpegdec.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmmjpeg libdl libcutils liblog libmmqjpeg_codec
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.wigig.netperftuner-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.wigig.netperftuner-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdisp-aba
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdisp-aba.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdisp-aba.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := diag_mdlog
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/diag_mdlog
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_primary_mic.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_primary_mic.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mmid
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mmid
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QTIDiagServices
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/QTIDiagServices/QTIDiagServices.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QRD_General_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/QRD
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/QRD/QRD_General_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530v1_pm4.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530v1_pm4.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libril-qc-settingsd-headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/qcril/settingsd
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_preview_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_preview_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov5670_f5670bq_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov5670_f5670bq_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_4k_video_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_4k_video_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_60_dw9761b
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_60_dw9761b.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_simcard1.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_simcard1.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.server-V2.1-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.quicinc.cne.server-V2.1-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdmmsink
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdmmsink.so
LOCAL_SHARED_LIBRARIES := libc++ libFileMux libmedia libdisplayconfig libaudioclient libutils libdl libm libgui libqdMetaData.system libbinder libc libmmosal libhidlbase libaacwrapper android.hidl.allocator@1.0 libwfdconfigutils libmmrtpdecoder libmmparser_lite liblog libhwui libhidlmemory libui android.hidl.memory@1.0 libwfdmminterface libcutils libwfdcommonutils libwfdcodecv4l2
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_csidtg_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_csidtg_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libgardencaseulp_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/garden-app/GARDEn/ulp_case
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := Fluid_Speaker_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Fluid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Fluid/Fluid_Speaker_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libslimcommon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 32
LOCAL_SRC_FILES     := obj_arm/STATIC_LIBRARIES/libslimcommon_intermediates/libslimcommon.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_a3_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_a3_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_default_video_dw9763
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_default_video_dw9763.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_hfr_60_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_hfr_60_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.soter@1.0-service.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/init/vendor.qti.hardware.soter@1.0-service.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_zsl_video_bu64297
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_zsl_video_bu64297.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-adec-omxamrwbplus-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-adec-omxamrwbplus-test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmm-omxcore libdl libaudioalsa libOmxAmrwbplusDec
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := hal_ss_test_manual
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hal_ss_test_manual
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Backup
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/Backup/Backup.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_gamma44
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_gamma44.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CtSystemUIRes
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_MODULE_PATH   := $(TARGET_OUT_VENDOR)/overlay
LOCAL_SRC_FILES     := vendor/overlay/CtSystemUITheme/CtSystemUIRes.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a225_pfp.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a225_pfp.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := port-bridge
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/port-bridge
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils libdsutils
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_4k_video_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_4k_video_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_4k_preview_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_4k_preview_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := gpfspath_oem_config.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/gpfspath_oem_config.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.constants@2.1
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.quicinc.cne.constants@2.1.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.quicinc.cne.constants@2.1.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_90_dw9761b
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_90_dw9761b.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscvePanorama_lite
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscvePanorama_lite.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscvePanorama_lite.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libfastcvopt libjpeg libc liblog libdl libscveBlobDescriptor libscveCommon
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libfastcvopt libjpeg libc liblog libdl libscveBlobDescriptor libscveCommon
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-still-jpegdma-drv-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-still-jpegdma-drv-test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vpplibraryunittest
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/vpplibraryunittest
LOCAL_SHARED_LIBRARIES := libc++ libm libc libvpplibrary libutils libdl libcutils liblog libvpptestutils
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imssettings
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/imssettings/imssettings.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_key.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_key.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := LteDirectDiscoveryLibrary-proto
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/LteDirectDiscoveryLibrary-proto.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_90_dw9761b_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_90_dw9761b_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libGPreqcancel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libGPreqcancel.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libGPreqcancel.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libutils libdl libcutils liblog libGPreqcancel_svc libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libutils libdl libcutils liblog libGPreqcancel_svc libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_us_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_us_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_1080p_video_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_1080p_video_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_s5k3m2xx
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_s5k3m2xx.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := adpl
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/adpl
LOCAL_SHARED_LIBRARIES := libc++ librmnetctl libm libqcmaputils libc libqmi libdiag libqmi_encdec libnetutils libdl libcutils liblog libqmiservices libdsutils libqmi_cci libqmi_client_qmux
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_fovcrop_video46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_fovcrop_video46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.display.color@1.0-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.display.color@1.0-service
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libsdm-disp-vndapis vendor.display.postproc@1.0 libhardware_legacy libc libhwbinder libhidltransport libbinder libc++ libutils libdl libcutils liblog libm libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CmccMmsRes
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_MODULE_PATH   := $(TARGET_OUT_VENDOR)/overlay
LOCAL_SRC_FILES     := vendor/overlay/CmccMmsTheme/CmccMmsRes.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := pktlogconf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/pktlogconf
LOCAL_SHARED_LIBRARIES := libc++ libc libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_4K_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_4K_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libvpp_frc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libvpp_frc.so
LOCAL_SHARED_LIBRARIES := libc
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := radioconfiglibrary
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/radioconfiglibrary.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_simcard2.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_simcard2.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := diag_socket_log
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/diag_socket_log
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveObjectSegmentation_stub
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscveObjectSegmentation_stub.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscveObjectSegmentation_stub.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libadsprpc libm libc libdl libscveCommon
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libadsprpc libm libc libdl libscveCommon
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.soter@1.0-provision
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/vendor.qti.hardware.soter@1.0-provision
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog vendor.qti.hardware.soter@1.0 libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mdm_helper
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mdm_helper
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmdmdetect libutils libmdmimgload libcutils liblog libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_hdr_gb_lib
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_hdr_gb_lib.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libstdc++ libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_4k_preview_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_4k_preview_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := apdr.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/apdr.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := msm_irqbalance
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/msm_irqbalance
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := dpmd.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/init/dpmd.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_zsl_preview_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_zsl_preview_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadreno_utils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libadreno_utils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libadreno_utils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libgsl libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libgsl libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmqjpeg_codec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmqjpeg_codec.so
LOCAL_SHARED_LIBRARIES := libc++ libcrypto libm libfastcvopt libc libmmjpeg libdl libcutils liblog libmmqjpegdma
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.server@2.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.quicinc.cne.server@2.0.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.quicinc.cne.server@2.0.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.0 libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.0 libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_5_0.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_5_0.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_hfr_120_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_hfr_120_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_1080p_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_1080p_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.constants-V2.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.quicinc.cne.constants-V2.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := perfservice
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/perfservice
LOCAL_SHARED_LIBRARIES := libc++ libm libc libbinder libutils libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := privapp-permissions-com.qualcomm.location.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/privapp-permissions-com.qualcomm.location.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := fm_qsoc_patches
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/fm_qsoc_patches
LOCAL_SHARED_LIBRARIES := libc++ libm libc libqmi libdl libcutils libqmiservices libqmi_cci
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ftmdaemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/ftmdaemon
LOCAL_SHARED_LIBRARIES := libc++ libm libbt-hidlclient libdl libnl libdiag libc libcutils liblog libbtnv libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_bg_stats46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_bg_stats46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := report.png
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/report.png
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := test_bet_8996
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/test_bet_8996
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_isp_modules libmmcamera2_mct libm libc libdl libcutils
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := dsi_config.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := data
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/data/dsi_config.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_hfr_60_dw9714
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_hfr_60_dw9714.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := move_time_data.sh
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/move_time_data.sh
LOCAL_SHARED_LIBRARIES := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_headset.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_headset.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_ds_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_ds_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmrtpencoder
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libmmrtpencoder.so
LOCAL_SRC_FILES_arm64 := system/lib64/libmmrtpencoder.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog libmmosal
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_120_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_120_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := test_module_pproc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/test_module_pproc
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera2_pproc_modules libmmcamera2_mct libm libmmcamera_dbg libc libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_fullsize_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_fullsize_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_zsl_preview_bu64297
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_zsl_preview_bu64297.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx362_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx362_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := LteDirectDiscoveryLibrary
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/LteDirectDiscoveryLibrary.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_dw9714
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_dw9714.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libc2d30_bltlib
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libc2d30_bltlib.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libc2d30_bltlib.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgsl libutils libdl libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgsl libutils libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sony_imx298_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sony_imx298_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsubsystem_control
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsubsystem_control.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsubsystem_control.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libperipheral_client libmdmdetect libdl libcutils liblog libqmi_cci libqmi_common_so
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libperipheral_client libmdmdetect libdl libcutils liblog libqmi_cci libqmi_common_so
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_default_video_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_default_video_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_hfr_60_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_hfr_60_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdataitems
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdataitems.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdataitems.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov7251
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov7251.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_keypad.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_keypad.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_color_correct46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_color_correct46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_color_xform_viewfinder46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_color_xform_viewfinder46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libtcmd_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/wlan/ath6kl-utils/libtcmd
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_1080p_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_1080p_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_default_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_default_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_a3_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_a3_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsurround_3mic_proc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libsurround_3mic_proc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libnlnetmgr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libnlnetmgr.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libnlnetmgr.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libconfigdb libm libc libdiag libdl libcutils liblog libdsutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libconfigdb libm libc libdiag libdl libcutils liblog libdsutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_hvx_grid_sum
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_hvx_grid_sum.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libvpplibrary
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libvpplibrary.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libvpplibrary.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libadsprpc libm libc libutils libdl libcutils liblog libvpphvx libqdMetaData
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libadsprpc libm libc libutils libdl libcutils liblog libvpphvx libqdMetaData
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := footer_fail.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/footer_fail.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hfr_240_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hfr_240_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := diag_uart_log
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/diag_uart_log
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsp_hvx_stats
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libadsp_hvx_stats.so
LOCAL_SHARED_LIBRARIES := libadsp_hvx_skel
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_4k_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_4k_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.alarm@1.0-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.qti.hardware.alarm@1.0-service
LOCAL_INIT_RC       := vendor/etc/init/vendor.qti.hardware.alarm@1.0-service.rc
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libc++ libm libc libhidltransport vendor.qti.hardware.alarm@1.0 libutils libdl libcutils liblog libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := radioconfiginterface.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/radioconfiginterface.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.imscmservice-V2.1-java.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qualcomm.qti.imscmservice-V2.1-java.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_psensor.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_psensor.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdrtsp
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdrtsp.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libwfdconfigutils libutils libdl libcutils liblog libwfdcommonutils libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_240_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_240_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_4K_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_4K_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsecureui_svcsock
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsecureui_svcsock.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsecureui_svcsock.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ vendor.qti.hardware.tui_comm@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ vendor.qti.hardware.tui_comm@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_clnt_test_4001
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_clnt_test_4001
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_clnt_test_4000
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_clnt_test_4000
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_encdec libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libcppf
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libcppf.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsp_hvx_zzhdr_BGGR
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libadsp_hvx_zzhdr_BGGR.so
LOCAL_SHARED_LIBRARIES := libadsp_hvx_skel
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_hfr_120_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_hfr_120_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblistenjni
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/liblistenjni.so
LOCAL_SRC_FILES_arm64 := system/lib64/liblistenjni.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblistensoundmodel2 liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblistensoundmodel2 liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsprpc_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libadsprpc_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libadsprpc_system.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_4k_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_4k_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530_zap.elf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530_zap.elf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_4k_preview_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_4k_preview_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_4k_video_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_4k_video_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_60_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_60_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx258_bear_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx258_bear_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_zsl_preview_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_zsl_preview_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mmi
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/mmi
LOCAL_SHARED_LIBRARIES := libc++ libbase libui libhidlbase libhwbinder libc_malloc_debug libm libgui libxml2 libhidltransport libbinder libmmi libutils libc libcutils liblog libdl vendor.qti.hardware.factory@1.0 libhwui libft2 libicuuc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k3l8_mono_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k3l8_mono_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_a3_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_a3_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qcril.db
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/radio/qcril_database
LOCAL_SRC_FILES     := vendor/radio/qcril_database/qcril.db
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := MTP_Global_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/MTP
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/MTP/MTP_Global_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_hfr_60_bu64297
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_hfr_60_bu64297.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_4k_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_4k_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveT2T_skel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libscveT2T_skel.so
LOCAL_SHARED_LIBRARIES := 
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qti.snapdragon.sdk.display.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qti.snapdragon.sdk.display.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_report.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_report.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.latency-V2.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.latency-V2.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := uimremoteserver.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/uimremoteserver.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx298_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx298_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_video_4k_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_video_4k_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_sdcard.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_sdcard.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_iface_modules
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_iface_modules.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_isp_modules libmmcamera2_mct libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qcrilhook
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/qcrilhook.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_battery.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_battery.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsp_hvx_zzhdr_RGGB
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libadsp_hvx_zzhdr_RGGB.so
LOCAL_SHARED_LIBRARIES := libadsp_hvx_skel
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_fm.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_fm.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := perflock_native_stress_test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/perflock_native_stress_test
LOCAL_SHARED_LIBRARIES := libc++ libperfgluelayer libm libqti-perfd libc libutils libdl libcutils liblog libqti-util
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := slim_daemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/slim_daemon
LOCAL_SHARED_LIBRARIES := libc++ liblbs_core android.frameworks.sensorservice@1.0 libm libc libhwbinder libdiag libqmi_csi libgps.utils libutils android.hardware.sensors@1.0 libqmi_encdec liblog libdl libsensorndkbridge libloc_core libloc_api_v02 libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_snapshot_upscale
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_snapshot_upscale.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k2l7_8953_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k2l7_8953_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530v2_seq.fw2
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530v2_seq.fw2
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_hfr_120_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_hfr_120_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.sensorscalibrate-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.sensorscalibrate-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := uimlpaservice
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/uimlpaservice/uimlpaservice.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := athdiag
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/athdiag
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hfr_120_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hfr_120_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := dpmapi.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/dpmapi.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_zsl_preview_dw9761b
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_zsl_preview_dw9761b.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_default_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_default_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := fail.png
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/fail.png
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ims
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/ims/ims.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.wifidisplayhal@1.0-halimpl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/com.qualcomm.qti.wifidisplayhal@1.0-halimpl.so
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libm libc libhidltransport libutils libdl libcutils liblog com.qualcomm.qti.wifidisplayhal@1.0 libwfdmodulehdcpsession libwfdhaldsmanager
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx298_gt24c64_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx298_gt24c64_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_zsl_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_zsl_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_default_preview_dw9761b
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_default_preview_dw9761b.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_cpp_ds_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_cpp_ds_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_a3_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_a3_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qrd.wappush
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.qrd.wappush.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_s5k3l8
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_s5k3l8.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := TestApp5G
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/TestApp5G/TestApp5G.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ppd
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/ppd
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libm vendor.display.postproc@1.0 libc libhidltransport libutils libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := qcrild_librilutils-headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/qcril-hal/qcrild/librilutils
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmjpeg
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmjpeg.so
LOCAL_SHARED_LIBRARIES := libc++ libm libfastcvopt libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_cpp_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_cpp_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_fullsize_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_fullsize_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := izat.xt.srv.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/izat.xt.srv.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libGPTEE_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libGPTEE_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libGPTEE_system.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.bluetooth@1.0-service-qti
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/android.hardware.bluetooth@1.0-service-qti
LOCAL_INIT_RC       := vendor/etc/init/android.hardware.bluetooth@1.0-service-qti.rc
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ vendor.qti.hardware.fm@1.0 libm android.hardware.bluetooth@1.0 libhwbinder libhidltransport libutils libc libcutils liblog libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov4688
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov4688.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_a3_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_a3_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libwfd_proprietary_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/wfd
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libmmosal_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/mm-osal
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-audio-ftm
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-audio-ftm
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libtinyalsa libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_ds_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_ds_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libqmi_cci_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/commonsys-intf/qmi-framework/qcci/src
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_hfr_120_ad5823
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_hfr_120_ad5823.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qseecomd
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qseecomd
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog libdrmfs libQSEEComAPI
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sns_ar_testapp
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/sns_ar_testapp
LOCAL_SHARED_LIBRARIES := libc++ libc libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := MTP_Headset_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/MTP
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/MTP/MTP_Headset_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_system_info.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_system_info.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_4k_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_4k_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k3p3sm_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k3p3sm_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_hdr_be_stats46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_hdr_be_stats46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := uimremoteclientlibrary
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/uimremoteclientlibrary.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_4k_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_4k_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libois_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libois_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sunny_gt24c64_s5k2l7_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sunny_gt24c64_s5k2l7_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_dummyalgo
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_dummyalgo.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libm libc libdl libcutils liblog libmmcamera_imglib
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_hdr_snapshot_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_hdr_snapshot_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_csidtg_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_csidtg_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CNEService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/CNEService/CNEService.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k3m2xm_chromatix_bear.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k3m2xm_chromatix_bear.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_clnt_test_2000
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_clnt_test_2000
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vpplibraryfunctionaltest
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/vpplibraryfunctionaltest
LOCAL_SHARED_LIBRARIES := libc++ libm libc libvpplibrary libutils libdl libcutils liblog libvpptestutils
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx362
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx362.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_hfr_60_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_hfr_60_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libgarden_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/garden-app/GARDEn
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_qtech_f3l8yam_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_qtech_f3l8yam_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_hfr_120_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_hfr_120_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libmmparser_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/mm-parser/Api
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := CtNetworkSettingRes
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_MODULE_PATH   := $(TARGET_OUT_VENDOR)/overlay
LOCAL_SRC_FILES     := vendor/overlay/CtNetworkSettingRes/CtNetworkSettingRes.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_lcd_backlight.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_lcd_backlight.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hfr_90_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hfr_90_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_120_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_120_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qrtr-lookup
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qrtr-lookup
LOCAL_SHARED_LIBRARIES := libc++ libqrtr libc libm libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveCommon
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscveCommon.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscveCommon.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libadsprpc libscveCommon_stub libm libc libdl liblog libion
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libadsprpc libscveCommon_stub libm libc libdl liblog libion
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_60_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_60_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_color_xform_encoder46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_color_xform_encoder46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := netmgrd
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/netmgrd
LOCAL_SHARED_LIBRARIES := libc++ libhidltransport libnetutils libutils libandroid_net libconfigdb libnlnetmgr libqmi_client_helper libdl libnetmgr libqmiservices libqmi_client_qmux libhidlbase librmnetctl libdiag libm libc libqmi liblog libdsutils libqmi_cci android.system.net.netd@1.1 libcutils libnetmgr_common libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_zsl_preview_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_zsl_preview_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sunny_gt24c64_imx298_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sunny_gt24c64_imx298_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.qteeconnector@1.0-service.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/init/vendor.qti.hardware.qteeconnector@1.0-service.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsettings
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsettings.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsettings.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libprotobuf-cpp-full libdiag libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libprotobuf-cpp-full libdiag libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov2281
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov2281.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libgardencasetracking_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/garden-app/GARDEn/tracking_case
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx258_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx258_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.location
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/com.qualcomm.location/com.qualcomm.location.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblimits_priv
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 64
LOCAL_SRC_FILES     := obj/STATIC_LIBRARIES/liblimits_priv_intermediates/liblimits_priv.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libpvr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libpvr.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libpvr.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfduibcsinkinterface
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfduibcsinkinterface.so
LOCAL_SHARED_LIBRARIES := libc++ libwfduibcsink libm libc libwfdconfigutils libutils libdl libcutils liblog libwfdcommonutils libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_hvx_add_constant
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_hvx_add_constant.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqcrilFramework
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqcrilFramework.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqcrilFramework.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libdl liblog libsettings
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libdl liblog libsettings
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := izat.xt.srv
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/izat.xt.srv.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Liquid_Speaker_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Liquid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Liquid/Liquid_Speaker_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_1080p_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_1080p_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_4k_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_4k_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_flashlight.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_flashlight.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_4k_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_4k_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_facedetection_lib
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_facedetection_lib.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_tuning
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_tuning.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera2_pproc_modules libmmcamera2_isp_modules libmmcamera2_mct libm libfastcvopt libmmcamera2_stats_modules libdl libmmcamera2_sensor_modules libmm-qcamera libmmcamera_eztune_module libmmcamera_dbg libcutils libmmcamera2_iface_modules libmmcamera2_imglib_modules liblog libmmcamera_tuning_lookup libc
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_eztune_module
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_eztune_module.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libmmcamera_ppbase_module libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := reboot.png
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/reboot.png
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := NetworkSetting
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/NetworkSetting/NetworkSetting.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsdm-disp-apis
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libsdm-disp-apis.so
LOCAL_SRC_FILES_arm64 := system/lib64/libsdm-disp-apis.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Fluid_Bluetooth_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Fluid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Fluid/Fluid_Bluetooth_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_hfr_120_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_hfr_120_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_a3_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_a3_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_cpp_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_cpp_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_vibrator.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_vibrator.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_sub_module
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_sub_module.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.constants@2.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.quicinc.cne.constants@2.0.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.quicinc.cne.constants@2.0.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_hfr_60_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_hfr_60_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_pp_buf_mgr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_pp_buf_mgr.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_sensor_modules
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_sensor_modules.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libmmcamera_pdaf libxml2 libc libz libcutils liblog libdl libmmcamera_pdafcamif
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx362_chromatix_bear.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx362_chromatix_bear.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := fmfactorytest
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/fmfactorytest
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sunny_q5v41b_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sunny_q5v41b_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmi_client_helper
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqmi_client_helper.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqmi_client_helper.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libidl libqmi_client_qmux libdiag libc libcutils liblog libdl libqmiservices libdsutils libqmi_cci
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libidl libqmi_client_qmux libdiag libc libcutils liblog libdl libqmiservices libdsutils libqmi_cci
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_clamp_video40
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_clamp_video40.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_hfr_120_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_hfr_120_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.imscmservice-V2.1-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.qualcomm.qti.imscmservice-V2.1-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_msensor.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_msensor.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_zsl_video_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_zsl_video_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxWmaDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxWmaDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxWmaDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := flp.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/flp.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_eeprom_util
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_eeprom_util.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.factory@1.0-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.qti.hardware.factory@1.0-service
LOCAL_INIT_RC       := vendor/etc/init/vendor.qti.hardware.factory@1.0-service.rc
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libc++ libm libc libhidltransport libutils libdl libcutils liblog vendor.qti.hardware.factory@1.0 libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := StoreKeybox
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/StoreKeybox
LOCAL_SHARED_LIBRARIES := libc++ libm libc liboemcrypto libutils libz libcutils liblog libdl libQSEEComAPI
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_hfr_60_dw9763
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_hfr_60_dw9763.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_zsl_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_zsl_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libvqzip
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libvqzip.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hdr_snapshot_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hdr_snapshot_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a420_pfp.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a420_pfp.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_default_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_default_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := MTP_Handset_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/MTP
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/MTP/MTP_Handset_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_cpp_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_cpp_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.quicinc.cne.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqti_performance
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libqti_performance.so
LOCAL_SRC_FILES_arm64 := system/lib64/libqti_performance.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libhidltransport libm libc vendor.qti.hardware.iop@2.0 libnativehelper libutils libdl libcutils liblog libqti-perfd-client_system libhwui libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libhidltransport libm libc vendor.qti.hardware.iop@2.0 libnativehelper libutils libdl libcutils liblog libqti-perfd-client_system libhwui libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k2l7_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k2l7_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := dun-server
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/dun-server
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := MTP_Hdmi_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/MTP
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/MTP/MTP_Hdmi_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_onsemi_cat24c16_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_onsemi_cat24c16_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmm-color-convertor
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmm-color-convertor.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmm-color-convertor.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qualsound.wav
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/qualsound.wav
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_mce40
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_mce40.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_a3_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_a3_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := header.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/header.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_mct_shimlayer
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_mct_shimlayer.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera2_pproc_modules libmmcamera2_isp_modules libmmcamera2_mct libm libmmcamera_dbg libmmcamera2_stats_modules libdl libmmcamera2_sensor_modules libc libcutils libmmcamera2_iface_modules libmmcamera2_imglib_modules libmmcamera_thread_services
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k3l8_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k3l8_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_a3_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_a3_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_5_1.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_5_1.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxAlacDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxAlacDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxAlacDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libjpegdhw
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libjpegdhw.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_8_0.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_8_0.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a420_pm4.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a420_pm4.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqcci_legacy
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqcci_legacy.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqcci_legacy.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libidl libqmi_client_qmux libdiag libc libcutils liblog libdl libqmi
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libidl libqmi_client_qmux libdiag libc libcutils liblog libdl libqmi
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_rs_stats46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_rs_stats46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-qcamera-daemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-qcamera-daemon
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera2_pproc_modules libmmcamera2_isp_modules libmmcamera2_mct libm libmmcamera_dbg libmmcamera2_stats_modules libdl libmmcamera2_sensor_modules libc libcutils libmmcamera2_iface_modules libmmcamera2_imglib_modules libmmcamera_thread_services
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx258_mono
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx258_mono.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qti.snapdragon.sdk.display
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.qti.snapdragon.sdk.display.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libcneoplookup_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/cne/OperatorLookup
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := uimremoteclient.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/uimremoteclient.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_clnt_test_1000
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_clnt_test_1000
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_clnt_test_1001
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_clnt_test_1001
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsp_hvx_callback_skel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libadsp_hvx_callback_skel.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libadsprpc libm libadsp_hvx_stub libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-pp-dpps
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-pp-dpps
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ android.frameworks.sensorservice@1.0 libm libqdutils libc libhidltransport libbinder libutils libdl libcutils libdisp-aba libtinyxml liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx378_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx378_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_fullsize_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_fullsize_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := DR_AP_Service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/DR_AP_Service
LOCAL_SHARED_LIBRARIES := lib_drplugin_server libc++ libm libc libdiag libgps.utils libutils libdl libcutils liblog libqmi_cci
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-adec-omxaac-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-adec-omxaac-test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmm-omxcore libOmxAacDec libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_hfr_120_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_hfr_120_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qdss.agent.sh
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/persist/coresight
LOCAL_SRC_FILES     := persist/coresight/qdss.agent.sh
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_default_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_default_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqcreverb
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := soundfx
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/soundfx/libqcreverb.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wfdconfig.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/wfdconfig.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := host_manager_11ad
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/host_manager_11ad
LOCAL_SHARED_LIBRARIES := libc++ libc libnl libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-audio-alsa-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-audio-alsa-test
LOCAL_SHARED_LIBRARIES := libc++ libc libaudioalsa libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_zsl_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_zsl_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_default_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_default_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mdm_helper_proxy
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mdm_helper_proxy
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmdmdetect libutils libmdmimgload libcutils liblog libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := AutoRegistration
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/AutoRegistration/AutoRegistration.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CtFrameworksRes
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_MODULE_PATH   := $(TARGET_OUT_VENDOR)/overlay
LOCAL_SRC_FILES     := vendor/overlay/CtFrameworksTheme/CtFrameworksRes.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hdr_video_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hdr_video_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wigig_remoteserver
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/wigig_remoteserver
LOCAL_SHARED_LIBRARIES := libc++ libwigig_utils libwigig_pciaccess libm libc libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_hfr_120_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_hfr_120_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := liblocationservice_glue_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/framework/native/framework-glue
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_hdr46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_hdr46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx378
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx378.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_hfr_90_bu64297
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_hfr_90_bu64297.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_5_2.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_5_2.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_90_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_90_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.keymaster@4.0-service-qti
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/android.hardware.keymaster@4.0-service-qti
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libqtikeymaster4 libcrypto libc++ libm libc libhwbinder libhidltransport android.hardware.keymaster@4.0 libutils libdl libcutils liblog libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_isp_modules
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_isp_modules.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := uimremotesimlocklibrary
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/uimremotesimlocklibrary.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := audioflacapp
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/audioflacapp
LOCAL_SHARED_LIBRARIES := libc++ libm libc libFlacSwDec libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_default_preview_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_default_preview_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_hfr_90_ad5823
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_hfr_90_ad5823.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_hfr_120_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_hfr_120_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := runall.png
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/runall.png
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_tintless_algo
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmmcamera_tintless_algo.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmmcamera_tintless_algo.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libmmcamera_dbg libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libmmcamera_dbg libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libulp2
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libulp2.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libulp2.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libDRPlugin libm libc libgps.utils libutils libdl libcutils liblog libevent_observer libloc_core liblbs_core libizat_core
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libDRPlugin libm libc libgps.utils libutils libdl libcutils liblog libevent_observer libloc_core liblbs_core libizat_core
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sunny_gt24c64a_imx362_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sunny_gt24c64a_imx362_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_hfr_90_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_hfr_90_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_telephone.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_telephone.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Liquid_Handset_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Liquid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Liquid/Liquid_Handset_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libQTapGLES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/egl/libQTapGLES.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/egl/libQTapGLES.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libdl libc libz libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libdl libc libz libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx258
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx258.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.api@1.1
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.quicinc.cne.api@1.1.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.quicinc.cne.api@1.1.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.1 libutils libdl libcutils liblog com.quicinc.cne.api@1.0
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.1 libutils libdl libcutils liblog com.quicinc.cne.api@1.0
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.api@1.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.quicinc.cne.api@1.0.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.quicinc.cne.api@1.0.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ com.quicinc.cne.constants@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ com.quicinc.cne.constants@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libizat_core
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libizat_core.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libizat_core.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libsqlite libm libc libgps.utils libutils libdl libcutils liblog libloc_core libloc_api_v02
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libsqlite libm libc libgps.utils libutils libdl libcutils liblog libloc_core libloc_api_v02
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_led_green.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_led_green.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdrmfs
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdrmfs.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdrmfs.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libxml2 libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libxml2 libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx214
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx214.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libEGL_adreno
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/egl/libEGL_adreno.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/egl/libEGL_adreno.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libdl libgsl libc libz libcutils liblog libadreno_utils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libdl libgsl libc libz libcutils liblog libadreno_utils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmqjpegdma
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmqjpegdma.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QFingerprintService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := vendor/app/QFingerprintService/QFingerprintService.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_bf_stats47
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_bf_stats47.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := path_config.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/path_config.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_scaler_encoder46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_scaler_encoder46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_default_preview_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_default_preview_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hfr_60_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hfr_60_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libgeofence
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libgeofence.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libgeofence.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libutils libdl libcutils liblog libloc_core liblbs_core libizat_core
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libutils libdl libcutils liblog libloc_core liblbs_core libizat_core
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_4k_preview_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_4k_preview_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QtiTelephonyServicelibrary
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/QtiTelephonyServicelibrary.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Fluid_General_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Fluid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Fluid/Fluid_General_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_hfr_60_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_hfr_60_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx230
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx230.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QRD_Bluetooth_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/QRD
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/QRD/QRD_Bluetooth_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ROW_profiles.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := cne/Nexus/ROW
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/cne/Nexus/ROW/ROW_profiles.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libGLESv2_adreno
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/egl/libGLESv2_adreno.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/egl/libGLESv2_adreno.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libdl libgsl libc libz libcutils liblog libllvm-glnext libadreno_utils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libdl libgsl libc libz libcutils liblog libllvm-glnext libadreno_utils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Perfdump
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/Perfdump/Perfdump.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_a3_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_a3_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqdi
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqdi.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqdi.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libqmi libdiag libqmi_client_helper libqmi_client_qmux libdl liblog libqmiservices libdsutils libqmi_cci libqmi_common_so
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libqmi libdiag libqmi_client_helper libqmi_client_qmux libdl liblog libqmiservices libdsutils libqmi_cci libqmi_common_so
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov8856
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov8856.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := TrustZoneAccessService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := vendor/app/TrustZoneAccessService/TrustZoneAccessService.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_dw9763
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_dw9763.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QtiTelephonyService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/QtiTelephonyService/QtiTelephonyService.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := diag_dci_sample
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/diag_dci_sample
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := PowerOffAlarm
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := vendor/app/PowerOffAlarm/PowerOffAlarm.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := hdcp1prov
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hdcp1prov
LOCAL_SHARED_LIBRARIES := libhdcp1prov libc++ libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libGPTEE_vendor
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libGPTEE_vendor.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libGPTEE_vendor.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.soter-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.soter-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_hfr_90_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_hfr_90_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qti-telephony-common
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/qti-telephony-common.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CarrierSwitch
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/CarrierSwitch/CarrierSwitch.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx318
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx318.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wigig_wiburn
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/wigig_wiburn
LOCAL_SHARED_LIBRARIES := libc++ libwigig_utils libwigig_pciaccess libm libc libwigig_flashaccess libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_lsensor.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_lsensor.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cneapiclient
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/cneapiclient.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qcom-system-daemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qcom-system-daemon
LOCAL_SHARED_LIBRARIES := libc++ libsubsystem_control libm libc libxml2 libdiag libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.tui_comm@1.0-service-qti
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.qti.hardware.tui_comm@1.0-service-qti
LOCAL_SHARED_LIBRARIES := libhidlbase libbase vendor.qti.hardware.tui_comm@1.0 libc++ libm libc libhwbinder libhidltransport libbinder libutils libdl libcutils liblog libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_zsl_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_zsl_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_snapshot_downscale
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_snapshot_downscale.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_cac47
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_cac47.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := irsc_util
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/irsc_util
LOCAL_SHARED_LIBRARIES := libc++ libc liblog libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_memory.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_memory.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libacdb-fts
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libacdb-fts.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libacdb-fts.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libaudcal libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libaudcal libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sap.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/sap.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_cpp_module
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_cpp_module.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera2_pp_buf_mgr
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveObjectTracker_stub
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscveObjectTracker_stub.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscveObjectTracker_stub.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libadsprpc libm libfastcvopt libc libdl liblog libscveCommon
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libadsprpc libm libfastcvopt libc libdl liblog libscveCommon
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cnss-daemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/cnss-daemon
LOCAL_SHARED_LIBRARIES := libc++ libm libc libnl libcld80211 libdl libcutils liblog libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libGPreqcancel_svc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libGPreqcancel_svc.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libGPreqcancel_svc.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libqti_vraudio_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/mm-audio/audio-include
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_rohm_brcg064gwz_3_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_rohm_brcg064gwz_3_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := xtwifi.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/xtwifi.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sunny_8865_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sunny_8865_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_us_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_us_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libavenhancements
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libavenhancements.so
LOCAL_SRC_FILES_arm64 := system/lib64/libavenhancements.so
LOCAL_SHARED_LIBRARIES_arm := libmediaplayerservice libc++ libmedia_omx libmedia libexpat libaudioclient libstagefright_httplive libutils libcrypto libdl libaudioutils libbinder libcamera_client libc android.hidl.memory@1.0 libstagefright libhidlbase libm android.hidl.allocator@1.0 libmediaextractor libstagefright_foundation libz liblog libnativewindow libhidlmemory libmedia_helper libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libmediaplayerservice libc++ libmedia_omx libmedia libexpat libaudioclient libstagefright_httplive libutils libcrypto libdl libaudioutils libbinder libcamera_client libc android.hidl.memory@1.0 libstagefright libhidlbase libm android.hidl.allocator@1.0 libmediaextractor libstagefright_foundation libz liblog libnativewindow libhidlmemory libmedia_helper libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdavenhancements
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdavenhancements.so
LOCAL_SHARED_LIBRARIES := libc++ libui libmedia libm libgui libmediaextractor libbinder libmediautils libstagefright_foundation libutils libc liblog libdl libstagefright
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.soter@1.0-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.qti.hardware.soter@1.0-service
LOCAL_INIT_RC       := vendor/etc/init/vendor.qti.hardware.soter@1.0-service.rc
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl liblog vendor.qti.hardware.soter@1.0
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_liveshot_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_liveshot_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_onsemi_cat24c32_imx362_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_onsemi_cat24c32_imx362_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := audiosphere.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/audiosphere.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqcmaputils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqcmaputils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqcmaputils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libdl libdsutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libdl libdsutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_4k_video_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_4k_video_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_clamp_encoder40
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_clamp_encoder40.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := display_proprietary_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/display/includes
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := xdivert
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/xdivert/xdivert.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsdm-disp-vndapis
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsdm-disp-vndapis.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsdm-disp-vndapis.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libutils libm libc libsdmutils libbinder libqservice libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libutils libm libc libsdmutils libbinder libqservice libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveObjectTracker
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscveObjectTracker.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscveObjectTracker.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libfastcvopt libc libdl liblog libscveCommon libscveObjectSegmentation
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libfastcvopt libc libdl liblog libscveCommon libscveObjectSegmentation
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_mesh_rolloff44
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_mesh_rolloff44.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libcdsprpc_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libcdsprpc_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libcdsprpc_system.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sns_cm_conc_test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/sns_cm_conc_test
LOCAL_SHARED_LIBRARIES := libc++ libc libsensor1 libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hfr_60_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hfr_60_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdspCV_skel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libdspCV_skel.so
LOCAL_SHARED_LIBRARIES := 
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530_zap.b01
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530_zap.b01
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_video_hdr_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_video_hdr_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sns_hal_batch
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/sns_hal_batch
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_zsl_video_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_zsl_video_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qti_permissions.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/qti_permissions.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_gic46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_gic46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_ihist_stats46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_ihist_stats46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_csidtg_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_csidtg_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QFPCalibration
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := vendor/app/QFPCalibration/QFPCalibration.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := izat.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/izat.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_default_video_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_default_video_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CtRoamingSettings
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/CtRoamingSettings/CtRoamingSettings.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libidl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libidl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libidl.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530_gpmu.fw2
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530_gpmu.fw2
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov5695
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov5695.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libxml
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libxml.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libxml.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qcrilhook.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/qcrilhook.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_hfr_60_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_hfr_60_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_snapshot_downscale
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_snapshot_downscale.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.display.color@1.0-service.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/init/vendor.display.color@1.0-service.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsdm-diag
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsdm-diag.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsdm-diag.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libsdmutils libm libc libdiag libbinder libdl
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libsdmutils libm libc libdiag libbinder libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_default_video_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_default_video_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx258_mono_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx258_mono_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_imglib_modules
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_imglib_modules.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils liblog libmmcamera_imglib
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libfastcvopt
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libfastcvopt.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libfastcvopt.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl liblog libOpenCL libion
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl liblog libOpenCL libion
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsystem_health_mon
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsystem_health_mon.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsystem_health_mon.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := tftp_server
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/tftp_server
LOCAL_SHARED_LIBRARIES := libc++ libm libc libqsocket libqrtr liblog libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdiagbridge
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib64/libdiagbridge.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libgps.utils libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_dw9761b_2d_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_dw9761b_2d_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqfp-service
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib64/libqfp-service.so
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libhidltransport android.hardware.biometrics.fingerprint@2.1 libm libc libdiag libutils libdl libcutils liblog vendor.qti.hardware.fingerprint@1.0 libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libQSEEComAPIStatic
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 32
LOCAL_SRC_FILES     := obj_arm/STATIC_LIBRARIES/libQSEEComAPIStatic_intermediates/libQSEEComAPIStatic.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.factory@1.0-impl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/vendor.qti.hardware.factory@1.0-impl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/vendor.qti.hardware.factory@1.0-impl.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libc++ libm libc libhidltransport libutils libdl libcutils liblog vendor.qti.hardware.factory@1.0 libhardware
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libc++ libm libc libhidltransport libutils libdl libcutils liblog vendor.qti.hardware.factory@1.0 libhardware
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_4k_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_4k_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmparser_lite
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libmmparser_lite.so
LOCAL_SRC_FILES_arm64 := system/lib64/libmmparser_lite.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog libmmosal
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_default_video_dw9761b
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_default_video_dw9761b.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_us_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_us_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libfastcvadsp
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libfastcvadsp.so
LOCAL_SHARED_LIBRARIES := 
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_zsl_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_zsl_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdpmfdmgr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libdpmfdmgr.so
LOCAL_SRC_FILES_arm64 := system/lib64/libdpmfdmgr.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc com.qualcomm.qti.dpm.api@1.0 libbinder libdpmframework libutils libdl libcutils libdiag_system
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc com.qualcomm.qti.dpm.api@1.0 libbinder libdpmframework libutils libdl libcutils libdiag_system
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := thermal-engine
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/thermal-engine
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libqmi_cci libqmi_common_so libthermalioctl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_1080p_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_1080p_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxMux
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libOmxMux.so
LOCAL_SRC_FILES_arm64 := system/lib64/libOmxMux.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libFileMux libm libc libbinder libutils libdl liblog libmmosal
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libFileMux libm libc libbinder libutils libdl liblog libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_zsl_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_zsl_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libvpphvx
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libvpphvx.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libvpphvx.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libadsprpc libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libadsprpc libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmrtpdecoder
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libmmrtpdecoder.so
LOCAL_SRC_FILES_arm64 := system/lib64/libmmrtpdecoder.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog libmmosal
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libSeemplog
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libSeemplog.so
LOCAL_SRC_FILES_arm64 := system/lib64/libSeemplog.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_pseudo
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_pseudo.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hfr_120_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hfr_120_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qfp-daemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qfp-daemon
LOCAL_SHARED_LIBRARIES := libc++ android.hardware.biometrics.fingerprint@2.1 libm libc libhidltransport libqfp-service libutils libdl libcutils liblog vendor.qti.hardware.fingerprint@1.0
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libQTEEConnector_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/securemsm/QTEEConnector/include
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_default_preview_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_default_preview_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libjpegehw
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libjpegehw.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_fullsize_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_fullsize_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Fluid_Handset_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Fluid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Fluid/Fluid_Handset_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov5670_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov5670_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdrmMinimalfs
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 32
LOCAL_SRC_FILES     := obj_arm/STATIC_LIBRARIES/libdrmMinimalfs_intermediates/libdrmMinimalfs.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_fullsize_preview_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_fullsize_preview_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := lib_soundmodel_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/mm-audio/audio-include
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := a330_pfp.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a330_pfp.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Fluid_Hdmi_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Fluid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Fluid/Fluid_Hdmi_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libqdp_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/qcril-data-hal/qdp
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k3m2xx_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k3m2xx_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := icm
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/icm
LOCAL_SHARED_LIBRARIES := libc++ libm libc libnl libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_snr47
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_snr47.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsdsprpc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsdsprpc.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsdsprpc.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hfr_120_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hfr_120_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_us_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_us_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := remoteSimLockAuthentication
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/remoteSimLockAuthentication/remoteSimLockAuthentication.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qdss.functions.sh
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/persist/coresight
LOCAL_SRC_FILES     := persist/coresight/qdss.functions.sh
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_s5k3m2xm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_s5k3m2xm.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530_pfp.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530_pfp.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxApeDecSw
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxApeDecSw.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxApeDecSw.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_hfr_90_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_hfr_90_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mlid
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mlid
LOCAL_SHARED_LIBRARIES := libc++ liblowi_client libm libc libdl libcutils
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_hfr_120_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_hfr_120_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_zsl_video_dw9761b
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_zsl_video_dw9761b.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_raw_hdr_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_raw_hdr_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_snapshot_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_snapshot_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ATT_profiles.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := cne/Nexus/ATT
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/cne/Nexus/ATT/ATT_profiles.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sns_regedit_ssi
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/sns_regedit_ssi
LOCAL_SHARED_LIBRARIES := libc++ libm libc libsensor1 libdl libcutils
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmdsprpc_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libmdsprpc_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libmdsprpc_system.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := main.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/main.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_common_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_common_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx258_gt24c16_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx258_gt24c16_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sensor_def_intrinsyc.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := sensors
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/sensors/sensor_def_intrinsyc.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-qjpeg-enc-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-qjpeg-enc-test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmqjpeg_codec
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_4k_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_4k_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_cs_stats46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_cs_stats46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx214_klt
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx214_klt.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ssgtzd
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/ssgtzd
LOCAL_SHARED_LIBRARIES := libminksocket libc liblog libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hdr_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hdr_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdrplugin_client
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdrplugin_client.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdrplugin_client.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libutils libdl liblog libloc_core liblbs_core
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libutils libdl liblog libloc_core liblbs_core
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_1_1.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_1_1.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov5670
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov5670.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_hfr_60_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_hfr_60_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_pproc_modules
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_pproc_modules.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_eztune_module libmmcamera2_mct libmmcamera2_cpp_module libmmcamera_dbg libc libm libmmcamera_ppbase_module libdl libcutils liblog libmmcamera2_imglib_modules libmmcamera2_c2d_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_ds_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_ds_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530v1_pfp.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530v1_pfp.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsensor1
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsensor1.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsensor1.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libqmi_encdec libutils libdl libcutils liblog libqmi_cci libqmi_common_so
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libqmi_encdec libutils libdl libcutils liblog libqmi_cci libqmi_common_so
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmi_csvt_srvc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib64/libqmi_csvt_srvc.so
LOCAL_SHARED_LIBRARIES := libc++ libm libqcci_legacy libc libqmi_client_qmux libdiag libdl libcutils liblog libqmiservices libqmi
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_pedestal_correct46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_pedestal_correct46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hdr_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hdr_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.scve.objecttracker@1.0-adapter-helper
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/vendor.qti.hardware.scve.objecttracker@1.0-adapter-helper.so
LOCAL_SRC_FILES_arm64 := system/lib64/vendor.qti.hardware.scve.objecttracker@1.0-adapter-helper.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libm libc libhwbinder libhidltransport libutils libdl libcutils liblog libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libm libc libhwbinder libhidltransport libutils libdl libcutils liblog libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := uimremoteserverlibrary
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/uimremoteserverlibrary.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_cpp_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_cpp_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.alarm-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.alarm-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_power.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_power.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libkeymasterutils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libkeymasterutils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libkeymasterutils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libcrypto libm libc libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libcrypto libm libc libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_video_hdr_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_video_hdr_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_nfc.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_nfc.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := tbaseLoader
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/tbaseLoader
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog libQSEEComAPI
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_linearization40
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_linearization40.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := lib_remote_simlock
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/lib_remote_simlock.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/lib_remote_simlock.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libdl liblog libqmiservices libqmi_cci
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libdl liblog libqmiservices libqmi_cci
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := FidoCryptoService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/FidoCryptoService/FidoCryptoService.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ftm_test_config
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/ftm_test_config
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_default_preview_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_default_preview_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_common_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_common_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_zsl_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_zsl_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_default_video_dw9714
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_default_video_dw9714.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k3m2xm_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k3m2xm_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov7251_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov7251_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libnetmgr_nr_fusion
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libnetmgr_nr_fusion.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libnetmgr_nr_fusion.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libconfigdb libm libc libdiag libdl libcutils liblog libnetmgr libnetmgr_common libqmiservices libdsutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libconfigdb libm libc libdiag libdl libcutils liblog libnetmgr libnetmgr_common libqmiservices libdsutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_hfr_90_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_hfr_90_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.bluetooth@1.0-impl-qti
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/android.hardware.bluetooth@1.0-impl-qti.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/android.hardware.bluetooth@1.0-impl-qti.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libdiag libm android.hardware.bluetooth@1.0 libhidltransport libutils libc libcutils liblog libdl libqmiservices libqmi_cci libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libdiag libm android.hardware.bluetooth@1.0 libhidltransport libutils libc libcutils liblog libdl libqmiservices libqmi_cci libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmm-hdcpmgr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmm-hdcpmgr.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmm-hdcpmgr.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_4_0.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_4_0.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_led_red.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_led_red.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libril-qc-hal-qmi
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libril-qc-hal-qmi.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libril-qc-hal-qmi.so
LOCAL_SHARED_LIBRARIES_arm := libc++ android.hardware.radio.deprecated@1.0 libdsi_netctrl libxml2 libhidltransport libqdp libperipheral_client libmdmdetect libqmi_client_helper libutils libdl vendor.qti.hardware.radio.ims@1.4 vendor.qti.hardware.radio.ims@1.0 vendor.qti.hardware.radio.ims@1.1 vendor.qti.hardware.radio.ims@1.2 vendor.qti.hardware.radio.ims@1.3 librilqmiservices libm libbinder libcutils android.hardware.secure_element@1.0 vendor.qti.hardware.radio.lpa@1.0 libc libril-qc-radioconfig libril-qcril-hook-oem vendor.qti.hardware.radio.uim_remote_client@1.0 vendor.qti.hardware.radio.uim@1.0 vendor.qti.hardware.radio.uim@1.1 vendor.qti.hardware.radio.uim_remote_server@1.0 libhidlbase libdiag vendor.qti.hardware.radio.qtiradio@1.0 android.hardware.radio@1.2 libhardware_legacy libidl libhwbinder libqmi android.hardware.radio@1.0 qcrild_librilutils libqmi_client_qmux libsqlite liblog vendor.qti.hardware.radio.am@1.0 libdsutils libqmi_cci libqmiservices android.hardware.radio@1.1 libril-qc-ltedirectdisc libtime_genoff liblqe libqcrilFramework android.hardware.radio.config@1.0 vendor.qti.hardware.radio.qtiradio@2.0 vendor.qti.hardware.radio.qcrilhook@1.0 libsystem_health_mon libsettings
LOCAL_SHARED_LIBRARIES_arm64 := libc++ android.hardware.radio.deprecated@1.0 libdsi_netctrl libxml2 libhidltransport libqdp libperipheral_client libmdmdetect libqmi_client_helper libutils libdl vendor.qti.hardware.radio.ims@1.4 vendor.qti.hardware.radio.ims@1.0 vendor.qti.hardware.radio.ims@1.1 vendor.qti.hardware.radio.ims@1.2 vendor.qti.hardware.radio.ims@1.3 librilqmiservices libm libbinder libcutils android.hardware.secure_element@1.0 vendor.qti.hardware.radio.lpa@1.0 libc libril-qc-radioconfig libril-qcril-hook-oem vendor.qti.hardware.radio.uim_remote_client@1.0 vendor.qti.hardware.radio.uim@1.0 vendor.qti.hardware.radio.uim@1.1 vendor.qti.hardware.radio.uim_remote_server@1.0 libhidlbase libdiag vendor.qti.hardware.radio.qtiradio@1.0 android.hardware.radio@1.2 libhardware_legacy libidl libhwbinder libqmi android.hardware.radio@1.0 qcrild_librilutils libqmi_client_qmux libsqlite liblog vendor.qti.hardware.radio.am@1.0 libdsutils libqmi_cci libqmiservices android.hardware.radio@1.1 libril-qc-ltedirectdisc libtime_genoff liblqe libqcrilFramework android.hardware.radio.config@1.0 vendor.qti.hardware.radio.qtiradio@2.0 vendor.qti.hardware.radio.qcrilhook@1.0 libsystem_health_mon libsettings
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsdmextension
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsdmextension.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsdmextension.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libsdmutils libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libsdmutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_default_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_default_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libprmediadrmdecrypt_customer
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := qcdrm/playready/lib/mediadrm
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/qcdrm/playready/lib/mediadrm/libprmediadrmdecrypt_customer.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libbinder libutils libz libcutils liblog libdl libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cnss_diag
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/cnss_diag
LOCAL_SHARED_LIBRARIES := libc++ libdiag libm libhardware_legacy libc libnl libcld80211 libdl libcutils liblog libtime_genoff
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libtzdrmgenprov
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libtzdrmgenprov.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libtzdrmgenprov.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libcsd-client
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libcsd-client.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libcsd-client.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libacdbloader libm libc libmdmdetect libutils libdl libcutils liblog libqmi_cci libqmi_common_so
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libacdbloader libm libc libmdmdetect libutils libdl libcutils liblog libqmi_cci libqmi_common_so
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_faceproc2
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_faceproc2.so
LOCAL_SHARED_LIBRARIES := libc++ libdl libc libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx258_mono_bear_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx258_mono_bear_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libril-qc-hal-ims-headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/qcril-hal/modules/ims
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_thread_services
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_thread_services.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := myftm
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/myftm
LOCAL_SHARED_LIBRARIES := libc++ libm libc libnl libz libcutils libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_zsl_video_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_zsl_video_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_snapshot_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_snapshot_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libtinyxml2_1
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libtinyxml2_1.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libtinyxml2_1.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_default_preview_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_default_preview_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libloc2jnibridge
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib64/libloc2jnibridge.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libgps.utils libutils libdl liblog libloc_api_v02
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_default_preview_dw9761b_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_default_preview_dw9761b_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libkeymasterprovision
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libkeymasterprovision.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libkeymasterprovision.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libkeymasterutils libcrypto libm libc libdl liblog libkeymasterdeviceutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libkeymasterutils libcrypto libm libc libdl liblog libkeymasterdeviceutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libnetmgr_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libnetmgr_common.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libnetmgr_common.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libconfigdb libm libc libdiag libdl libcutils liblog libnetmgr libqmiservices libdsutils libqmi_cci
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libconfigdb libm libc libdiag libdl libcutils liblog libnetmgr libqmiservices libdsutils libqmi_cci
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx230_qc2002_with_gyro_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx230_qc2002_with_gyro_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libFidoCryptoJNI
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libFidoCryptoJNI.so
LOCAL_SRC_FILES_arm64 := system/lib64/libFidoCryptoJNI.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libFidoCrypto_system libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libFidoCrypto_system libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.perf@1.0-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.qti.hardware.perf@1.0-service
LOCAL_INIT_RC       := vendor/etc/init/vendor.qti.hardware.perf@1.0-service.rc
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libperfgluelayer libc++ libm libhardware_legacy libc libhwbinder vendor.qti.hardware.perf@1.0 libhidltransport libutils libdl libcutils liblog libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_120_dw9761b
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_120_dw9761b.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_hfr_120_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_hfr_120_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CmccSystemUIRes
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_MODULE_PATH   := $(TARGET_OUT_VENDOR)/overlay
LOCAL_SRC_FILES     := vendor/overlay/CmccSystemUITheme/CmccSystemUIRes.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_hfr_90_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_hfr_90_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_zsl_preview_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_zsl_preview_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_hfr_240_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_hfr_240_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdhaldsmanager
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libwfdhaldsmanager.so
LOCAL_SHARED_LIBRARIES := libhidlbase libmmosal_proprietary libm libc libutils libdl liblog com.qualcomm.qti.wifidisplayhal@1.0 libwfdmmservice libc++ libwfdcommonutils_proprietary
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_hfr_120_bu64297
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_hfr_120_bu64297.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_csidtg
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_csidtg.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := adsprpcd
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/adsprpcd
LOCAL_SHARED_LIBRARIES := libc++ libc liblog libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := csidtg_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/csidtg_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_default_video_dw9761b_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_default_video_dw9761b_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_default_video_bu64297
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_default_video_bu64297.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_zsl_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_zsl_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CallEnhancement
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/CallEnhancement/CallEnhancement.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblistensoundmodel2
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/liblistensoundmodel2.so
LOCAL_SRC_FILES_arm64 := system/lib64/liblistensoundmodel2.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := time_daemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/time_daemon
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_mt_client_init_instance
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_mt_client_init_instance
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov8858
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov8858.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := diag_klog
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/diag_klog
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdsm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdsm.so
LOCAL_SHARED_LIBRARIES := libc++ libmedia libhidltransport libutils libwfdmmsink libwfdmmsrc libwfdmminterface libqdMetaData.system libdl libmmrtpencoder libmmosal libwfduibcinterface libhidlbase libm libc libwfdconfigutils libwfdrtsp liblog com.qualcomm.qti.wifidisplayhal@1.0 libnativewindow libmmrtpdecoder libcutils libwfdcommonutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := rtspclient
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/rtspclient
LOCAL_SHARED_LIBRARIES := libc++ libm libc libwfdrtsp libdl libwfdcommonutils libmmosal
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_a3_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_a3_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmosal
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libmmosal.so
LOCAL_SRC_FILES_arm64 := system/lib64/libmmosal.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_60_dw9761b_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_60_dw9761b_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.imscmservice.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qualcomm.qti.imscmservice.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := xtra_t_app
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/xtra_t_app/xtra_t_app.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := lowi_test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/lowi_test
LOCAL_SHARED_LIBRARIES := libc++ libm liblowi_client libc libxml2 libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qti.dpmframework.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qti.dpmframework.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.voiceprint-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.voiceprint-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libril-qc-radioconfig
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libril-qc-radioconfig.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libril-qc-radioconfig.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_hfr_90_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_hfr_90_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_dw9761b
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_dw9761b.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := librilqmiservices
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/librilqmiservices.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/librilqmiservices.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libqmi_common_so libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libqmi_common_so libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.ims.callinfo-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.ims.callinfo-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscvePanorama
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscvePanorama.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscvePanorama.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libfastcvopt libjpeg libc liblog libdl libOpenCL libscveBlobDescriptor libscveCommon
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libfastcvopt libjpeg libc liblog libdl libOpenCL libscveBlobDescriptor libscveCommon
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_clnt_test_0001
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_clnt_test_0001
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_clnt_test_0000
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_clnt_test_0000
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530_pm4.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530_pm4.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := strings-zh-rCN.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/strings-zh-rCN.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdnative
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libwfdnative.so
LOCAL_SRC_FILES_arm64 := system/lib64/libwfdnative.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libhidlmemory libui android.hidl.base@1.0 libm android.hidl.token@1.0-utils android.hardware.graphics.common@1.0 libandroidfw libhidltransport libutils libc libnativehelper libdl libandroid_runtime libinput android.hardware.graphics.bufferqueue@1.0 liblog libc++ libwfdclient
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libhidlmemory libui android.hidl.base@1.0 libm android.hidl.token@1.0-utils android.hardware.graphics.common@1.0 libandroidfw libhidltransport libutils libc libnativehelper libdl libandroid_runtime libinput android.hardware.graphics.bufferqueue@1.0 liblog libc++ libwfdclient
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqtigef
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqtigef.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqtigef.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_preview_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_preview_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.constants-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.quicinc.cne.constants-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_zsl_video_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_zsl_video_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wfdconfigsink.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/wfdconfigsink.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sensorrdiag
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/sensorrdiag
LOCAL_SHARED_LIBRARIES := libc++ libm libc libsensor1 libdiag libdl libqmi_encdec liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_hfr_120_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_hfr_120_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx258_gt24c32_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx258_gt24c32_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqdp
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqdp.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqdp.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libqmi_client_helper libdl libcutils libqmiservices libdsutils libqmi_cci libqmi_common_so
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libqmi_client_helper libdl libcutils libqmiservices libdsutils libqmi_cci libqmi_common_so
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_zsl_video_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_zsl_video_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_snapshot_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_snapshot_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_fw.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/qmi_fw.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov8858_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov8858_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libkeymasterdeviceutils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libkeymasterdeviceutils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libkeymasterdeviceutils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libcrypto libm libc libdl libcutils liblog libQSEEComAPI libion
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libcrypto libm libc libdl libcutils liblog libQSEEComAPI libion
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_video_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_video_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := RemoteSimlock.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/RemoteSimlock.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_faceproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_faceproc.so
LOCAL_SHARED_LIBRARIES := libc++ libdl libc libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := camera_config.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/camera_config.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := radioconfig
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/radioconfig/radioconfig.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_default_preview_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_default_preview_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdcommonutils_proprietary
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libwfdcommonutils_proprietary.so
LOCAL_SHARED_LIBRARIES := libc++ libmmosal_proprietary libm libc libutils libdl libcutils liblog libion
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdmodulehdcpsession
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libwfdmodulehdcpsession.so
LOCAL_SHARED_LIBRARIES := libhidlbase libmmosal_proprietary libm libwfdhdcpcp libc libutils libdl liblog com.qualcomm.qti.wifidisplayhal@1.0 libc++ libwfdcommonutils_proprietary
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.keymaster@3.0-service-qti.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/init/android.hardware.keymaster@3.0-service-qti.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_tuning_lookup
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_tuning_lookup.so
LOCAL_SHARED_LIBRARIES := libc++ libm libfastcvopt libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := xtra_root_cert.pem
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/xtra_root_cert.pem
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_a3_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_a3_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sns_cm_test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/sns_cm_test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libsensor1 libutils libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_hfr_60_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_hfr_60_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := OptInAppOverlay
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_MODULE_PATH   := $(TARGET_OUT_VENDOR)/overlay
LOCAL_SRC_FILES     := vendor/overlay/OptInAppOverlay/OptInAppOverlay.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_hfr_60_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_hfr_60_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libApeSwDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libApeSwDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libApeSwDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc liblog libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc liblog libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.keymaster@3.0-impl-qti
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/android.hardware.keymaster@3.0-impl-qti.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/android.hardware.keymaster@3.0-impl-qti.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libcrypto libm android.hardware.keymaster@3.0 libc libhwbinder libhidltransport libutils libdl libcutils liblog libkeymasterdeviceutils
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libcrypto libm android.hardware.keymaster@3.0 libc libhwbinder libhidltransport libutils libdl libcutils liblog libkeymasterdeviceutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmi_cci_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libqmi_cci_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libqmi_cci_system.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hfr_60_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hfr_60_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.gatekeeper@1.0-service-qti
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/android.hardware.gatekeeper@1.0-service-qti
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl liblog android.hardware.gatekeeper@1.0 libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := btnvtool
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/btnvtool
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libbtnv
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdpmframework
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libdpmframework.so
LOCAL_SRC_FILES_arm64 := system/lib64/libdpmframework.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libhardware_legacy libc libhwbinder com.qualcomm.qti.dpm.api@1.0 libhidltransport libbinder libutils libdl libcutils liblog libhardware libdiag_system
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libhardware_legacy libc libhwbinder com.qualcomm.qti.dpm.api@1.0 libhidltransport libbinder libutils libdl libcutils liblog libhardware libdiag_system
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libapps_mem_heap
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libapps_mem_heap.so
LOCAL_SHARED_LIBRARIES := libc
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qrd.wappush.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qrd.wappush.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sonyimx135_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sonyimx135_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ModemTestMode
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/ModemTestMode/ModemTestMode.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_dw9716
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_dw9716.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_4K_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_4K_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov2680
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov2680.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.api-V1.1-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.quicinc.cne.api-V1.1-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsdm-color
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsdm-color.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsdm-color.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libtinyxml2_1 libsdmutils libbinder libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libtinyxml2_1 libsdmutils libbinder libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqisl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqisl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqisl.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdhdcpcp
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libwfdhdcpcp.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libwfdhdcpcp.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libmmosal_proprietary libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libmmosal_proprietary libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmi_encdec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqmi_encdec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqmi_encdec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libqmi_common_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/commonsys-intf/qmi-framework/common/src
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := hal_proxy_daemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hal_proxy_daemon
LOCAL_SHARED_LIBRARIES := libc++ libwifi-hal-qcom libm libhardware_legacy libc libnl libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfduibcsrc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfduibcsrc.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog libwfdcommonutils libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sensors_settings
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/persist/sensors
LOCAL_SRC_FILES     := persist/sensors/sensors_settings
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wcnss_filter
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/wcnss_filter
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libutils libdl libcutils liblog libbtnv
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdsd2pcm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libdsd2pcm.so
LOCAL_SHARED_LIBRARIES := libc++ libc liblog libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qti.location.sdk
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.qti.location.sdk.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530v3_seq.fw2
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530v3_seq.fw2
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := UxPerformance
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/UxPerformance.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_raw_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_raw_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ppbase_module
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ppbase_module.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_4k_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_4k_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_gps.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_gps.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_1_6.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_1_6.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libfastcvadsp_stub
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libfastcvadsp_stub.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libfastcvadsp_stub.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libadsprpc libm libc libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libadsprpc libm libc libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_hfr_60_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_hfr_60_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QSensorTest
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := vendor/app/QSensorTest/QSensorTest.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwigig_utils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libwigig_utils.so
LOCAL_SRC_FILES_arm64 := system/lib64/libwigig_utils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := datastatusnotification
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/datastatusnotification/datastatusnotification.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_hdr_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_hdr_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_default_preview_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_default_preview_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libminksocket
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libminksocket.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libminksocket.so
LOCAL_SHARED_LIBRARIES_arm := libc libdl
LOCAL_SHARED_LIBRARIES_arm64 := libc libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := uimremoteserver
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/uimremoteserver/uimremoteserver.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k3l8_f3l8yam_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k3l8_f3l8yam_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_simple_ril_test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi_simple_ril_test
LOCAL_SHARED_LIBRARIES := libc++ libm libqcci_legacy libidl libqmi libc libcutils liblog libdl libqmiservices libqmi_cci libqmi_client_qmux
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwigig_flashaccess
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libwigig_flashaccess.so
LOCAL_SRC_FILES_arm64 := system/lib64/libwigig_flashaccess.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libwigig_utils libwigig_pciaccess libm libc libdl
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libwigig_utils libwigig_pciaccess libm libc libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := remotesimlockservice
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/remotesimlockservice/remotesimlockservice.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_default_video_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_default_video_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QtiSystemService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/QtiSystemService/QtiSystemService.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_hdr_video_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_hdr_video_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.constants@1.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.quicinc.cne.constants@1.0.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.quicinc.cne.constants@1.0.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_aec_bg_stats47
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_aec_bg_stats47.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdpmctmgr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libdpmctmgr.so
LOCAL_SRC_FILES_arm64 := system/lib64/libdpmctmgr.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc com.qualcomm.qti.dpm.api@1.0 libbinder libdpmframework libutils libdl libcutils libdiag_system
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc com.qualcomm.qti.dpm.api@1.0 libbinder libdpmframework libutils libdl libcutils libdiag_system
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_tintless_bg_pca_algo
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmmcamera_tintless_bg_pca_algo.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmmcamera_tintless_bg_pca_algo.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libmmcamera_dbg libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libmmcamera_dbg libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_clnt_test_3000
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_clnt_test_3000
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_encdec libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_clnt_test_3001
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_clnt_test_3001
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libqmi_encdec libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_emmc.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_emmc.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vulkan.msm8996
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/vulkan.msm8996.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/vulkan.msm8996.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgsl libz libcutils liblog libdl libllvm-glnext libadreno_utils libhardware libnativewindow
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgsl libz libcutils liblog libdl libllvm-glnext libadreno_utils libhardware libnativewindow
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmparser
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := extractors
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/extractors/libmmparser.so
LOCAL_SRC_FILES_arm64 := system/lib64/extractors/libmmparser.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libmediaextractor libmmparser_lite libstagefright_foundation libutils libdl libcutils liblog libmmosal libstagefright
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libmediaextractor libmmparser_lite libstagefright_foundation libutils libdl libcutils liblog libmmosal libstagefright
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmdmimgload
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib64/libmdmimgload.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.server-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.quicinc.cne.server-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_video_4k_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_video_4k_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libfastcrc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libfastcrc.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libfastcrc.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := qcrilDataModule_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/qcril-data-hal/datamodule
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sound_trigger.primary.msm8996
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/sound_trigger.primary.msm8996.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/sound_trigger.primary.msm8996.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libaudioroute libm libc libexpat libtinyalsa libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libaudioroute libm libc libexpat libtinyalsa libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_hfr_60_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_hfr_60_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_cpp_ds_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_cpp_ds_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := footer.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/footer.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdservice
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdservice.so
LOCAL_SHARED_LIBRARIES := libc++ libm libgui libwfdconfigutils libbinder libwfdsm libutils libwfdmminterface libcutils liblog libdl libwfdcommonutils libinput libmmosal libc
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libqti-utils_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/common/qti-utils
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_default_video_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_default_video_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_hfr_60_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_hfr_60_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libizat_client_api
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libizat_client_api.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libizat_client_api.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_pdaf
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_pdaf.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_zsl_preview_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_zsl_preview_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdmmservice
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libwfdmmservice.so
LOCAL_SHARED_LIBRARIES := libc++ libmmosal_proprietary libm libc libbinder libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_fullsize_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_fullsize_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxQcelp13Dec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxQcelp13Dec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxQcelp13Dec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov2680_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov2680_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Fluid_Global_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Fluid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Fluid/Fluid_Global_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libqmi_encdec_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/commonsys-intf/qmi-framework/encdec
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_a3_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_a3_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QRD_Handset_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/QRD
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/QRD/QRD_Handset_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sunny_q13v06k_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sunny_q13v06k_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsprpc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libadsprpc.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libadsprpc.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := poweroff.png
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/poweroff.png
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libgarden
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libgarden.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libgarden.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ liblocation_api libm libc libhidltransport libgps.utils libutils libdl liblog android.hardware.gnss@1.0 android.hardware.gnss@1.1 libloc_core
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ liblocation_api libm libc libhidltransport libgps.utils libutils libdl liblog android.hardware.gnss@1.0 android.hardware.gnss@1.1 libloc_core
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_240_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_240_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libxtadapter
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libxtadapter.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libxtadapter.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libdl liblog libloc_core libizat_core
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libdl liblog libloc_core libizat_core
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdcodecv4l2
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdcodecv4l2.so
LOCAL_SHARED_LIBRARIES := libc++ libm libwfdmminterface libutils libc libcutils liblog libdl libwfdcommonutils libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hfr_60_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hfr_60_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.esepowermanager@1.0-impl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/vendor.qti.esepowermanager@1.0-impl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/vendor.qti.esepowermanager@1.0-impl.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libm libc libhwbinder libhidltransport vendor.qti.esepowermanager@1.0 libutils libdl libcutils liblog libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libm libc libhwbinder libhidltransport vendor.qti.esepowermanager@1.0 libutils libdl libcutils liblog libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libQTEEConnector_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libQTEEConnector_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libQTEEConnector_system.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase vendor.qti.hardware.qteeconnector@1.0 libm libc libhidltransport libutils libdl libcutils liblog libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase vendor.qti.hardware.qteeconnector@1.0 libm libc libhidltransport libutils libdl libcutils liblog libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.server@2.1
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.quicinc.cne.server@2.1.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.quicinc.cne.server@2.1.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.1 libutils libdl libcutils liblog com.quicinc.cne.server@2.0
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.1 libutils libdl libcutils liblog com.quicinc.cne.server@2.0
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.qdutils_disp@1.0-service-qti.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/init/vendor.qti.hardware.qdutils_disp@1.0-service-qti.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdmminterface
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdmminterface.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl liblog com.qualcomm.qti.wifidisplayhal@1.0 libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.data.latency-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.data.latency-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_video_16M_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_video_16M_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdconfigutils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdconfigutils.so
LOCAL_SHARED_LIBRARIES := libc++ libm libwfdmminterface libnl libbinder libutils libc libcutils liblog libdl libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_loudspeaker.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_loudspeaker.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libloc_externalDr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libloc_externalDr.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libloc_externalDr.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libgps.utils libutils libdl liblog libgnsspps libloc_core liblbs_core libloc_externalDrcore
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libgps.utils libutils libdl liblog libgnsspps libloc_core liblbs_core libloc_externalDrcore
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := SimContacts
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/SimContacts/SimContacts.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libril-qc-ltedirectdisc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libril-qc-ltedirectdisc.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libril-qc-ltedirectdisc.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.constants-V2.1-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.quicinc.cne.constants-V2.1-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscalar
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscalar.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscalar.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libsdmutils libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libsdmutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_cpp_ds_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_cpp_ds_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_a3_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_a3_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libC2D2
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libC2D2.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libC2D2.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgsl libutils libdl libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgsl libutils libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_4k_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_4k_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := energy-awareness
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/energy-awareness
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := garden_app
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/garden_app
LOCAL_SHARED_LIBRARIES := libc++ libm libc libgps.utils libdl liblog libloc_core libgarden
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hfr_120_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hfr_120_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := imx214_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/imx214_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qti.location.sdk.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qti.location.sdk.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libDRPlugin
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libDRPlugin.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libDRPlugin.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libutils libdl liblog libgnsspps libloc_core liblbs_core libdrplugin_client
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libutils libdl liblog libgnsspps libloc_core liblbs_core libdrplugin_client
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sns_dsps_tc0001
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/sns_dsps_tc0001
LOCAL_SHARED_LIBRARIES := libc++ libc libsensor1 libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_headset_key.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_headset_key.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_sce40
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_sce40.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblocationservice_glue
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/liblocationservice_glue.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/liblocationservice_glue.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libutils liblocation_api liblbs_core libm libhardware_legacy libc libflp libgps.utils libgeofence libdl libcutils liblog libulp2 libloc_core liblocationservice libdataitems
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libutils liblocation_api liblbs_core libm libhardware_legacy libc libflp libgps.utils libgeofence libdl libcutils liblog libulp2 libloc_core liblocationservice libdataitems
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_a3_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_a3_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_wifi.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_wifi.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qmi_test_service_start_svc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := qmi-framework-tests
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qmi-framework-tests/qmi_test_service_start_svc
LOCAL_SHARED_LIBRARIES := libc++ libm libc libqmi_csi libdl libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := PktRspTest
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/PktRspTest
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libFileMux
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libFileMux.so
LOCAL_SRC_FILES_arm64 := system/lib64/libFileMux.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libmmparser_lite libutils libdl libcutils liblog libmmosal
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libmmparser_lite libutils libdl libcutils liblog libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libacdbrtac
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libacdbrtac.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libacdbrtac.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libaudcal libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libaudcal libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_cpp_us_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_cpp_us_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_zsl_video_dw9761b_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_zsl_video_dw9761b_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxG711Dec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxG711Dec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxG711Dec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_hfr_60_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_hfr_60_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.wifidisplayhal@1.0-service.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/init/com.qualcomm.qti.wifidisplayhal@1.0-service.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsdsprpc_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libsdsprpc_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libsdsprpc_system.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdiagjni
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdiagjni.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdiagjni.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libacdbloader
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libacdbloader.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libacdbloader.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libadiertac libm libc libaudcal libtinyalsa libutils libdl libcutils liblog libacdbrtac libacdb-fts
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libadiertac libm libc libaudcal libtinyalsa libutils libdl libcutils liblog libacdbrtac libacdb-fts
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_cpp_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_cpp_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqtikeymaster4
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqtikeymaster4.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqtikeymaster4.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libkeymasterutils libcrypto libm libc libhwbinder libhidltransport android.hardware.keymaster@4.0 libutils libdl libcutils liblog libkeymasterdeviceutils libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libkeymasterutils libcrypto libm libc libhwbinder libhidltransport android.hardware.keymaster@4.0 libutils libdl libcutils liblog libkeymasterdeviceutils libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := power_off_alarm
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/power_off_alarm
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libdiag_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/commonsys-intf/diag
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_pcba.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_pcba.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sensors.ssc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/sensors.ssc.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/sensors.ssc.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libpower libsensor1 libdiag libsensor_reg libutils libc libcutils liblog libdl libhardware
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libpower libsensor1 libdiag libsensor_reg libutils libc libcutils liblog libdl libhardware
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxVpp
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxVpp.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxVpp.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libbinder libmm-omxcore libvpplibrary libutils libdl libcutils liblog libqdMetaData
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libbinder libmm-omxcore libvpplibrary libutils libdl libcutils liblog libqdMetaData
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsensor_user_cal
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsensor_user_cal.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsensor_user_cal.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libsensor1 libsensor_reg libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libsensor1 libsensor_reg libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_10_0.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_10_0.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libhdcp1prov
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libhdcp1prov.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libhdcp1prov.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.gnss@1.0-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.qti.gnss@1.0-service
LOCAL_INIT_RC       := vendor/etc/init/vendor.qti.gnss@1.0-service.rc
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libm vendor.qti.gnss@1.0 libc libhwbinder libhidltransport libutils libdl libcutils liblog android.hardware.gnss@1.0 android.hardware.gnss@1.1 libc++
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_4K_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_4K_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_hfr_60_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_hfr_60_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := spectraltool
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/spectraltool
LOCAL_SHARED_LIBRARIES := libc++ libm libc libnl libcld80211 libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_hfr_90_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_hfr_90_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := liblocationservice_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/framework/native/lcp
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a300_pm4.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a300_pm4.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_1080p_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_1080p_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_eebinparse
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_eebinparse.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_hdr_snapshot_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_hdr_snapshot_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_bu64297
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_bu64297.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdsi_netctrl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdsi_netctrl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdsi_netctrl.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libconfigdb libm libc libqmi libdiag libqmi_client_helper libqmi_client_qmux libdl libcutils liblog libqdi libqmiservices libdsutils libqmi_cci libqmi_common_so libnetmgr
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libconfigdb libm libc libqmi libdiag libqmi_client_helper libqmi_client_qmux libdl libcutils liblog libqdi libqmiservices libdsutils libqmi_cci libqmi_common_so libnetmgr
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_confirm.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_confirm.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsensor_reg
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsensor_reg.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsensor_reg.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libsensor1 libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libsensor1 libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libasphere
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := soundfx
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/soundfx/libasphere.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/soundfx/libasphere.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_4k_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_4k_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_hfr_90_dw9714
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_hfr_90_dw9714.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_1080p_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_1080p_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wifidisplayhalservice
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/wifidisplayhalservice
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libm libc libhidltransport libbinder com.qualcomm.qti.wifidisplayhal@1.0-halimpl libutils libdl liblog com.qualcomm.qti.wifidisplayhal@1.0
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_4k_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_4k_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libloc_mq_client_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/framework/utils/native/mq_client/src
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libUBWC
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libUBWC.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libUBWC.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libvideoutils libc libcutils liblog libdl
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libvideoutils libc libcutils liblog libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdpmtcm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libdpmtcm.so
LOCAL_SRC_FILES_arm64 := system/lib64/libdpmtcm.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc com.qualcomm.qti.dpm.api@1.0 libbinder libdpmframework libutils libdl libcutils libdiag_system
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc com.qualcomm.qti.dpm.api@1.0 libbinder libdpmframework libutils libdl libcutils libdiag_system
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmulawdec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmulawdec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmulawdec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsp_hvx_skel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libadsp_hvx_skel.so
LOCAL_SHARED_LIBRARIES := 
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_touch.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_touch.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := pm-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/pm-service
LOCAL_SHARED_LIBRARIES := libc++ libm libc libqmi_csi libbinder libqmi_encdec libmdmdetect libperipheral_client libutils libdl libcutils liblog libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_snapshot_downscale_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_snapshot_downscale_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a225_pm4.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a225_pm4.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := pagedown.png
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/pagedown.png
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_s5k3p3sm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_s5k3p3sm.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libcneqmiutils_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/cne/cneQmiUtils
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libCB
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libCB.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libCB.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgsl libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgsl libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libevent_observer_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/utils/eventObserver
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libQSEEComAPI
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libQSEEComAPI.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libQSEEComAPI.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.gatekeeper@1.0-impl-qti
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/android.hardware.gatekeeper@1.0-impl-qti.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/android.hardware.gatekeeper@1.0-impl-qti.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ android.hardware.gatekeeper@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog libkeymasterdeviceutils
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ android.hardware.gatekeeper@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog libkeymasterdeviceutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_black_level47
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_black_level47.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_cpp_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_cpp_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libimscamera_jni
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libimscamera_jni.so
LOCAL_SRC_FILES_arm64 := system/lib64/libimscamera_jni.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils libnativehelper liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils libnativehelper liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_zsl_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_zsl_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.dpm.api@1.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.qualcomm.qti.dpm.api@1.0.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.qualcomm.qti.dpm.api@1.0.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := DeviceInfo
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/DeviceInfo/DeviceInfo.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_hfr_90_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_hfr_90_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdrc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libdrc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libpdnotifier
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libpdnotifier.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libpdnotifier.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libqmi_encdec libutils libdl libcutils liblog libqmi_cci libqmi_common_so
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libqmi_encdec libutils libdl libcutils liblog libqmi_cci libqmi_common_so
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_fovcrop_viewfinder46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_fovcrop_viewfinder46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdpmqmihal
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdpmqmihal.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdpmqmihal.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libc++ libm libhardware_legacy libc libhwbinder com.qualcomm.qti.dpm.api@1.0 libhidltransport libutils libdl libcutils liblog libhardware
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libc++ libm libhardware_legacy libc libhwbinder com.qualcomm.qti.dpm.api@1.0 libhidltransport libutils libdl libcutils liblog libhardware
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qmapbridge.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qualcomm.qmapbridge.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_us_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_us_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := WfdCommon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/WfdCommon.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libjpegdmahw
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libjpegdmahw.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qcrild
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/qcrild
LOCAL_INIT_RC       := vendor/etc/init/qcrild.rc
LOCAL_SHARED_LIBRARIES := libc++ libm libhardware_legacy libc libril-qc-hal-qmi libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_a3_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_a3_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov13850
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov13850.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libril-qc-qmi-1
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libril-qc-qmi-1.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libril-qc-qmi-1.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libdsi_netctrl libxml2 libdiag libqdp libperipheral_client libmdmdetect libutils libdl vendor.qti.hardware.radio.ims@1.0 librilqmiservices libqmi_client_helper android.hardware.secure_element@1.0 vendor.qti.hardware.radio.lpa@1.0 libc libril-qc-radioconfig libril-qcril-hook-oem vendor.qti.hardware.radio.uim_remote_client@1.0 libqmiservices vendor.qti.hardware.radio.uim@1.1 libsettings libhidlbase libhidltransport vendor.qti.hardware.radio.qtiradio@1.0 libm libhardware_legacy libidl libhwbinder libqmi android.hardware.radio@1.0 libqmi_client_qmux libril liblog vendor.qti.hardware.radio.am@1.0 libdsutils libqmi_cci vendor.qti.hardware.radio.uim@1.0 librilutils libsqlite libril-qc-ltedirectdisc libtime_genoff liblqe vendor.qti.hardware.radio.uim_remote_server@1.0 android.hardware.radio.config@1.0 libcutils vendor.qti.hardware.radio.qcrilhook@1.0 libsystem_health_mon
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libdsi_netctrl libxml2 libdiag libqdp libperipheral_client libmdmdetect libutils libdl vendor.qti.hardware.radio.ims@1.0 librilqmiservices libqmi_client_helper android.hardware.secure_element@1.0 vendor.qti.hardware.radio.lpa@1.0 libc libril-qc-radioconfig libril-qcril-hook-oem vendor.qti.hardware.radio.uim_remote_client@1.0 libqmiservices vendor.qti.hardware.radio.uim@1.1 libsettings libhidlbase libhidltransport vendor.qti.hardware.radio.qtiradio@1.0 libm libhardware_legacy libidl libhwbinder libqmi android.hardware.radio@1.0 libqmi_client_qmux libril liblog vendor.qti.hardware.radio.am@1.0 libdsutils libqmi_cci vendor.qti.hardware.radio.uim@1.0 librilutils libsqlite libril-qc-ltedirectdisc libtime_genoff liblqe vendor.qti.hardware.radio.uim_remote_server@1.0 android.hardware.radio.config@1.0 libcutils vendor.qti.hardware.radio.qcrilhook@1.0 libsystem_health_mon
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_hfr_60_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_hfr_60_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := librs_adreno_sha1
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/librs_adreno_sha1.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/librs_adreno_sha1.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libSubSystemShutdown
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libSubSystemShutdown.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libSubSystemShutdown.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libsubsystem_control libm libc libmdmdetect libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libsubsystem_control libm libc libmdmdetect libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_default_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_default_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libaudio_log_utils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libaudio_log_utils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libaudio_log_utils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libexpat libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libexpat libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_zsl_video_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_zsl_video_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxAmrwbplusDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxAmrwbplusDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxAmrwbplusDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmi_common_so
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqmi_common_so.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqmi_common_so.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_fullsize_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_fullsize_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_a3_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_a3_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := pageup.png
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/pageup.png
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hfr_90_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hfr_90_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_clamp_viewfinder40
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_clamp_viewfinder40.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_default_video_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_default_video_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov2685
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov2685.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := s5k3p8sp_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/s5k3p8sp_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libqti_resampler_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/mm-audio/audio-include
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_default_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_default_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxApeDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxApeDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxApeDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libFidoCrypto_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libFidoCrypto_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libFidoCrypto_system.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libQTEEConnector_system libutils libc liblog libdl vendor.qti.hardware.qteeconnector@1.0
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libQTEEConnector_system libutils libc liblog libdl vendor.qti.hardware.qteeconnector@1.0
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Qmmi
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/Qmmi/Qmmi.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmi_jni
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libmmi_jni.so
LOCAL_SRC_FILES_arm64 := system/lib64/libmmi_jni.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog libdiag_system
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog libdiag_system
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdiag_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libdiag_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libdiag_system.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.data.factory@1.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/vendor.qti.data.factory@1.0.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/vendor.qti.data.factory@1.0.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libutils vendor.qti.latency@2.0 vendor.qti.ims.rcsconfig@1.0 libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.1 libcutils vendor.qti.hardware.data.dynamicdds@1.0 libdl vendor.qti.hardware.data.qmi@1.0 liblog com.quicinc.cne.server@2.2 com.quicinc.cne.server@2.0 com.quicinc.cne.server@2.1 com.quicinc.cne.api@1.1 com.quicinc.cne.api@1.0 libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libutils vendor.qti.latency@2.0 vendor.qti.ims.rcsconfig@1.0 libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.1 libcutils vendor.qti.hardware.data.dynamicdds@1.0 libdl vendor.qti.hardware.data.qmi@1.0 liblog com.quicinc.cne.server@2.2 com.quicinc.cne.server@2.0 com.quicinc.cne.server@2.1 com.quicinc.cne.api@1.1 com.quicinc.cne.api@1.0 libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_csidtg_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_csidtg_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_handset.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_handset.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.imsrtpservice@1.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/vendor.qti.imsrtpservice@1.0.so
LOCAL_SRC_FILES_arm64 := system/lib64/vendor.qti.imsrtpservice@1.0.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_zsl_preview_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_zsl_preview_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmosal_proprietary
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmmosal_proprietary.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmmosal_proprietary.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liboemcrypto
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/liboemcrypto.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/liboemcrypto.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc vendor.display.config@1.3 libutils libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc vendor.display.config@1.3 libutils libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_video_full
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_video_full.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_zsl_preview_dw9761b_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_zsl_preview_dw9761b_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := fmfactorytestserver
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/fmfactorytestserver
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := fmconfig
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/fmconfig
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := DynamicDDSService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/DynamicDDSService/DynamicDDSService.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qseecom_sample_client
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qseecom_sample_client
LOCAL_SHARED_LIBRARIES := libc++ libm libc libSecureUILib libutils libdl libcutils liblog libQSEEComAPI
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_ds_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_ds_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Liquid_Headset_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Liquid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Liquid/Liquid_Headset_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_default_video_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_default_video_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qrtr-ns
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qrtr-ns
LOCAL_SHARED_LIBRARIES := libc++ libqrtr libc libm libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_a3_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_a3_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libwfd_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/wfd
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := pass.png
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/pass.png
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := lowi-server
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/lowi-server
LOCAL_SHARED_LIBRARIES := libc++ libwpa_client libm libc libnl libdiag libgps.utils libdl libcutils liblog libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqct_resampler
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libqct_resampler.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.camera
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.qualcomm.qti.camera.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libFlacSwDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libFlacSwDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libFlacSwDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc liblog libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc liblog libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CallFeaturesSetting
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/CallFeaturesSetting/CallFeaturesSetting.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libconfigdb
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libconfigdb.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libconfigdb.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libdl libxml libdsutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libdl libxml libdsutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libbtnv
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libbtnv.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libbtnv.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_camera_back.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_camera_back.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hfr_240_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hfr_240_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov2281_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov2281_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.keymaster@3.0-service-qti
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/android.hardware.keymaster@3.0-service-qti
LOCAL_INIT_RC       := vendor/etc/init/android.hardware.keymaster@3.0-service-qti.rc
LOCAL_SHARED_LIBRARIES := libhidlbase libbase android.hardware.keymaster@3.0 libc++ libm libhardware_legacy libc libhwbinder libhidltransport libutils libdl libcutils liblog libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := KmInstallKeybox
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/KmInstallKeybox
LOCAL_SHARED_LIBRARIES := libc++ libkeymasterutils libcrypto libkeymasterprovision libm libc libqtikeymaster4 libdl libcutils liblog libkeymasterdeviceutils libqmiservices libqmi_cci
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_default_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_default_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.soter@1.0-impl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/vendor.qti.hardware.soter@1.0-impl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/vendor.qti.hardware.soter@1.0-impl.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libcrypto libm libc libhwbinder libhidltransport libutils libdl libcutils liblog libQSEEComAPI vendor.qti.hardware.soter@1.0 libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libcrypto libm libc libhwbinder libhidltransport libutils libdl libcutils liblog libQSEEComAPI vendor.qti.hardware.soter@1.0 libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_bpc47
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_bpc47.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.server@2.2
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.quicinc.cne.server@2.2.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.quicinc.cne.server@2.2.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.1 com.quicinc.cne.constants@2.0 libutils libdl libcutils liblog com.quicinc.cne.server@2.0 com.quicinc.cne.server@2.1
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport com.quicinc.cne.constants@2.1 com.quicinc.cne.constants@2.0 libutils libdl libcutils liblog com.quicinc.cne.server@2.0 com.quicinc.cne.server@2.1
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_liveshot_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_liveshot_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := librs_adreno
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/librs_adreno.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/librs_adreno.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libCB libm libc libgsl libz libcutils libdl
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libCB libm libc libgsl libz libcutils libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530_zap.mdt
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530_zap.mdt
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov4688_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov4688_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libAR_jni
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libAR_jni.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libAR_jni.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_4k_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_4k_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_a3_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_a3_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libgsl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libgsl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libgsl.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libz libcutils liblog libdl libsync
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libz libcutils liblog libdl libsync
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqti-utils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqti-utils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqti-utils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libq3dtools_esx
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/egl/libq3dtools_esx.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/egl/libq3dtools_esx.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libdl libc libz libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libdl libc libz libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := lowi.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/lowi.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Liquid_Hdmi_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Liquid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Liquid/Liquid_Hdmi_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_hfr_120_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_hfr_120_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.server-V2.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.quicinc.cne.server-V2.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libvideoutils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libvideoutils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libvideoutils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libcutils libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_hdr_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_hdr_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov13850_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov13850_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_hfr_90_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_hfr_90_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libAlacSwDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libAlacSwDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libAlacSwDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc liblog libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc liblog libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libStDrvInt
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libStDrvInt.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libStDrvInt.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc liblog libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc liblog libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := main_wear.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/main_wear.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_stats_modules
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_stats_modules.so
LOCAL_SHARED_LIBRARIES := libhidlbase libmmcamera_dbg android.frameworks.sensorservice@1.0 libmmcamera2_mct libmmcamera2_stats_algorithm libmmcamera2_is libsensor1 libhidltransport libm libutils libc libcutils libdl libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsensor_reg_system
LOCAL_MODULE_STEM   := libsensor_reg
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libsensor_reg.so
LOCAL_SRC_FILES_arm64 := system/lib64/libsensor_reg.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libsensor1 libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libsensor1 libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libadm.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libtinyalsa libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.qteeconnector@1.0-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.qti.hardware.qteeconnector@1.0-service
LOCAL_INIT_RC       := vendor/etc/init/vendor.qti.hardware.qteeconnector@1.0-service.rc
LOCAL_SHARED_LIBRARIES := libhidlbase vendor.qti.hardware.qteeconnector@1.0 libm libc libhwbinder libhidltransport libutils libdl liblog libc++
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libflash_pmic
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libflash_pmic.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ATFWD-daemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/ATFWD-daemon
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libm libc libhwbinder libqmi libhidltransport libqmi_client_helper libmdmdetect libutils libdl libcutils liblog vendor.qti.hardware.radio.atcmdfwd@1.0 libqmiservices libqmi_cci libqmi_client_qmux libc++
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qtiNetworkLib
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/qtiNetworkLib.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := test_diag
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/test_diag
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libDiagService
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib64/libDiagService.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils libnativehelper libandroid_runtime liblog libdiag_system
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_hdr_snapshot_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_hdr_snapshot_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_hfr_90_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_hfr_90_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sw2d_lib
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sw2d_lib.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libm libfastcvopt libc libdl libcutils liblog libmmcamera_imglib
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a330_pm4.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a330_pm4.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libpdmapper
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libpdmapper.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libpdmapper.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libqmi_encdec libutils libdl libcutils liblog libjson libqmi_cci libqmi_common_so
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libqmi_encdec libutils libdl libcutils liblog libjson libqmi_cci libqmi_common_so
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_hvx_zzHDR
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_hvx_zzHDR.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qcrild_librilutils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/qcrild_librilutils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/qcrild_librilutils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_ltm47
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_ltm47.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_reboot.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_reboot.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx258_mono_gt24c32_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx258_mono_gt24c32_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qcrilmsgtunnel
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/qcrilmsgtunnel/qcrilmsgtunnel.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_a3_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_a3_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Liquid_Global_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Liquid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Liquid/Liquid_Global_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := RemoteSimlockManager.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/RemoteSimlockManager.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libqmi_csi_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/commonsys-intf/qmi-framework/qcsi/src
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_common.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_common.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hfr_240
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hfr_240.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libxtwifi_ulp_adaptor
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libxtwifi_ulp_adaptor.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libxtwifi_ulp_adaptor.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov2685_scv3b4035
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov2685_scv3b4035.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ssr_setup
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/ssr_setup
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmdmdetect libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.tui_comm@1.0-service-qti.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/init/vendor.qti.hardware.tui_comm@1.0-service-qti.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-adec-omxwma-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-adec-omxwma-test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmm-omxcore libOmxWmaDec libdl libaudioalsa
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libloc_base_util_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/framework/utils/native/base_util/src
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libFidoCrypto_vendor
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libFidoCrypto_vendor.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libFidoCrypto_vendor.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libQTEEConnector_vendor libm libc libc++ libutils libdl liblog libQSEEComAPI vendor.qti.hardware.qteeconnector@1.0
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libQTEEConnector_vendor libm libc libc++ libutils libdl liblog libQSEEComAPI vendor.qti.hardware.qteeconnector@1.0
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libslimclient
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libslimclient.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libslimclient.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libgps.utils libutils libdl libqmi_encdec liblog libqmi_cci libqmi_common_so
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libgps.utils libutils libdl libqmi_encdec liblog libqmi_cci libqmi_common_so
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libfastcvadsp_skel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libfastcvadsp_skel.so
LOCAL_SHARED_LIBRARIES := libc libfastcvadsp
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOpenCL_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libOpenCL_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libOpenCL_system.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libvndksupport libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libvndksupport libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := dpmd
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/dpmd
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libm libhardware_legacy libc libhwbinder com.qualcomm.qti.dpm.api@1.0 libhidltransport libdpmframework libutils libdl libdiag_system
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_zsl_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_zsl_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxDsdDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libOmxDsdDec.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-qomx-ienc-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-qomx-ienc-test
LOCAL_SHARED_LIBRARIES := libqomx_core libc++ libm libc libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxAlacDecSw
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxAlacDecSw.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxAlacDecSw.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_1080p_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_1080p_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_a3_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_a3_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_default_preview_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_default_preview_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmdmdetect
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmdmdetect.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmdmdetect.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqsappsver
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 32
LOCAL_SRC_FILES     := obj_arm/STATIC_LIBRARIES/libqsappsver_intermediates/libqsappsver.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libGPQTEEC_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libGPQTEEC_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libGPQTEEC_system.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase vendor.qti.hardware.qteeconnector@1.0 libm libQTEEConnector_system libGPTEE_system libutils libc libcutils liblog libdl libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase vendor.qti.hardware.qteeconnector@1.0 libm libQTEEConnector_system libGPTEE_system libutils libc libcutils liblog libdl libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_scaler_viewfinder46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_scaler_viewfinder46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsdedrm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsdedrm.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsdedrm.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libdrm libdrmutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libdrm libdrmutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_quadracfa
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_quadracfa.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libremosaic_daemon libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov4688_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov4688_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_lc898212xd_qc2002
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_lc898212xd_qc2002.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := radioconfig.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/radioconfig.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_ad5823
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_ad5823.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_hfr_120_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_hfr_120_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfduibcinterface
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfduibcinterface.so
LOCAL_SHARED_LIBRARIES := libc++ libwfduibcsrcinterface libm libc libutils libdl libcutils liblog libwfduibcsinkinterface libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_1080p_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_1080p_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libril-qc-hal-sms-headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/qcril-hal/modules/sms
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_s5k3p8sp
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_s5k3p8sp.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_hfr_240_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_hfr_240_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_video_16M_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_video_16M_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libGLESv1_CM_adreno
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/egl/libGLESv1_CM_adreno.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/egl/libGLESv1_CM_adreno.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libdl libgsl libc libz libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libdl libgsl libc libz libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsp_hvx_stub
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libadsp_hvx_stub.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libadsprpc libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblocationservice
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/liblocationservice.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/liblocationservice.so
LOCAL_SHARED_LIBRARIES_arm := libizat_core libc++ libm liblowi_client libc libgps.utils libdl libcutils liblog libulp2 libloc_core liblbs_core libdataitems
LOCAL_SHARED_LIBRARIES_arm64 := libizat_core libc++ libm liblowi_client libc libgps.utils libdl libcutils liblog libulp2 libloc_core liblbs_core libdataitems
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sensor_calibrate
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/sensor_calibrate.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/sensor_calibrate.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libxml2 libsensor1 libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libxml2 libsensor1 libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hdr_snapshot_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hdr_snapshot_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsensors_lib
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 64
LOCAL_SRC_FILES     := obj/STATIC_LIBRARIES/libsensors_lib_intermediates/libsensors_lib.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_indicator.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_indicator.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_hfr_60_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_hfr_60_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := dpmserviceapp
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/dpmserviceapp/dpmserviceapp.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libhdr_tm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libhdr_tm.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libhdr_tm.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libsdm-color libc libtinyxml2_1 libbinder libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libsdm-color libc libtinyxml2_1 libbinder libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqomx_jpegenc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libqomx_jpegenc.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmmjpeg libdl libcutils liblog libmmqjpeg_codec
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libprmediadrmplugin_customer
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := qcdrm/playready/lib/mediadrm
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/qcdrm/playready/lib/mediadrm/libprmediadrmplugin_customer.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libstagefright_foundation libutils libbase64 liblog libdl libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_mct
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_mct.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := xtwifi-client
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/xtwifi-client
LOCAL_SHARED_LIBRARIES := libc++ libcrypto liblowi_client libloc_core libc libm libgps.utils libsqlite libz libcutils liblog libdl libgdtap libqmiservices liblbs_core libqmi_cci libqmi_common_so libizat_core
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveBlobDescriptor_skel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libscveBlobDescriptor_skel.so
LOCAL_SHARED_LIBRARIES := 
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_scaler_video46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_scaler_video46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hfr_90_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hfr_90_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QRD_Headset_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/QRD
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/QRD/QRD_Headset_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_fovcrop_encoder46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_fovcrop_encoder46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := SoterProvisioningTool
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/SoterProvisioningTool
LOCAL_SHARED_LIBRARIES := libc++ libkeymasterutils libkeymasterprovision libm libc libutils libdl libcutils liblog libkeymasterdeviceutils
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_a3_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_a3_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_zsl_preview_dw9763
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_zsl_preview_dw9763.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := rmt_storage
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/rmt_storage
LOCAL_SHARED_LIBRARIES := libc++ libm libc libqmi_csi libmdmdetect libdl libcutils liblog libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libgpustats
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libgpustats.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libgpustats.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libCB libm libc libgsl libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libCB libm libc libgsl libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.fingerprint-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.fingerprint-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.data.dynamicdds-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.data.dynamicdds-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.factory.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/permissions/vendor.qti.hardware.factory.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_s5k3l8_mono
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_s5k3l8_mono.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsp_hvx_add_constant
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libadsp_hvx_add_constant.so
LOCAL_SHARED_LIBRARIES := libadsp_hvx_skel
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_preview_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_preview_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := liblbs_core_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/framework/native/core
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_1080p_preview_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_1080p_preview_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_a3_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_a3_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_frame_algorithm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmmcamera2_frame_algorithm.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmmcamera2_frame_algorithm.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libmmcamera_dbg libm libc libdl libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libmmcamera_dbg libm libc libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := eglSubDriverAndroid
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/egl/eglSubDriverAndroid.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/egl/eglSubDriverAndroid.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libdl libgsl libc libEGL_adreno libz libcutils liblog libhardware
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libdl libgsl libc libEGL_adreno libz libcutils liblog libhardware
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wigig-service
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/wigig-service.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wfdservice
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/wfdservice
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libm libwfdservice libc libwfdconfigutils libbinder libutils libdl libcutils liblog libwfdcommonutils libmmosal
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libaudioalsa
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libaudioalsa.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libaudioalsa.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := WigigSettings
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/WigigSettings/WigigSettings.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := MTP_Speaker_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/MTP
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/MTP/MTP_Speaker_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := subsystem_ramdump
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/subsystem_ramdump
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmdmdetect libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_12_0.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_12_0.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qcril_features_def.h
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/obj/include/qcril
LOCAL_SRC_FILES     := obj/include/qcril/qcril_features_def.h
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_bluetooth.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_bluetooth.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libFIDOKeyProvisioning
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libFIDOKeyProvisioning.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libFIDOKeyProvisioning.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libFidoCrypto_vendor libm libc libutils libdl liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libFidoCrypto_vendor libm libc libutils libdl liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_zsl_preview_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_zsl_preview_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_zsl_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_zsl_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_1080p_preview_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_1080p_preview_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libomx-dts
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libomx-dts.so
LOCAL_SHARED_LIBRARIES := libdl libc liblog libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libvpp_svc_skel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libvpp_svc_skel.so
LOCAL_SHARED_LIBRARIES := libc
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qti
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qti
LOCAL_SHARED_LIBRARIES := libc++ librmnetctl libm libqcmaputils libc libqmi libdiag libqmi_encdec libnetutils libdl libcutils liblog libqmiservices libdsutils libqmi_cci libqmi_client_qmux
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_fullsize_video_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_fullsize_video_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libhdcp2p2prov
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libhdcp2p2prov.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libhdcp2p2prov.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.api-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.quicinc.cne.api-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := uimlpalibrary
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/uimlpalibrary.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveBlobDescriptor
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscveBlobDescriptor.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscveBlobDescriptor.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libfastcvopt libc libdl liblog libscveCommon
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libfastcvopt libc libdl liblog libscveCommon
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := leia_pm4_470.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/leia_pm4_470.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_dbg
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmmcamera_dbg.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmmcamera_dbg.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libril-qc-hal-framework-headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/qcril-hal/framework
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.location.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/com.qualcomm.location.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QRD_Speaker_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/QRD
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/QRD/QRD_Speaker_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_a3_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_a3_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CmccSettingsRes
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_MODULE_PATH   := $(TARGET_OUT_VENDOR)/overlay
LOCAL_SRC_FILES     := vendor/overlay/CmccSettingsTheme/CmccSettingsRes.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_cpp_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_cpp_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libQSEEComAPIStatic
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 64
LOCAL_SRC_FILES     := obj/STATIC_LIBRARIES/libQSEEComAPIStatic_intermediates/libQSEEComAPIStatic.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := CNESettings
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/CNESettings/CNESettings.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_default_video_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_default_video_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := MTP_Bluetooth_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/MTP
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/MTP/MTP_Bluetooth_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libalarmservice_jni
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libalarmservice_jni.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libalarmservice_jni.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imx298
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imx298.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_truly_cma481_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_truly_cma481_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sensors.qti
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/sensors.qti
LOCAL_SHARED_LIBRARIES := libc++ libm libc libsensor1 libdiag libqmi_csi libcutils libdl libqmi_encdec liblog libqmi_cci libqmi_common_so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libloc_externalDrcore
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libloc_externalDrcore.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libloc_externalDrcore.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdmmsrc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdmmsrc.so
LOCAL_SHARED_LIBRARIES := libc++ libFileMux libmedia libdisplayconfig libaudioclient libhidltransport libutils libdl libm libgui libqdMetaData.system libbinder libtinyalsa libhardware libwfdavenhancements libpng libstagefright_enc_common libmmrtpencoder libmmosal libstagefright libhidlbase libaacwrapper libc libwfdconfigutils libmediaextractor libstagefright_foundation liblog com.qualcomm.qti.wifidisplayhal@1.0 libui libmedia_helper libwfdmminterface libcutils libwfdcommonutils libwfdcodecv4l2 libOmxMux
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.qteeconnector@1.0-impl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/vendor.qti.hardware.qteeconnector@1.0-impl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/vendor.qti.hardware.qteeconnector@1.0-impl.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase vendor.qti.hardware.qteeconnector@1.0 libm libxml2 libhwbinder libhidltransport libutils libc liblog libdl libGPreqcancel_svc libQSEEComAPI libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase vendor.qti.hardware.qteeconnector@1.0 libm libxml2 libhwbinder libhidltransport libutils libc liblog libdl libGPreqcancel_svc libQSEEComAPI libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdrmMinimalfs
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 64
LOCAL_SRC_FILES     := obj/STATIC_LIBRARIES/libdrmMinimalfs_intermediates/libdrmMinimalfs.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblocationservice_jni
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/liblocationservice_jni.so
LOCAL_SRC_FILES_arm64 := system/lib64/liblocationservice_jni.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ android.hidl.base@1.0 vendor.qti.gnss@1.2 vendor.qti.gnss@1.1 libhardware_legacy libc libhwbinder libhidltransport libm libnativehelper libutils libdl libcutils liblog vendor.qti.gnss@1.0 libandroid_runtime android.hardware.gnss@1.0
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ android.hidl.base@1.0 vendor.qti.gnss@1.2 vendor.qti.gnss@1.1 libhardware_legacy libc libhwbinder libhidltransport libm libnativehelper libutils libdl libcutils liblog vendor.qti.gnss@1.0 libandroid_runtime android.hardware.gnss@1.0
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530v3_gpmu.fw2
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530v3_gpmu.fw2
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ConnectionSecurityService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := vendor/app/ConnectionSecurityService/ConnectionSecurityService.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := SimSettings
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/SimSettings/SimSettings.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_hsensor.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_hsensor.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libssdStatic
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 32
LOCAL_SRC_FILES     := obj_arm/STATIC_LIBRARIES/libssdStatic_intermediates/libssdStatic.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libril-qcril-hook-oem
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libril-qcril-hook-oem.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libril-qcril-hook-oem.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libril libm libc libdiag libutils libdl libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libril libm libc libdiag libutils libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := LteDirectDiscovery.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/LteDirectDiscovery.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libril-qc-hal-qmi-headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/qcril-hal/qcril_qmi
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libimsmedia_jni
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libimsmedia_jni.so
LOCAL_SRC_FILES_arm64 := system/lib64/libimsmedia_jni.so
LOCAL_SHARED_LIBRARIES_arm := libandroid libc++ libm libgui libbinder libutils libc libcutils libnativehelper libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libandroid libc++ libm libgui libbinder libutils libc libcutils libnativehelper libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := reset.png
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/reset.png
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqcvirt
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := soundfx
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/soundfx/libqcvirt.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/soundfx/libqcvirt.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_ov8865
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_ov8865.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_cpp_us_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_cpp_us_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libthermalioctl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib64/libthermalioctl.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxVideoDSMode
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libOmxVideoDSMode.so
LOCAL_SHARED_LIBRARIES := libc++ libmmosal_proprietary libm libOmxCore libbinder libutils libc liblog libdl libwfdmmservice libwfdcommonutils_proprietary
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mmi.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/mmi.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_120_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_120_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_240_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_240_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_hfr_120_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_hfr_120_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.imscmservice-V2.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.qualcomm.qti.imscmservice-V2.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_4K_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_4K_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := pm-proxy
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/pm-proxy
LOCAL_SHARED_LIBRARIES := libc++ libm libc libperipheral_client libmdmdetect libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmi_csi
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqmi_csi.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqmi_csi.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libqmi_encdec libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libqmi_encdec libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsecureui
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsecureui.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsecureui.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libStDrvInt libm libc libhwbinder libhidltransport libbinder libutils libdl liblog libsecureui_svcsock vendor.display.config@1.0 libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libStDrvInt libm libc libhwbinder libhidltransport libbinder libutils libdl liblog libsecureui_svcsock vendor.display.config@1.0 libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_truly_cmb433_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_truly_cmb433_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.server@1.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/com.quicinc.cne.server@1.0.so
LOCAL_SRC_FILES_arm64 := system/lib64/com.quicinc.cne.server@1.0.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ com.quicinc.cne.constants@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ com.quicinc.cne.constants@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := TimeService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := vendor/app/TimeService/TimeService.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmdsprpc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmdsprpc.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmdsprpc.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libllvm-qcom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libllvm-qcom.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libllvm-qcom.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hfr_240_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hfr_240_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdsutils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdsutils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdsutils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libmdmdetect libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libmdmdetect libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_4k_video_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_4k_video_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqcbassboost
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := soundfx
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/soundfx/libqcbassboost.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/soundfx/libqcbassboost.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_4k_preview_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_4k_preview_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libimsqmiservices_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/ims-ship/qmi
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov8865_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov8865_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := izat_remote_api_prop_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/framework/native/lcp
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := Liquid_Bluetooth_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Liquid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Liquid/Liquid_Bluetooth_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libbase64
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libbase64.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libbase64.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hdr_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hdr_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_c2d_module
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera2_c2d_module.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera2_pp_buf_mgr
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_button_backlight.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_button_backlight.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.data.qmi-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.data.qmi-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := dpmQmiMgr
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/dpmQmiMgr
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libdpmqmihal libc++ libm libhardware_legacy libc libhwbinder com.qualcomm.qti.dpm.api@1.0 libhidltransport libqmi_client_helper libutils libdl libcutils liblog libqmiservices libqmi_cci libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.gatekeeper@1.0-service-qti.rc
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := init
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/init/android.hardware.gatekeeper@1.0-service-qti.rc
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmi
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libmmi.so
LOCAL_SRC_FILES_arm64 := system/lib64/libmmi.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libui libm libc_malloc_debug libgui libxml2 libbinder libutils libc libcutils liblog libdl libicuuc libhwui libft2 libdiag_system
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libui libm libc_malloc_debug libgui libxml2 libbinder libutils libc libcutils liblog libdl libicuuc libhwui libft2 libdiag_system
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hfr_240_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hfr_240_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_a3_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_a3_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_gtm46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_gtm46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := fonts.ttf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/fonts.ttf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_imglib_faceproc_adspstub
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_imglib_faceproc_adspstub.so
LOCAL_SHARED_LIBRARIES := libc++ libadsprpc libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := sns_oem_test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/sns_oem_test
LOCAL_SHARED_LIBRARIES := libc++ libc libsensor1 libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := librmp
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/librmp.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/librmp.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdiag
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdiag.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdiag.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_120_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_120_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mmi_diag
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/mmi_diag
LOCAL_SHARED_LIBRARIES := libc++ libbase libhidlbase libm libc_malloc_debug libc libhwbinder libhidltransport libmmi libutils libdl libcutils liblog vendor.qti.hardware.factory@1.0 libdiag_system
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qrtr-cfg
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qrtr-cfg
LOCAL_SHARED_LIBRARIES := libc++ libqrtr libc libm libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libnetmgr_rmnet_ext
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libnetmgr_rmnet_ext.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libnetmgr_rmnet_ext.so
LOCAL_SHARED_LIBRARIES_arm := libc++ librmnetctl libconfigdb libm libc libdiag libdl libcutils liblog libnetmgr libnetmgr_common libdsutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ librmnetctl libconfigdb libm libc libdiag libdl libcutils liblog libnetmgr libnetmgr_common libdsutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ssr_diag
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/ssr_diag
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdiag libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a540_gpmu.fw2
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a540_gpmu.fw2
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.gnss@1.0-impl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/vendor.qti.gnss@1.0-impl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/vendor.qti.gnss@1.0-impl.so
LOCAL_SHARED_LIBRARIES_arm := libc++ vendor.qti.gnss@1.0 libhidltransport libutils libloc_core liblbs_core libizat_core liblocation_api libizat_client_api libdl libdataitems libhidlbase libm libc libhwbinder libgps.utils libflp android.hardware.gnss@1.0 liblocationservice liblocationservice_glue libxtadapter liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ vendor.qti.gnss@1.0 libhidltransport libutils libloc_core liblbs_core libizat_core liblocation_api libizat_client_api libdl libdataitems libhidlbase libm libc libhwbinder libgps.utils libflp android.hardware.gnss@1.0 liblocationservice liblocationservice_glue libxtadapter liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := librpmb
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/librpmb.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/librpmb.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_snapshot_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_snapshot_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libq3dtools_adreno
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := egl
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/egl/libq3dtools_adreno.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/egl/libq3dtools_adreno.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgsl libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgsl libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_raw_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_raw_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libSecureUILib
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libSecureUILib.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libSecureUILib.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libStDrvInt libm libc libhwbinder libhidltransport libbinder libdl liblog libsecureui_svcsock libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libStDrvInt libm libc libhwbinder libhidltransport libbinder libdl liblog libsecureui_svcsock libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfduibcsrcinterface
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfduibcsrcinterface.so
LOCAL_SHARED_LIBRARIES := libwfduibcsrc libc++ libm libc libwfdconfigutils libutils libdl libcutils liblog libwfdcommonutils libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqsappsver
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 64
LOCAL_SRC_FILES     := obj/STATIC_LIBRARIES/libqsappsver_intermediates/libqsappsver.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_rohm_bu64243gwz
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_rohm_bu64243gwz.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_hdr_snapshot_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_hdr_snapshot_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libevent_observer
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libevent_observer.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libevent_observer.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.qualcomm.qti.wifidisplayhal@1.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/com.qualcomm.qti.wifidisplayhal@1.0.so
LOCAL_SHARED_LIBRARIES := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libremosaic_daemon
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libremosaic_daemon.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsp_default_listener
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libadsp_default_listener.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libadsp_default_listener.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libadsprpc libm libc libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libadsprpc libm libc libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor_cmd_tool
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/vendor_cmd_tool
LOCAL_SHARED_LIBRARIES := libc++ libm libc libxml2 libnl libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_qtech_f5670bq_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_qtech_f5670bq_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := leia_pfp_470.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/leia_pfp_470.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_1080p_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_1080p_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8858_a3_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8858_a3_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := VZW_profiles.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := cne/Nexus/VZW
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/cne/Nexus/VZW/VZW_profiles.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmiservices
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqmiservices.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqmiservices.so
LOCAL_SHARED_LIBRARIES_arm := libidl libc++ libc libm libdl
LOCAL_SHARED_LIBRARIES_arm64 := libidl libc++ libc libm libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_video_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_video_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := com.quicinc.cne.server-V2.2-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/com.quicinc.cne.server-V2.2-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.fm@1.0-impl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/vendor.qti.hardware.fm@1.0-impl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/vendor.qti.hardware.fm@1.0-impl.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase vendor.qti.hardware.fm@1.0 libm libc libhwbinder libhidltransport android.hardware.bluetooth@1.0-impl-qti libutils libdl libcutils liblog libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase vendor.qti.hardware.fm@1.0 libm libc libhwbinder libhidltransport android.hardware.bluetooth@1.0-impl-qti libutils libdl libcutils liblog libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_default_preview_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_default_preview_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqti-at
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libqti-at.so
LOCAL_SRC_FILES_arm64 := system/lib64/libqti-at.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libxml2 libutils libdl libcutils liblog libqti-perfd-client_system libqti-util_system
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libxml2 libutils libdl libcutils liblog libqti-perfd-client_system libqti-util_system
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := csidtg_camera.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/csidtg_camera.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_lcd.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_lcd.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_chroma_enhan40
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_chroma_enhan40.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libxt_native
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libxt_native.so
LOCAL_SRC_FILES_arm64 := system/lib64/libxt_native.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ android.hidl.base@1.0 libm vendor.qti.gnss@1.0 libc libhwbinder libhidltransport libnativehelper libutils libdl liblog libandroid_runtime android.hardware.gnss@1.0
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ android.hidl.base@1.0 libm vendor.qti.gnss@1.0 libc libhwbinder libhidltransport libnativehelper libutils libdl liblog libandroid_runtime android.hardware.gnss@1.0
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_zsl_video_ad5823
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_zsl_video_ad5823.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_hfr_90_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_hfr_90_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_bear_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_bear_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ftm_test_config_msm8996-dtp-tasha-snd-card
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/ftm_test_config_msm8996-dtp-tasha-snd-card
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_onsemi_cat24c32_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_onsemi_cat24c32_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libcdsprpc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libcdsprpc.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libcdsprpc.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqsocket
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqsocket.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqsocket.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdcommonutils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := system/lib/libwfdcommonutils.so
LOCAL_SHARED_LIBRARIES := libc++ libui libm libdisplayconfig libwfdmminterface libnl libbinder libutils libgui libcutils liblog libdl libhwui libmmosal libc libwfdconfigutils libion
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_4K_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_4K_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_cb.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_cb.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.qdutils_disp@1.0-service-qti
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hw/vendor.qti.hardware.qdutils_disp@1.0-service-qti
LOCAL_INIT_RC       := vendor/etc/init/vendor.qti.hardware.qdutils_disp@1.0-service-qti.rc
LOCAL_SHARED_LIBRARIES := libhidlbase libbase libc++ libm libqdutils libc libhwbinder libhidltransport libbinder libutils libdl libcutils liblog vendor.qti.hardware.qdutils_disp@1.0 libhardware
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveObjectSegmentation_skel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libscveObjectSegmentation_skel.so
LOCAL_SHARED_LIBRARIES := 
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Liquid_General_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Liquid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Liquid/Liquid_General_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_chroma_suppress40
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_chroma_suppress40.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_color_xform_video46
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_color_xform_video46.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveObjectSegmentation
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscveObjectSegmentation.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscveObjectSegmentation.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libfastcvopt libc libdl liblog libOpenCL libscveCommon
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libfastcvopt libc libdl liblog libOpenCL libscveCommon
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx318_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx318_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadiertac
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libadiertac.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libadiertac.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libaudcal libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libaudcal libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530_zap.b02
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530_zap.b02
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := a530_zap.b00
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/a530_zap.b00
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblowi_client
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/liblowi_client.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/liblowi_client.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxAmrDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxAmrDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxAmrDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmi_cci
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqmi_cci.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqmi_cci.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libqmi_client_qmux libqmi_encdec libmdmdetect libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libqmi_client_qmux libqmi_encdec libmdmdetect libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QRD_Hdmi_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/QRD
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/QRD/QRD_Hdmi_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_le2464c_master_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_le2464c_master_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmmcamera2_sensor_modules libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libllvm-glnext
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libllvm-glnext.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libllvm-glnext.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libgsl libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libgsl libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_feedback.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_feedback.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := lib_drplugin_server
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/lib_drplugin_server.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/lib_drplugin_server.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libloc_externalDr libc libdiag libgps.utils libutils libz libcutils liblog libdl libloc_core liblbs_core libslimclient
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libloc_externalDr libc libdiag libgps.utils libutils libz libcutils liblog libdl libloc_core liblbs_core libslimclient
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadpcmdec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libadpcmdec.so
LOCAL_SHARED_LIBRARIES := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := xtwifi-inet-agent
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/xtwifi-inet-agent
LOCAL_SHARED_LIBRARIES := libc++ libm libcurl libgps.utils libc libcutils liblog libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_hfr_120_dw9763
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_hfr_120_dw9763.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := remotesimlockmanagerlibrary
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/remotesimlockmanagerlibrary.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libdataitems_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/framework/native/lcp/data-items
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libflp
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libflp.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libflp.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libutils libdl libcutils liblog libloc_core liblbs_core libizat_core
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libutils libdl libcutils liblog libloc_core liblbs_core libizat_core
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hfr_60_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hfr_60_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_hfr_90_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_hfr_90_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sony_imx214_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sony_imx214_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_zsl_preview_ad5823
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_zsl_preview_ad5823.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := lpa.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/lpa.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_le2464c_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_le2464c_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libRSDriver_adreno
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libRSDriver_adreno.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libRSDriver_adreno.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libEGL libm libRSCpuRef libGLESv2 libdl libbcinfo libRS_internal libc librs_adreno liblog libGLESv1_CM libnativewindow
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libEGL libm libRSCpuRef libGLESv2 libdl libbcinfo libRS_internal libc librs_adreno liblog libGLESv1_CM libnativewindow
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qdss.config.sh
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/persist/coresight
LOCAL_SRC_FILES     := persist/coresight/qdss.config.sh
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-adec-omxamr-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-adec-omxamr-test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmm-omxcore libdl libOmxAmrDec
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cacert_location.pem
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/cacert_location.pem
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_default_video_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_default_video_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_a3_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_a3_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_hfr_60_ad5823
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_hfr_60_ad5823.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := perflock-test.sh
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/data/nativetest/perflock_native_test
LOCAL_MULTILIB      := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := data/nativetest/perflock_native_test/perflock-test.sh
LOCAL_SHARED_LIBRARIES := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqti-perfd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqti-perfd.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqti-perfd.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libperfgluelayer libthermalclient libm libc libxml2 vendor.qti.hardware.perf@1.0 libhidltransport libqti-perfd-client libutils libdl libcutils liblog libqti-util libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libperfgluelayer libthermalclient libm libc libxml2 vendor.qti.hardware.perf@1.0 libhidltransport libqti-perfd-client libutils libdl libcutils liblog libqti-util libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_default_video_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_default_video_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := atfwd
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/atfwd/atfwd.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p8sp_a3_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p8sp_a3_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveCommon_stub
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscveCommon_stub.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscveCommon_stub.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libadsprpc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libadsprpc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_f5670bq_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_f5670bq_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := hdcp2p2prov
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hdcp2p2prov
LOCAL_SHARED_LIBRARIES := libc++ libhdcp2p2prov libm libc libdiag libdl liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_hfr_90_3a
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_hfr_90_3a.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := qcat_unbuffered
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/qcat_unbuffered
LOCAL_SHARED_LIBRARIES := libc++ libc liblog libdl libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libaudcal
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libaudcal.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libaudcal.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_us_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_us_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := liblocationservice_jni_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/commonsys/gps/framework/native/framework-jni
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wigig_logcollector.ini
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/wigig_logcollector.ini
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libstagefright_soft_qtiflacdec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libstagefright_soft_qtiflacdec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libstagefright_soft_qtiflacdec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libstagefright_foundation libutils libdl liblog libstagefright_omx
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libstagefright_foundation libutils libdl liblog libstagefright_omx
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libstreamparser
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libstreamparser.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libstreamparser.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libvideoutils libc libcutils liblog libdl
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libvideoutils libc libcutils liblog libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsi
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsi.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsi.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils liblog libpng libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils liblog libpng libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_led_blue.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_led_blue.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := thermal-engine.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/thermal-engine.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_a3_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_a3_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ConferenceDialer
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/app/ConferenceDialer/ConferenceDialer.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_zsl_preview_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_zsl_preview_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_cpu.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_cpu.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libbccQTI
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libbccQTI.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libbccQTI.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_hfr_120_dw9761b_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_hfr_120_dw9761b_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := layout_gsensor.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := mmi/layout
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/mmi/layout/layout_gsensor.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wdsdaemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/wdsdaemon
LOCAL_SHARED_LIBRARIES := libc++ libm libbt-hidlclient libdiag libc libcutils libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_default_preview_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_default_preview_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := librpmbStatic
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX := .a
LOCAL_MULTILIB      := 64
LOCAL_SRC_FILES     := obj/STATIC_LIBRARIES/librpmbStatic_intermediates/librpmbStatic.a
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2281_a3_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2281_a3_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_a3_default_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_a3_default_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_2_0.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_2_0.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx378_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx378_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := Fluid_Headset_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/Fluid
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/Fluid/Fluid_Headset_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := activity_recognition.msm8996
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/activity_recognition.msm8996.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/activity_recognition.msm8996.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libsensor1 libdiag libsensor_reg libutils libdl libcutils liblog libhardware
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libsensor1 libdiag libsensor_reg libutils libdl libcutils liblog libhardware
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.data.qmi@1.0
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/vendor.qti.hardware.data.qmi@1.0.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/vendor.qti.hardware.data.qmi@1.0.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := perflock_native_test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/perflock_native_test
LOCAL_SHARED_LIBRARIES := libc++ libperfgluelayer libm libqti-perfd libc libutils libdl libcutils liblog libqti-util
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libadsp_fd_skel
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := rfsa/adsp
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/rfsa/adsp/libadsp_fd_skel.so
LOCAL_SHARED_LIBRARIES := libc
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := keystore.msm8996
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/keystore.msm8996.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/keystore.msm8996.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libcrypto libm libc libdl libcutils liblog libhardware
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libcrypto libm libc libdl libcutils liblog libhardware
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_default_preview_dw9714
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_default_preview_dw9714.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_hfr_120_dw9714
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_hfr_120_dw9714.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_snapshot_hdr_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_snapshot_hdr_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_mono_default_video_ak7345
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_mono_default_video_ak7345.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_cpp_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_cpp_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libtime_genoff
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libtime_genoff.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libtime_genoff.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.data.factory-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.data.factory-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_cpp_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_cpp_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_hfr_60
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_hfr_60.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_4k_video_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_4k_video_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5670_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5670_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_1080p_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_1080p_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwigig_pciaccess
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libwigig_pciaccess.so
LOCAL_SRC_FILES_arm64 := system/lib64/libwigig_pciaccess.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libwigig_utils libm libdl
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libwigig_utils libm libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_default_preview_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_default_preview_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblowi_wifihal
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/liblowi_wifihal.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/liblowi_wifihal.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm liblowi_client libc libgps.utils libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm liblowi_client libc libgps.utils libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := QRD_Global_cal.acdb
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := acdbdata/QRD
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/acdbdata/QRD/QRD_Global_cal.acdb
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmi
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqmi.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqmi.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libidl libqmi_client_qmux libdiag libc libcutils liblog libdl libqmiservices libdsutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libidl libqmi_client_qmux libdiag libc libcutils liblog libdl libqmiservices libdsutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := radioconfiginterfacelibrary
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/radioconfiginterfacelibrary.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.alarm@1.0-impl
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/vendor.qti.hardware.alarm@1.0-impl.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/vendor.qti.hardware.alarm@1.0-impl.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libc++ libm libc libhidltransport vendor.qti.hardware.alarm@1.0 libutils libdl libcutils liblog libhardware
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libc++ libm libc libhidltransport vendor.qti.hardware.alarm@1.0 libutils libdl libcutils liblog libhardware
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := mm-qjpeg-dec-test
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/mm-qjpeg-dec-test
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmqjpeg_codec
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov2680_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov2680_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libulp2_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/gps/ulp2/src
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqomx_jpegenc_pipe
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libqomx_jpegenc_pipe.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libmmjpeg libdl libcutils liblog libmmqjpegdma libmmqjpeg_codec
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.wigig.supptunnel-V1.0-java
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/framework/vendor.qti.hardware.wigig.supptunnel-V1.0-java.jar
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := xtra-daemon
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/xtra-daemon
LOCAL_SHARED_LIBRARIES := libc++ libsqlite libcrypto libm libc libgps.utils libutils libdl libcutils liblog libloc_core libizat_core libandroid_net libssl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsecureui_svcsock_system
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libsecureui_svcsock_system.so
LOCAL_SRC_FILES_arm64 := system/lib64/libsecureui_svcsock_system.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libc++ vendor.qti.hardware.tui_comm@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libc++ vendor.qti.hardware.tui_comm@1.0 libm libc libhwbinder libhidltransport libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov4688_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov4688_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_snapshot_hdr_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_snapshot_hdr_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := loc_launcher
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/loc_launcher
LOCAL_SHARED_LIBRARIES := libc++ libm libc libgps.utils libutils libdl libcutils liblog
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOpenCL
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOpenCL.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOpenCL.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libcutils libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := perfunittests
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/perfunittests
LOCAL_SHARED_LIBRARIES := libc++ libperfgluelayer libm libqti-perfd libc libutils libdl libcutils liblog libqti-util
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_q3a_core
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmmcamera2_q3a_core.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmmcamera2_q3a_core.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libmmcamera_dbg libm libmmcamera2_stats_algorithm libmmcamera2_is libc libcutils libdl
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libmmcamera_dbg libm libmmcamera2_stats_algorithm libmmcamera2_is libc libcutils libdl
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := wigig_logcollector
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/wigig_logcollector
LOCAL_SHARED_LIBRARIES := libc++ libwigig_utils libwigig_pciaccess libm libc libdl
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := hvdcp_opti
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 64
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/hvdcp_opti
LOCAL_SHARED_LIBRARIES := libc++ libm libc libutils libdl libcutils
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := vendor.qti.hardware.scve.panorama@1.0-adapter-helper
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/vendor.qti.hardware.scve.panorama@1.0-adapter-helper.so
LOCAL_SRC_FILES_arm64 := system/lib64/vendor.qti.hardware.scve.panorama@1.0-adapter-helper.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libbase libm libc libhwbinder libhidltransport libutils libdl libcutils liblog libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libbase libm libc libhwbinder libhidltransport libutils libdl libcutils liblog libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := WfdService
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_SRC_FILES     := system/priv-app/WfdService/WfdService.apk
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PROPRIETARY_MODULE := false
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_gt24c64_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_gt24c64_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov7251_cpp_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov7251_cpp_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ssgqmigd
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/bin/ssgqmigd
LOCAL_SHARED_LIBRARIES := libc libdl liblog libm
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblbs_core
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/liblbs_core.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/liblbs_core.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libsqlite libcrypto libm libc libperipheral_client libgps.utils libutils libdl libcutils liblog libmdmdetect libloc_core libloc_api_v02 libizat_core
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libsqlite libcrypto libm libc libperipheral_client libgps.utils libutils libdl libcutils liblog libmdmdetect libloc_core libloc_api_v02 libizat_core
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := evt-sniff.cfg
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/data/evt-test
LOCAL_SRC_FILES     := data/evt-test/evt-sniff.cfg
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_default_preview_bu64297
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_default_preview_bu64297.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_hfr_90_lc898217xc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_hfr_90_lc898217xc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_cpp_ds_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_cpp_ds_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libdrmtime
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libdrmtime.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libdrmtime.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libtime_genoff libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libtime_genoff libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libmmosal_proprietary_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/mm-osal
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_pdafcamif
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_pdafcamif.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libm libc libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_hfr_90_dw9763
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_hfr_90_dw9763.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ChinaMobileFrameworksRes
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := APPS
LOCAL_MODULE_PATH   := $(TARGET_OUT_VENDOR)/overlay
LOCAL_SRC_FILES     := vendor/overlay/ChinaMobileFrameworksRes/ChinaMobileFrameworksRes.apk
LOCAL_PRIVILEGED_MODULE := 
LOCAL_PROPRIETARY_MODULE := true
LOCAL_CERTIFICATE   := platform
LOCAL_DEX_PREOPT    := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libQTEEConnector_vendor
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libQTEEConnector_vendor.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libQTEEConnector_vendor.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase vendor.qti.hardware.qteeconnector@1.0 libm libc libhidltransport libutils libdl libcutils liblog libc++
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase vendor.qti.hardware.qteeconnector@1.0 libm libc libhidltransport libutils libdl libcutils liblog libc++
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_is
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmmcamera2_is.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmmcamera2_is.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libmmcamera_dbg libm libc libdl libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libmmcamera_dbg libm libc libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libactuator_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libactuator_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_sony_imx378_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_sony_imx378_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := android.hardware.light.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/permissions/android.hardware.light.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libwfdclient
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libwfdclient.so
LOCAL_SRC_FILES_arm64 := system/lib64/libwfdclient.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libgui libbinder libutils libc libcutils liblog libdl libinput libmmosal
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libgui libbinder libutils libc libcutils liblog libdl libinput libmmosal
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_cpp_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_cpp_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_atmel_at24c32e_eeprom
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_atmel_at24c32e_eeprom.so
LOCAL_SHARED_LIBRARIES := libc++ libm libc libdl libcutils liblog libmmcamera_eeprom_util
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_a3_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_a3_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_1080p_video_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_1080p_video_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := liblqe
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/liblqe.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/liblqe.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libqmi_client_helper libdl libcutils liblog libqmiservices libdsutils libqmi_cci libqmi_common_so
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libqmi_client_helper libdl libcutils liblog libqmiservices libdsutils libqmi_cci libqmi_common_so
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx258_mono_bear_cpp_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx258_mono_bear_cpp_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libperipheral_client
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libperipheral_client.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libperipheral_client.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libbinder libmdmdetect libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libbinder libmdmdetect libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsensor1_system
LOCAL_MODULE_STEM   := libsensor1
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := false
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := system/lib/libsensor1.so
LOCAL_SRC_FILES_arm64 := system/lib64/libsensor1.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libqmi_cci_system libm libc libutils libdl libcutils liblog libdiag_system
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libqmi_cci_system libm libc libutils libdl libcutils liblog libdiag_system
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := gatekeeper.msm8996
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/hw/gatekeeper.msm8996.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/hw/gatekeeper.msm8996.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_hdr_video_lc898212xd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_hdr_video_lc898212xd.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera2_stats_algorithm
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libmmcamera2_stats_algorithm.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libmmcamera2_stats_algorithm.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libmmcamera_dbg libm libc libdl libcutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libmmcamera_dbg libm libc libdl libcutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_cpp_us_chromatix
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_cpp_us_chromatix.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_csidtg_zsl_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_csidtg_zsl_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libI420colorconvert
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libI420colorconvert.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libI420colorconvert.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl libcutils libmm-color-convertor
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl libcutils libmm-color-convertor
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_hfr_120
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_hfr_120.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8865_default_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8865_default_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := telephonyservice.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := permissions
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/permissions/telephonyservice.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libquipc_os_api
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libquipc_os_api.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libquipc_os_api.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libgps.utils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libgps.utils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_postproc
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_postproc.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libGPQTEEC_vendor
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libGPQTEEC_vendor.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libGPQTEEC_vendor.so
LOCAL_SHARED_LIBRARIES_arm := libhidlbase libGPTEE_vendor libm libQTEEConnector_vendor libc libc++ libutils libdl libcutils liblog vendor.qti.hardware.qteeconnector@1.0
LOCAL_SHARED_LIBRARIES_arm64 := libhidlbase libGPTEE_vendor libm libQTEEConnector_vendor libc libc++ libutils libdl libcutils liblog vendor.qti.hardware.qteeconnector@1.0
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxAacDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxAacDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxAacDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libssd
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libssd.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libssd.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libdiag libutils libdl libcutils liblog libQSEEComAPI
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_cpp_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_cpp_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_cpp_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_cpp_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov5695_a3_hfr_90
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov5695_a3_hfr_90.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := dpm.conf
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := dpm
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/etc/dpm/dpm.conf
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libmmcamera_isp_bhist_stats44
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libmmcamera_isp_bhist_stats44.so
LOCAL_SHARED_LIBRARIES := libc++ libmmcamera_dbg libmmcamera2_mct libm libc libdl libcutils libmmcamera_isp_sub_module
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libOmxEvrcDec
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libOmxEvrcDec.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libOmxEvrcDec.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libutils libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libutils libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libscveBlobDescriptor_stub
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libscveBlobDescriptor_stub.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libscveBlobDescriptor_stub.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libadsprpc libm libc libdl liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libadsprpc libm libc libdl liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqrtr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqrtr.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqrtr.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libc libdl libm
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libc libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libgdtap
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libgdtap.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libgdtap.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libgps.utils libutils libdl libcutils liblog libloc_core liblbs_core libizat_core
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libgps.utils libutils libdl libcutils liblog libloc_core liblbs_core libizat_core
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_cpp_video_4k
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_cpp_video_4k.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3p3sm_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3p3sm_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xm_postproc_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xm_postproc_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsensor_test
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsensor_test.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsensor_test.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libsensor1 libsensor_reg libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libsensor1 libsensor_reg libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := ov13850_q13v06k_chromatix.xml
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_RELATIVE_PATH := camera
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES     := vendor/etc/camera/ov13850_q13v06k_chromatix.xml
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := cpp_firmware_v1_6_0.fw
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := DATA
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/vendor/firmware
LOCAL_SRC_FILES     := vendor/firmware/cpp_firmware_v1_6_0.fw
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libril-qc-qmi-services-headers
LOCAL_EXPORT_C_INCLUDE_DIRS := vendor/qcom/proprietary/qcril-qmi-services-headers
include $(BUILD_HEADER_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_with_gyro_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_with_gyro_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_preview
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_preview.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx230_qc2002_cpp_video_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx230_qc2002_cpp_video_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_hdr_snapshot_3a_bear
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_hdr_snapshot_3a_bear.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libqmi_client_qmux
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libqmi_client_qmux.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libqmi_client_qmux.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libidl libdiag libc libcutils liblog libdl libqmiservices libdsutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libidl libdiag libc libcutils liblog libdl libqmiservices libdsutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libvpptestutils
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libvpptestutils.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libvpptestutils.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libvpplibrary libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libvpplibrary libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_zsl_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_zsl_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3m2xx_4k_video_ad5816g
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3m2xx_4k_video_ad5816g.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov8856_cpp_video
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov8856_cpp_video.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k2l7_8953_4k_preview_ak7371
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k2l7_8953_4k_preview_ak7371.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libsensor_thresh
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libsensor_thresh.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libsensor_thresh.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libm libc libsensor1 libutils libdl libcutils liblog
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libm libc libsensor1 libutils libdl libcutils liblog
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_ov13850_q13v06k_common
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_ov13850_q13v06k_common.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx298_snapshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx298_snapshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_s5k3l8_f3l8yam_liveshot
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_s5k3l8_f3l8yam_liveshot.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := rtspserver
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := EXECUTABLES
LOCAL_MODULE_RELATIVE_PATH := 
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := false
LOCAL_SRC_FILES     := system/bin/rtspserver
LOCAL_SHARED_LIBRARIES := libc++ libm libc libwfdrtsp libdl libwfdcommonutils libmmosal
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libnetmgr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := both
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES_arm := vendor/lib/libnetmgr.so
LOCAL_SRC_FILES_arm64 := vendor/lib64/libnetmgr.so
LOCAL_SHARED_LIBRARIES_arm := libc++ libconfigdb libm libhardware_legacy libc libdiag libdl libcutils liblog libdsutils
LOCAL_SHARED_LIBRARIES_arm64 := libc++ libconfigdb libm libhardware_legacy libc libdiag libdl libcutils liblog libdsutils
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx214_zsl_video_lc898122
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx214_zsl_video_lc898122.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE        := libchromatix_imx362_snapshot_hdr
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_OWNER  := qcom
LOCAL_MODULE_TAGS   := optional debug
LOCAL_MODULE_CLASS  := SHARED_LIBRARIES
LOCAL_MULTILIB      := 32
LOCAL_PROPRIETARY_MODULE := true
LOCAL_STRIP_MODULE  := false
LOCAL_SRC_FILES     := vendor/lib/libchromatix_imx362_snapshot_hdr.so
LOCAL_SHARED_LIBRARIES := libc++ libc libcutils libdl libm
LOCAL_EXPORT_C_INCLUDE_DIRS := 
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi-framework/inc
LOCAL_COPY_HEADERS    := obj/include/qmi-framework/inc/qmi_client.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/data_common_v01.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-camera/imglib
LOCAL_COPY_HEADERS    := obj/include/mm-camera/imglib/img_meta.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/qmi_client.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := adreno/EGL
LOCAL_COPY_HEADERS    := obj/include/adreno/EGL/eglext.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := bt/hci_qcomm_init
LOCAL_COPY_HEADERS    := obj/include/bt/hci_qcomm_init/bt_nv.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-camera/imglib
LOCAL_COPY_HEADERS    := obj/include/mm-camera/imglib/img_mem_ops.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := adreno
LOCAL_COPY_HEADERS    := obj/include/adreno/c2d2.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/wireless_data_service_v01.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-camera/imglib
LOCAL_COPY_HEADERS    := obj/include/mm-camera/imglib/lib2d.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-audio/audio-log-utils
LOCAL_COPY_HEADERS    := obj/include/mm-audio/audio-log-utils/log_utils.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi-framework/inc
LOCAL_COPY_HEADERS    := obj/include/qmi-framework/inc/common_v01.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/qmi_idl_lib.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/qmi_port_defs.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-camera/imglib
LOCAL_COPY_HEADERS    := obj/include/mm-camera/imglib/img_common.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := fastrpc/inc
LOCAL_COPY_HEADERS    := obj/include/fastrpc/inc/remote.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/qmi_qos_srvc.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := libperipheralclient/inc
LOCAL_COPY_HEADERS    := obj/include/libperipheralclient/inc/pm-service.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := data/inc
LOCAL_COPY_HEADERS    := obj/include/data/inc/queue.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := libmdmdetect/inc
LOCAL_COPY_HEADERS    := obj/include/libmdmdetect/inc/mdm_detect.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := adreno/EGL
LOCAL_COPY_HEADERS    := obj/include/adreno/EGL/eglplatform.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := libvqzip
LOCAL_COPY_HEADERS    := obj/include/libvqzip/VQZip.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-camera/imglib
LOCAL_COPY_HEADERS    := obj/include/mm-camera/imglib/img_comp_factory.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := data/inc
LOCAL_COPY_HEADERS    := obj/include/data/inc/dsi_netctrl.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/qmi_platform_config.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/common_v01.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-audio/surround_sound_3mic
LOCAL_COPY_HEADERS    := obj/include/mm-audio/surround_sound_3mic/surround_rec_interface.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/wireless_data_service_common_v01.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/qmi.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/authentication_service_v01.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := libgpustats
LOCAL_COPY_HEADERS    := obj/include/libgpustats/gpustats.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := adreno/EGL
LOCAL_COPY_HEADERS    := obj/include/adreno/EGL/egl.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-audio/audio-log-utils
LOCAL_COPY_HEADERS    := obj/include/mm-audio/audio-log-utils/log_xml_parser.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-camera/imglib
LOCAL_COPY_HEADERS    := obj/include/mm-camera/imglib/img_thread_ops.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi-framework/inc
LOCAL_COPY_HEADERS    := obj/include/qmi-framework/inc/qmi_cci_common.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := data/inc
LOCAL_COPY_HEADERS    := obj/include/data/inc/dsi_netctrl_qos.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi-framework/inc
LOCAL_COPY_HEADERS    := obj/include/qmi-framework/inc/qmi_common.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-camera/imglib
LOCAL_COPY_HEADERS    := obj/include/mm-camera/imglib/img_comp.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := fastrpc/inc
LOCAL_COPY_HEADERS    := obj/include/fastrpc/inc/AEEStdErr.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := mm-camera/imglib
LOCAL_COPY_HEADERS    := obj/include/mm-camera/imglib/img_buffer.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi-framework/inc
LOCAL_COPY_HEADERS    := obj/include/qmi-framework/inc/qmi_cci_target.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi-framework/inc
LOCAL_COPY_HEADERS    := obj/include/qmi-framework/inc/qmi_idl_lib.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi-framework/inc
LOCAL_COPY_HEADERS    := obj/include/qmi-framework/inc/qmi_idl_lib_internal.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi/inc
LOCAL_COPY_HEADERS    := obj/include/qmi/inc/qmi_idl_lib_internal.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

include $(CLEAR_VARS)
LOCAL_COPY_HEADERS_TO := qmi-framework/inc
LOCAL_COPY_HEADERS    := obj/include/qmi-framework/inc/qmi_cci_target_ext.h
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_COPY_HEADERS)

$(shell mkdir -p out/target/common/obj/JAVA_LIBRARIES/qtiNetworkLib_intermediates/ && cp -f vendor/qcom/proprietary/prebuilt_HY11/target/common/obj/JAVA_LIBRARIES/qtiNetworkLib_intermediates/classes-header.jar out/target/common/obj/JAVA_LIBRARIES/qtiNetworkLib_intermediates/)