#ifndef HIDL_GENERATED_VENDOR_QTI_HARDWARE_FM_V1_0_IFMHCI_H
#define HIDL_GENERATED_VENDOR_QTI_HARDWARE_FM_V1_0_IFMHCI_H

#include <android/hidl/base/1.0/IBase.h>
#include <vendor/qti/hardware/fm/1.0/IFmHciCallbacks.h>
#include <vendor/qti/hardware/fm/1.0/types.h>

#include <android/hidl/manager/1.0/IServiceNotification.h>

#include <hidl/HidlSupport.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <utils/NativeHandle.h>
#include <utils/misc.h>

namespace vendor {
namespace qti {
namespace hardware {
namespace fm {
namespace V1_0 {

struct IFmHci : public ::android::hidl::base::V1_0::IBase {
    typedef android::hardware::details::i_tag _hidl_tag;

    // Forward declaration for forward reference support:

    /**
     * The Host Controller Interface (HCI) is the layer defined by the Fm
     * specification between the software that runs on the host and the Fm
     * controller chip. This boundary is the natural choice for a Hardware
     * Abstraction Layer (HAL). Dealing only in HCI packets and events simplifies
     * the stack and abstracts away power management, initialization, and other
     * implementation-specific details related to the hardware.
     */
    virtual bool isRemote() const override { return false; }


    // @entry @callflow(next={"sendHciCommand", "close"})
    /**
     * Initialize the underlying HCI interface.
     * 
     * This method should be used to initialize any hardware interfaces
     * required to communicate with the Fm hardware in the
     * device.
     * 
     * The |oninitializationComplete| callback must be invoked in response
     * to this function to indicate success before any other function
     * (sendHciCommand) is invoked on this interface.
     * 
     * @param callback implements IFmHciCallbacks which will
     * receive callbacks when incoming HCI packets are received
     * from the controller to be sent to the host.
     */
    virtual ::android::hardware::Return<void> initialize(const ::android::sp<::vendor::qti::hardware::fm::V1_0::IFmHciCallbacks>& callback) = 0;

    // @callflow(next={"sendHciCommand", "close"})
    /**
     * Send an HCI command the Fm controller.
     * Commands must be executed in order.
     * 
     * @param command is the HCI command to be sent
     */
    virtual ::android::hardware::Return<void> sendHciCommand(const ::android::hardware::hidl_vec<uint8_t>& command) = 0;

    // @exit
    /**
     * Close the HCI interface
     */
    virtual ::android::hardware::Return<void> close() = 0;

    using interfaceChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& descriptors)>;
    virtual ::android::hardware::Return<void> interfaceChain(interfaceChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options) override;

    using interfaceDescriptor_cb = std::function<void(const ::android::hardware::hidl_string& descriptor)>;
    virtual ::android::hardware::Return<void> interfaceDescriptor(interfaceDescriptor_cb _hidl_cb) override;

    using getHashChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_array<uint8_t, 32>>& hashchain)>;
    virtual ::android::hardware::Return<void> getHashChain(getHashChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> setHALInstrumentation() override;

    virtual ::android::hardware::Return<bool> linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie) override;

    virtual ::android::hardware::Return<void> ping() override;

    using getDebugInfo_cb = std::function<void(const ::android::hidl::base::V1_0::DebugInfo& info)>;
    virtual ::android::hardware::Return<void> getDebugInfo(getDebugInfo_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> notifySyspropsChanged() override;

    virtual ::android::hardware::Return<bool> unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient) override;
    // cast static functions
    static ::android::hardware::Return<::android::sp<::vendor::qti::hardware::fm::V1_0::IFmHci>> castFrom(const ::android::sp<::vendor::qti::hardware::fm::V1_0::IFmHci>& parent, bool emitError = false);
    static ::android::hardware::Return<::android::sp<::vendor::qti::hardware::fm::V1_0::IFmHci>> castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError = false);

    static const char* descriptor;

    static ::android::sp<IFmHci> tryGetService(const std::string &serviceName="default", bool getStub=false);
    static ::android::sp<IFmHci> tryGetService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return tryGetService(str, getStub); }
    static ::android::sp<IFmHci> tryGetService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return tryGetService(str, getStub); }
    static ::android::sp<IFmHci> tryGetService(bool getStub) { return tryGetService("default", getStub); }
    static ::android::sp<IFmHci> getService(const std::string &serviceName="default", bool getStub=false);
    static ::android::sp<IFmHci> getService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return getService(str, getStub); }
    static ::android::sp<IFmHci> getService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return getService(str, getStub); }
    static ::android::sp<IFmHci> getService(bool getStub) { return getService("default", getStub); }
    __attribute__ ((warn_unused_result))::android::status_t registerAsService(const std::string &serviceName="default");
    static bool registerForNotifications(
            const std::string &serviceName,
            const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification);
};

static inline std::string toString(const ::android::sp<::vendor::qti::hardware::fm::V1_0::IFmHci>& o) {
    std::string os = "[class or subclass of ";
    os += ::vendor::qti::hardware::fm::V1_0::IFmHci::descriptor;
    os += "]";
    os += o->isRemote() ? "@remote" : "@local";
    return os;
}


}  // namespace V1_0
}  // namespace fm
}  // namespace hardware
}  // namespace qti
}  // namespace vendor

#endif  // HIDL_GENERATED_VENDOR_QTI_HARDWARE_FM_V1_0_IFMHCI_H
