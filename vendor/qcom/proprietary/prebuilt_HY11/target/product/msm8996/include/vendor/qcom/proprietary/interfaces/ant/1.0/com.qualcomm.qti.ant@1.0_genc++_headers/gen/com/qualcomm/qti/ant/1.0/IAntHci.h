#ifndef HIDL_GENERATED_COM_QUALCOMM_QTI_ANT_V1_0_IANTHCI_H
#define HIDL_GENERATED_COM_QUALCOMM_QTI_ANT_V1_0_IANTHCI_H

#include <android/hidl/base/1.0/IBase.h>
#include <com/qualcomm/qti/ant/1.0/IAntHciCallbacks.h>
#include <com/qualcomm/qti/ant/1.0/types.h>

#include <android/hidl/manager/1.0/IServiceNotification.h>

#include <hidl/HidlSupport.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <utils/NativeHandle.h>
#include <utils/misc.h>

namespace com {
namespace qualcomm {
namespace qti {
namespace ant {
namespace V1_0 {

struct IAntHci : public ::android::hidl::base::V1_0::IBase {
    typedef android::hardware::details::i_tag _hidl_tag;

    // Forward declaration for forward reference support:

    virtual bool isRemote() const override { return false; }


    // @entry
    /**
     * Initialize the underlying ANT interface.
     * 
     */
    virtual ::android::hardware::Return<void> initialize(const ::android::sp<::com::qualcomm::qti::ant::V1_0::IAntHciCallbacks>& callback) = 0;

    virtual ::android::hardware::Return<void> sendAntControl(const ::android::hardware::hidl_vec<uint8_t>& command) = 0;

    virtual ::android::hardware::Return<void> sendAntData(const ::android::hardware::hidl_vec<uint8_t>& command) = 0;

    // @exit
    /**
     * Close the ANT interface
     */
    virtual ::android::hardware::Return<void> close() = 0;

    using interfaceChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& descriptors)>;
    virtual ::android::hardware::Return<void> interfaceChain(interfaceChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options) override;

    using interfaceDescriptor_cb = std::function<void(const ::android::hardware::hidl_string& descriptor)>;
    virtual ::android::hardware::Return<void> interfaceDescriptor(interfaceDescriptor_cb _hidl_cb) override;

    using getHashChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_array<uint8_t, 32>>& hashchain)>;
    virtual ::android::hardware::Return<void> getHashChain(getHashChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> setHALInstrumentation() override;

    virtual ::android::hardware::Return<bool> linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie) override;

    virtual ::android::hardware::Return<void> ping() override;

    using getDebugInfo_cb = std::function<void(const ::android::hidl::base::V1_0::DebugInfo& info)>;
    virtual ::android::hardware::Return<void> getDebugInfo(getDebugInfo_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> notifySyspropsChanged() override;

    virtual ::android::hardware::Return<bool> unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient) override;
    // cast static functions
    static ::android::hardware::Return<::android::sp<::com::qualcomm::qti::ant::V1_0::IAntHci>> castFrom(const ::android::sp<::com::qualcomm::qti::ant::V1_0::IAntHci>& parent, bool emitError = false);
    static ::android::hardware::Return<::android::sp<::com::qualcomm::qti::ant::V1_0::IAntHci>> castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError = false);

    static const char* descriptor;

    static ::android::sp<IAntHci> tryGetService(const std::string &serviceName="default", bool getStub=false);
    static ::android::sp<IAntHci> tryGetService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return tryGetService(str, getStub); }
    static ::android::sp<IAntHci> tryGetService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return tryGetService(str, getStub); }
    static ::android::sp<IAntHci> tryGetService(bool getStub) { return tryGetService("default", getStub); }
    static ::android::sp<IAntHci> getService(const std::string &serviceName="default", bool getStub=false);
    static ::android::sp<IAntHci> getService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return getService(str, getStub); }
    static ::android::sp<IAntHci> getService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return getService(str, getStub); }
    static ::android::sp<IAntHci> getService(bool getStub) { return getService("default", getStub); }
    __attribute__ ((warn_unused_result))::android::status_t registerAsService(const std::string &serviceName="default");
    static bool registerForNotifications(
            const std::string &serviceName,
            const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification);
};

static inline std::string toString(const ::android::sp<::com::qualcomm::qti::ant::V1_0::IAntHci>& o) {
    std::string os = "[class or subclass of ";
    os += ::com::qualcomm::qti::ant::V1_0::IAntHci::descriptor;
    os += "]";
    os += o->isRemote() ? "@remote" : "@local";
    return os;
}


}  // namespace V1_0
}  // namespace ant
}  // namespace qti
}  // namespace qualcomm
}  // namespace com

#endif  // HIDL_GENERATED_COM_QUALCOMM_QTI_ANT_V1_0_IANTHCI_H
