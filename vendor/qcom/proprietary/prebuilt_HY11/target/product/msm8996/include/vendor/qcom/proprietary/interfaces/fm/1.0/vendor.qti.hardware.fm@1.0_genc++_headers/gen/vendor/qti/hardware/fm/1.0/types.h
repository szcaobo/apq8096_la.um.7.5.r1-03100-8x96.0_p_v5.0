#ifndef HIDL_GENERATED_VENDOR_QTI_HARDWARE_FM_V1_0_TYPES_H
#define HIDL_GENERATED_VENDOR_QTI_HARDWARE_FM_V1_0_TYPES_H

#include <hidl/HidlSupport.h>
#include <hidl/MQDescriptor.h>
#include <utils/NativeHandle.h>
#include <utils/misc.h>

namespace vendor {
namespace qti {
namespace hardware {
namespace fm {
namespace V1_0 {

// Forward declaration for forward reference support:
enum class Status : int32_t;

enum class Status : int32_t {
    SUCCESS = 0,
    TRANSPORT_ERROR = 1, // (::vendor::qti::hardware::fm::V1_0::Status.SUCCESS implicitly + 1)
    INITIALIZATION_ERROR = 2, // (::vendor::qti::hardware::fm::V1_0::Status.TRANSPORT_ERROR implicitly + 1)
    UNKNOWN = 3, // (::vendor::qti::hardware::fm::V1_0::Status.INITIALIZATION_ERROR implicitly + 1)
};

/**
 * FM packets are transmitted as a vector of type uint8_t.
 */
typedef ::android::hardware::hidl_vec<uint8_t> HciPacket;

constexpr int32_t operator|(const ::vendor::qti::hardware::fm::V1_0::Status lhs, const ::vendor::qti::hardware::fm::V1_0::Status rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) | static_cast<int32_t>(rhs));
}

constexpr int32_t operator|(const int32_t lhs, const ::vendor::qti::hardware::fm::V1_0::Status rhs) {
    return static_cast<int32_t>(lhs | static_cast<int32_t>(rhs));
}

constexpr int32_t operator|(const ::vendor::qti::hardware::fm::V1_0::Status lhs, const int32_t rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) | rhs);
}

constexpr int32_t operator&(const ::vendor::qti::hardware::fm::V1_0::Status lhs, const ::vendor::qti::hardware::fm::V1_0::Status rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) & static_cast<int32_t>(rhs));
}

constexpr int32_t operator&(const int32_t lhs, const ::vendor::qti::hardware::fm::V1_0::Status rhs) {
    return static_cast<int32_t>(lhs & static_cast<int32_t>(rhs));
}

constexpr int32_t operator&(const ::vendor::qti::hardware::fm::V1_0::Status lhs, const int32_t rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) & rhs);
}

constexpr int32_t &operator|=(int32_t& v, const ::vendor::qti::hardware::fm::V1_0::Status e) {
    v |= static_cast<int32_t>(e);
    return v;
}

constexpr int32_t &operator&=(int32_t& v, const ::vendor::qti::hardware::fm::V1_0::Status e) {
    v &= static_cast<int32_t>(e);
    return v;
}

template<typename>
static inline std::string toString(int32_t o);
template<>
inline std::string toString<::vendor::qti::hardware::fm::V1_0::Status>(int32_t o) {
    using ::android::hardware::details::toHexString;
    std::string os;
    ::android::hardware::hidl_bitfield<::vendor::qti::hardware::fm::V1_0::Status> flipped = 0;
    bool first = true;
    if ((o & ::vendor::qti::hardware::fm::V1_0::Status::SUCCESS) == static_cast<int32_t>(::vendor::qti::hardware::fm::V1_0::Status::SUCCESS)) {
        os += (first ? "" : " | ");
        os += "SUCCESS";
        first = false;
        flipped |= ::vendor::qti::hardware::fm::V1_0::Status::SUCCESS;
    }
    if ((o & ::vendor::qti::hardware::fm::V1_0::Status::TRANSPORT_ERROR) == static_cast<int32_t>(::vendor::qti::hardware::fm::V1_0::Status::TRANSPORT_ERROR)) {
        os += (first ? "" : " | ");
        os += "TRANSPORT_ERROR";
        first = false;
        flipped |= ::vendor::qti::hardware::fm::V1_0::Status::TRANSPORT_ERROR;
    }
    if ((o & ::vendor::qti::hardware::fm::V1_0::Status::INITIALIZATION_ERROR) == static_cast<int32_t>(::vendor::qti::hardware::fm::V1_0::Status::INITIALIZATION_ERROR)) {
        os += (first ? "" : " | ");
        os += "INITIALIZATION_ERROR";
        first = false;
        flipped |= ::vendor::qti::hardware::fm::V1_0::Status::INITIALIZATION_ERROR;
    }
    if ((o & ::vendor::qti::hardware::fm::V1_0::Status::UNKNOWN) == static_cast<int32_t>(::vendor::qti::hardware::fm::V1_0::Status::UNKNOWN)) {
        os += (first ? "" : " | ");
        os += "UNKNOWN";
        first = false;
        flipped |= ::vendor::qti::hardware::fm::V1_0::Status::UNKNOWN;
    }
    if (o != flipped) {
        os += (first ? "" : " | ");
        os += toHexString(o & (~flipped));
    }os += " (";
    os += toHexString(o);
    os += ")";
    return os;
}

static inline std::string toString(::vendor::qti::hardware::fm::V1_0::Status o) {
    using ::android::hardware::details::toHexString;
    if (o == ::vendor::qti::hardware::fm::V1_0::Status::SUCCESS) {
        return "SUCCESS";
    }
    if (o == ::vendor::qti::hardware::fm::V1_0::Status::TRANSPORT_ERROR) {
        return "TRANSPORT_ERROR";
    }
    if (o == ::vendor::qti::hardware::fm::V1_0::Status::INITIALIZATION_ERROR) {
        return "INITIALIZATION_ERROR";
    }
    if (o == ::vendor::qti::hardware::fm::V1_0::Status::UNKNOWN) {
        return "UNKNOWN";
    }
    std::string os;
    os += toHexString(static_cast<int32_t>(o));
    return os;
}


}  // namespace V1_0
}  // namespace fm
}  // namespace hardware
}  // namespace qti
}  // namespace vendor
namespace android {
namespace hardware {
template<> struct hidl_enum_iterator<::vendor::qti::hardware::fm::V1_0::Status>
{
    const ::vendor::qti::hardware::fm::V1_0::Status* begin() { return static_begin(); }
    const ::vendor::qti::hardware::fm::V1_0::Status* end() { return begin() + 4; }
    private:
    static const ::vendor::qti::hardware::fm::V1_0::Status* static_begin() {
        static const ::vendor::qti::hardware::fm::V1_0::Status kVals[4] {
            ::vendor::qti::hardware::fm::V1_0::Status::SUCCESS,
            ::vendor::qti::hardware::fm::V1_0::Status::TRANSPORT_ERROR,
            ::vendor::qti::hardware::fm::V1_0::Status::INITIALIZATION_ERROR,
            ::vendor::qti::hardware::fm::V1_0::Status::UNKNOWN,
        };
        return &kVals[0];
    }};

}  // namespace hardware
}  // namespace android

#endif  // HIDL_GENERATED_VENDOR_QTI_HARDWARE_FM_V1_0_TYPES_H
