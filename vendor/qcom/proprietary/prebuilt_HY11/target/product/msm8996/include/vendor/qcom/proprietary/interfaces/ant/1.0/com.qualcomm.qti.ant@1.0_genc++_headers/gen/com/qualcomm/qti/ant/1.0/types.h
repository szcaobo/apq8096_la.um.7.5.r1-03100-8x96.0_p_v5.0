#ifndef HIDL_GENERATED_COM_QUALCOMM_QTI_ANT_V1_0_TYPES_H
#define HIDL_GENERATED_COM_QUALCOMM_QTI_ANT_V1_0_TYPES_H

#include <hidl/HidlSupport.h>
#include <hidl/MQDescriptor.h>
#include <utils/NativeHandle.h>
#include <utils/misc.h>

namespace com {
namespace qualcomm {
namespace qti {
namespace ant {
namespace V1_0 {

// Forward declaration for forward reference support:
enum class Status : int32_t;

enum class Status : int32_t {
    SUCCESS = 0,
    TRANSPORT_ERROR = 1, // (::com::qualcomm::qti::ant::V1_0::Status.SUCCESS implicitly + 1)
    INITIALIZATION_ERROR = 2, // (::com::qualcomm::qti::ant::V1_0::Status.TRANSPORT_ERROR implicitly + 1)
    UNKNOWN = 3, // (::com::qualcomm::qti::ant::V1_0::Status.INITIALIZATION_ERROR implicitly + 1)
};

/**
 * FM packets are transmitted as a vector of type uint8_t.
 */
typedef ::android::hardware::hidl_vec<uint8_t> AntPacket;

constexpr int32_t operator|(const ::com::qualcomm::qti::ant::V1_0::Status lhs, const ::com::qualcomm::qti::ant::V1_0::Status rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) | static_cast<int32_t>(rhs));
}

constexpr int32_t operator|(const int32_t lhs, const ::com::qualcomm::qti::ant::V1_0::Status rhs) {
    return static_cast<int32_t>(lhs | static_cast<int32_t>(rhs));
}

constexpr int32_t operator|(const ::com::qualcomm::qti::ant::V1_0::Status lhs, const int32_t rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) | rhs);
}

constexpr int32_t operator&(const ::com::qualcomm::qti::ant::V1_0::Status lhs, const ::com::qualcomm::qti::ant::V1_0::Status rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) & static_cast<int32_t>(rhs));
}

constexpr int32_t operator&(const int32_t lhs, const ::com::qualcomm::qti::ant::V1_0::Status rhs) {
    return static_cast<int32_t>(lhs & static_cast<int32_t>(rhs));
}

constexpr int32_t operator&(const ::com::qualcomm::qti::ant::V1_0::Status lhs, const int32_t rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) & rhs);
}

constexpr int32_t &operator|=(int32_t& v, const ::com::qualcomm::qti::ant::V1_0::Status e) {
    v |= static_cast<int32_t>(e);
    return v;
}

constexpr int32_t &operator&=(int32_t& v, const ::com::qualcomm::qti::ant::V1_0::Status e) {
    v &= static_cast<int32_t>(e);
    return v;
}

template<typename>
static inline std::string toString(int32_t o);
template<>
inline std::string toString<::com::qualcomm::qti::ant::V1_0::Status>(int32_t o) {
    using ::android::hardware::details::toHexString;
    std::string os;
    ::android::hardware::hidl_bitfield<::com::qualcomm::qti::ant::V1_0::Status> flipped = 0;
    bool first = true;
    if ((o & ::com::qualcomm::qti::ant::V1_0::Status::SUCCESS) == static_cast<int32_t>(::com::qualcomm::qti::ant::V1_0::Status::SUCCESS)) {
        os += (first ? "" : " | ");
        os += "SUCCESS";
        first = false;
        flipped |= ::com::qualcomm::qti::ant::V1_0::Status::SUCCESS;
    }
    if ((o & ::com::qualcomm::qti::ant::V1_0::Status::TRANSPORT_ERROR) == static_cast<int32_t>(::com::qualcomm::qti::ant::V1_0::Status::TRANSPORT_ERROR)) {
        os += (first ? "" : " | ");
        os += "TRANSPORT_ERROR";
        first = false;
        flipped |= ::com::qualcomm::qti::ant::V1_0::Status::TRANSPORT_ERROR;
    }
    if ((o & ::com::qualcomm::qti::ant::V1_0::Status::INITIALIZATION_ERROR) == static_cast<int32_t>(::com::qualcomm::qti::ant::V1_0::Status::INITIALIZATION_ERROR)) {
        os += (first ? "" : " | ");
        os += "INITIALIZATION_ERROR";
        first = false;
        flipped |= ::com::qualcomm::qti::ant::V1_0::Status::INITIALIZATION_ERROR;
    }
    if ((o & ::com::qualcomm::qti::ant::V1_0::Status::UNKNOWN) == static_cast<int32_t>(::com::qualcomm::qti::ant::V1_0::Status::UNKNOWN)) {
        os += (first ? "" : " | ");
        os += "UNKNOWN";
        first = false;
        flipped |= ::com::qualcomm::qti::ant::V1_0::Status::UNKNOWN;
    }
    if (o != flipped) {
        os += (first ? "" : " | ");
        os += toHexString(o & (~flipped));
    }os += " (";
    os += toHexString(o);
    os += ")";
    return os;
}

static inline std::string toString(::com::qualcomm::qti::ant::V1_0::Status o) {
    using ::android::hardware::details::toHexString;
    if (o == ::com::qualcomm::qti::ant::V1_0::Status::SUCCESS) {
        return "SUCCESS";
    }
    if (o == ::com::qualcomm::qti::ant::V1_0::Status::TRANSPORT_ERROR) {
        return "TRANSPORT_ERROR";
    }
    if (o == ::com::qualcomm::qti::ant::V1_0::Status::INITIALIZATION_ERROR) {
        return "INITIALIZATION_ERROR";
    }
    if (o == ::com::qualcomm::qti::ant::V1_0::Status::UNKNOWN) {
        return "UNKNOWN";
    }
    std::string os;
    os += toHexString(static_cast<int32_t>(o));
    return os;
}


}  // namespace V1_0
}  // namespace ant
}  // namespace qti
}  // namespace qualcomm
}  // namespace com
namespace android {
namespace hardware {
template<> struct hidl_enum_iterator<::com::qualcomm::qti::ant::V1_0::Status>
{
    const ::com::qualcomm::qti::ant::V1_0::Status* begin() { return static_begin(); }
    const ::com::qualcomm::qti::ant::V1_0::Status* end() { return begin() + 4; }
    private:
    static const ::com::qualcomm::qti::ant::V1_0::Status* static_begin() {
        static const ::com::qualcomm::qti::ant::V1_0::Status kVals[4] {
            ::com::qualcomm::qti::ant::V1_0::Status::SUCCESS,
            ::com::qualcomm::qti::ant::V1_0::Status::TRANSPORT_ERROR,
            ::com::qualcomm::qti::ant::V1_0::Status::INITIALIZATION_ERROR,
            ::com::qualcomm::qti::ant::V1_0::Status::UNKNOWN,
        };
        return &kVals[0];
    }};

}  // namespace hardware
}  // namespace android

#endif  // HIDL_GENERATED_COM_QUALCOMM_QTI_ANT_V1_0_TYPES_H
