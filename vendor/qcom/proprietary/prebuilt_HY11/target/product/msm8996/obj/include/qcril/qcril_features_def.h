/******************************************************************************
  @file    qcril_features_def.h
  @brief   qcril feature definition information

  DESCRIPTION

  ---------------------------------------------------------------------------

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
  ---------------------------------------------------------------------------
******************************************************************************/
#ifndef QCRIL_FEATURES_DEF_H
#define QCRIL_FEATURES_DEF_H

#define EXTENDED_FAIL_ERROR_CAUSE_FOR_VOICE_CALL (1)
#define HOLD_RESUME_ERROR_CODES (1)
#define QCRIL_FEATURES_DEF_MAX_REQUEST_ID      145
#define QCRIL_FEATURES_DEF_MAX_UNSOL_ID       1050
#define QCRIL_FEATURES_DEF_MAX_ERROR_ID        525
#define RIL_REQUEST_GET_ADN_RECORD (300)
#define RIL_REQUEST_UPDATE_ADN_RECORD (301)
#define RIL_UNSOL_RESPONSE_ADN_INIT_DONE (1046)
#define RIL_UNSOL_RESPONSE_ADN_RECORDS (1047)
#define RIL_NUM_ADN_RECORDS (10)
#define RIL_MAX_NUM_AD_COUNT (4)
#define RIL_MAX_NUM_EMAIL_COUNT (2)
typedef struct {                                  
    int       record_id;                                  
    char*     name;                                  
    char*     number;                                  
    int       email_elements;                                  
    char*     email[RIL_MAX_NUM_EMAIL_COUNT];                                  
    int       anr_elements;                                  
    char*     ad_number[RIL_MAX_NUM_AD_COUNT];                                  
} RIL_AdnRecordInfo;
typedef struct {                                  
    int               record_elements;                                  
    RIL_AdnRecordInfo adn_record_info[RIL_NUM_ADN_RECORDS];                                  
} RIL_AdnRecord_v1;

#endif /* QCRIL_FEATURES_DEF_H */
