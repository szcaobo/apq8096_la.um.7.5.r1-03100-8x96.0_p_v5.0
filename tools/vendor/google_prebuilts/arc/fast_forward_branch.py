#!/usr/bin/python3
#
# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Helper to fast-forward branch to a given build number."""

import argparse
import logging
import os
import subprocess
import sys
import tempfile
import xml.etree.ElementTree

import lib.build_artifact_fetcher
import lib.manifest_file
import lib.util

_CURRENT_MASTER_BRANCH = 'nyc-mr1-arc'
_REPO_SYNC_PARALLELISM = 16
_SHALLOW_HISTORY_PROJECTS = ['vendor/partner_gms']
_DUMMY_BRANCH = 'fast-forward-empty'

_REPO_ROOT = lib.util.find_repo_root()

def _suspend_active_projects():
  """Suspend active branches by repo-starting a dummy branch."""
  logging.info('Making sure there is no active branches...')
  output = lib.util.check_output('repo', 'info', '-o', '-b', cwd=_REPO_ROOT)
  active_projects = []
  lines = output.splitlines()
  for line in lines:
    line = line.strip()

    if line.startswith('* '):
      logging.info('Found active branches in %s', last_line)
      active_projects.append(last_line)

    cols = line.split(': ')
    if len(cols) == 2:
      if cols[0] == 'Manifest branch' and cols[1] != _CURRENT_MASTER_BRANCH:
        logging.error('Current branch is %s (expected: %s)', cols[1],
                      _CURRENT_MASTER_BRANCH)
        sys.exit(1)
    else:
      last_line = line

  if active_projects:
    lib.util.check_call('repo', 'start', _DUMMY_BRANCH, *active_projects,
                        cwd=_REPO_ROOT)
  return active_projects

def _repo_sync():
  """Sync the current repo to the latest."""
  lib.util.check_call('repo', 'sync', '-q', '-c',
                       '-j%d' % _REPO_SYNC_PARALLELISM,
                      cwd=_REPO_ROOT)

def _repo_snapshot_manifest(filename):
  lib.util.check_call('repo', 'manifest', '-r', '-o', filename, cwd=_REPO_ROOT)

def _repo_switch_branch(branch):
  """Switch to the given repo branch and sync."""
  lib.util.check_call('repo', 'init', '-q', '-b', branch, cwd=_REPO_ROOT)
  _repo_sync()

def _collect_project_info(projects):
  """Collect interested project info."""
  info = {}
  for project in projects:
    if not project.groups or 'notdefault' in project.groups:
      continue
    info[project.path or project.name] = project.revision
  return info

def _find_diff(target_info, base_info):
  """Returns target revision of changed git repositories."""
  # Note: deleted project on ToT is not removed from release branch for now.
  diff = {}
  has_error = False
  for name, revision in target_info.items():
    if name not in base_info:
      logging.error('Missing project in the release branch: %s', name)
      has_error = True
      continue

    if revision != base_info[name]:
      logging.debug('%s: %s -> %s', name, base_info[name], revision)
      diff[name] = revision

  if has_error:
    logging.warning('FYI: repos are not reset for you to debug.')
    sys.exit(1)

  return diff

def _local_branch_name(release_branch, target_build):
  """Construct the local branch name during the work."""
  return 'fast-forward-%s-%s' % (release_branch, target_build)

def _fast_forward(args):
  """Fast forward the specified (release) branch to the given build."""
  logging.info('Switch to branch %s', args.release_branch)
  _repo_switch_branch(args.release_branch)

  # Gather info of current base branch, which is supposed to be a release
  # branch to be fast-forwarded.
  with tempfile.NamedTemporaryFile() as manifest_file:
    _repo_snapshot_manifest(manifest_file.name)
    manifest = lib.manifest_file.ManifestFile(manifest_file.name)
    manifest.parse_manifest()
    base_info = _collect_project_info(manifest.projects)

  # Gather info of target repo manifest to fast-forward to.
  logging.info('Gathering repo manifest from ab/%s', args.target_build)
  artifact_fetcher = lib.build_artifact_fetcher.BuildArtifactFetcher(
      'arm', 'user', args.target_build)
  manifest = lib.manifest_file.RemoteManifestFile(artifact_fetcher)
  manifest.download_manifest()
  manifest.parse_manifest()
  target_info = _collect_project_info(manifest.projects)

  diff_info = _find_diff(target_info, base_info)
  if not diff_info:
    logging.info('Everything up to date.')
    sys.exit(0)

  logging.info('Projects with diff:')
  for project in diff_info.keys():
    logging.info(' * %s', project)

  branch_name = _local_branch_name(args.release_branch, args.target_build)
  cmd = ['repo', 'start', branch_name] + list(diff_info.keys())
  lib.util.check_call(*cmd, cwd=_REPO_ROOT)
  need_manual_merge = []
  for path, commit in diff_info.items():
    project_path = os.path.join(_REPO_ROOT, path)

    # This can happen when ToT is not synced pass to the target manifest, or
    # when the project has max depth.
    try:
      lib.util.check_call('git', 'cat-file', '-e', commit,
                          cwd=project_path)
    except subprocess.CalledProcessError:
      logging.info('Target commit is not found locally. Fetching from remote.')
      lib.util.check_call('git', 'fetch', 'goog', commit,
                          cwd=project_path)

    logging.info('Merging %s to %s', path, commit)
    try:
      cmd = ['git', 'merge', '--no-ff', '--log',
             '-m', 'ARC Release merge: %s to ab/%s\n\nBug: %s\nTest: None' % (
                 args.release_branch, args.target_build, args.bug),
             commit]

      # Special case: Projects may not be common history to merge.  Note that
      # unlike text, binary is hard to resolve.  "-X theirs" tells the recursive
      # merge strategy to use the target build's version if the original merge
      # fails.
      if path in _SHALLOW_HISTORY_PROJECTS:
        cmd.extend(['--allow-unrelated-histories', '-X', 'theirs'])

      lib.util.check_call(*cmd, cwd=project_path)

      # Just to generate Change-Id since it is done by commit-msg hook.
      lib.util.check_call('git', 'commit', '--amend', '--no-edit',
                          cwd=project_path)
    except subprocess.CalledProcessError:
      logging.error('Failed to merge %s.  Needs manual merge!', path)
      need_manual_merge.append(path)

    # Sanity check the HEAD content.  There should be no different comparing to
    # the target build.
    diff = lib.util.check_output('git', 'diff', commit, cwd=project_path)
    if diff:
      logging.error('Merge result is different from the target build!')
      logging.error('`git-diff %s` shows:\n%s', commit, diff)
      sys.exit(1)

    logging.info('-' * 70)

  if need_manual_merge:
    logging.error('')
    logging.error('Merge conflict. Some projects require manual merge.')
    for project in sorted(need_manual_merge):
      logging.error(' * %s', project)
    logging.error('Please resolve all conflicts, then git add, git commit. '
                  'When finished, upload to Gerrit by:')
    logging.error('  repo upload -t --br %s --no-verify', branch_name)
  elif args.skip_upload:
    logging.info('Skipping the upload of branch %s', branch_name)
  else:
    logging.info('Uploading all changes on branch %s', branch_name)
    lib.util.check_call('repo', 'upload', '-t', '--br', branch_name,
                        '--no-verify', cwd=_REPO_ROOT)

def _abandon(args):
  """Abandon work in progress."""
  branch_name = _local_branch_name(args.release_branch, args.target_build)
  # TODO(victorhsieh): should also reset merge status.
  if subprocess.call(['repo', 'abandon', branch_name], cwd=_REPO_ROOT) != 0:
    logging.warning('Nothing changed since %s does not exist', branch_name)
  _repo_switch_branch(_CURRENT_MASTER_BRANCH)

def _verify_arguments(args):
  if args.action == 'fast-forward' and not args.bug:
    logging.error('--bug/-b must be provided for fast-forward')
    sys.exit(1)

def main():
  parser = argparse.ArgumentParser(description=__doc__)
  parser.add_argument('action', choices=('fast-forward', 'abandon'),
                      help='Action to perform.')
  parser.add_argument('release_branch', metavar='release-branch', type=str,
                      help='The release branch to fast-forward.')
  parser.add_argument('target_build', metavar='target-build', type=str,
                      help=('The build number that contains the manifest to '
                            'fast-forward to.'))
  parser.add_argument('-b', '--bug', type=str,
                      help='Bug number to associate with.')
  parser.add_argument('-v', '--verbose', action='count', default=0,
                      help='Shows more messages.')
  parser.add_argument('--skip-upload', action='store_true',
                      help='Skip uploading to Gerrit.')
  args = parser.parse_args()

  _verify_arguments(args)

  logger = logging.getLogger()
  logging.basicConfig(format='%(levelname)s: %(message)s',
                      level=(logging.INFO, logging.DEBUG)[args.verbose])

  try:
    if args.action == 'fast-forward':
      projects = None
      try:
        projects = _suspend_active_projects()
        _fast_forward(args)
      finally:
        if projects:
          lib.util.check_call('repo', 'abandon', _DUMMY_BRANCH, *projects,
                              cwd=_REPO_ROOT)
    elif args.action == 'abandon':
      _abandon(args)

  except Exception as e:
    logging.exception('Failed to perform action')
    logging.error('To abandon, run `%s abandon <release-branch> '
                  '<target-build>`', sys.argv[0])

if __name__ == '__main__':
  main()
