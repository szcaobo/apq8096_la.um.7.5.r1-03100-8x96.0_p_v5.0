# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Functions for dealing with the crash dump server"""

import logging
import os
import re
import urllib.parse

import lib.util as util

_CRASH_NETLOCS = ('crash.corp.google.com', 'crash')


def is_crash_server_url(url):
  result = urllib.parse.urlparse(url)
  return result.netloc in _CRASH_NETLOCS


def get_report_id(url):
  result = urllib.parse.urlparse(url)
  assert result.netloc in _CRASH_NETLOCS
  if result.path == '/browse':
    query = urllib.parse.parse_qs(result.query)
    if 'reportid' in query:
      return query['reportid'][0]
    else:
      assert 'q' in query
      m = re.search(r"reportid='([0-9a-fA-F]+)'", query['q'][0].lower())
      assert m, query['q']
      return m.group(1)
  elif re.match('/[0-9a-fA-F]+', result.path):
    return result.path[1:]
  else:
    assert False, 'Unable to extract the report id from "%s"' % url


def download_minidump(crash_id):
  minidump_path = util.helper_temp_path('crash', crash_id, crash_id + '.dmp')
  if os.path.exists(minidump_path):
    return minidump_path

  logging.info('Downloading minidump %s', minidump_path)

  url = urllib.parse.urlunparse((
      'https',
      _CRASH_NETLOCS[0],
      '/fetch_file',
      '',
      urllib.parse.urlencode(dict(
          filename='upload_file_minidump',
          reportid=crash_id)),
      ''))

  util.makedirs(os.path.dirname(minidump_path))

  # We download to a slightly different path just in case we fail and end up
  # with a partially downloaded file.
  minidump_path_tmp = minidump_path + '.tmp'
  with open(minidump_path_tmp, 'wb') as minidump_file:
    util.check_call(
        '/usr/bin/sso_client', '-location', '-request_timeout', '300', url,
        stdout=minidump_file)
  os.rename(minidump_path_tmp, minidump_path)

  return minidump_path
