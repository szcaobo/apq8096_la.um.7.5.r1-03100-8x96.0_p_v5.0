# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Wrapper for minidump files"""

import collections
import logging
import os
import re
import subprocess

import lib.util as util


ModuleInfo = collections.namedtuple(
    'Module', ['name', 'hash'])


def is_minidump(path):
  with open(path, 'rb') as f:
    sig = f.read(4)
    return sig == b'MDMP'


class MinidumpFile(object):
  def __init__(self, path, arc_build_id=None):
    assert is_minidump(path), '"%s" is not a minidump.' % path
    self._path = path
    self._arc_build_id = arc_build_id or self._read_arc_build_id()
    self._arch = None
    self._modules = []

    self._read_arch_and_modules()

  def _read_arc_build_id(self):
    with open(self._path, 'rb') as df:
      m = re.search(rb'CHROMEOS_ARC_VERSION=(\d+)', df.read())
      if not m:
        return None
      return m.group(1).decode('utf-8')

  def _read_arch_and_modules(self):
    output = util.check_output(
        util.get_prebuilt('minidump_stackwalk'), '-m', self._path,
        stderr=subprocess.DEVNULL)
    logging.debug(output)

    arch_match = re.search(r'^CPU\|.*$', output, re.MULTILINE)
    self._arch = arch_match.group(0).split('|')[1]
    if self._arch == 'amd64':
      self._arch = 'x86_64'

    modules = [
        ModuleInfo(*module_match.group(0).split('|')[3:5])
        for module_match in re.finditer(r'^Module\|.*$', output, re.MULTILINE)]

    self._modules = [m for m in modules if m.name]

  @property
  def arc_build_id(self):
    return self._arc_build_id

  @property
  def arch(self):
    return self._arch

  @property
  def modules(self):
    return self._modules

  def stacktrace_with_symbols(self, symbol_path):
    util.check_call(
        util.get_prebuilt('minidump_stackwalk'), self._path, symbol_path)

  def convert_to_core(self):
    core_path = os.path.splitext(self._path)[0] + '.core'
    if os.path.exists(core_path):
      return core_path

    logging.info('Converting minidump %s to core', self._path)

    program = None
    if self._arch == 'x86':
      program = util.get_prebuilt('minidump-2-core')
    elif self._arch == 'x86_64':
      program = util.get_prebuilt('x86_64-linux-gnu-minidump-2-core')
    elif self._arch == 'arm':
      program = util.get_prebuilt('arm-linux-gnueabihf-minidump-2-core')
    else:
      raise ValueError('Unsupported arch: %s' % self._arch)

    # We output to a slightly different path just in case we fail and end up with
    # a partially processed file.
    core_path_tmp = core_path + '.tmp'
    with open(core_path_tmp, 'wb') as crash_file:
      util.check_call(program, self._path, stdout=crash_file)
    os.rename(core_path_tmp, core_path)

    return core_path
