# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A helper class for parsing an Android fingerprint entry from a log file."""

import logging
import os
import re

import lib.build_artifact_fetcher
import lib.manifest_file
import lib.symbol_file


class Fingerprint(object):
  def __init__(self, is_local_build, build_id, variant, abi):
    self._is_local_build = is_local_build
    self._build_id = build_id
    self._variant = variant
    self._abi = abi

    self._remote_artifact_fetcher = None
    self._remote_manifest = None
    self._remote_symbols = None

  @property
  def is_local_build(self):
    return self._is_local_build

  @property
  def build_id(self):
    return self._build_id

  @property
  def variant(self):
    return self._variant

  @property
  def abi(self):
    return self._abi

  def get_artifact_fetcher(self):
    if not self._remote_artifact_fetcher and not self._is_local_build:
      # Get information about the build from the build artifacts that were
      # stored for it.
      self._remote_artifact_fetcher = (
          lib.build_artifact_fetcher.BuildArtifactFetcher(
              self._abi, self._variant, self._build_id))

    return self._remote_artifact_fetcher

  def get_remote_manifest(self):
    if not self._remote_manifest and not self._is_local_build:
      # We fetch the manifest file so we know what versions of sources to look
      # at
      self._remote_manifest = lib.manifest_file.RemoteManifestFile(
          self.get_artifact_fetcher())
      self._remote_manifest.download_manifest()
      self._remote_manifest.parse_manifest()

    return self._remote_manifest

  def get_remote_symbols(self):
    if not self._remote_symbols and not self._is_local_build:
      # We fetch the symbol files so we know how to go from crash address to
      # files and lines.
      self._remote_symbols = lib.symbol_file.RemoteSymbolFile(
          self.get_artifact_fetcher())
      self._remote_symbols.unzip_symbols()
      logging.debug('Symbols unzipped to: %s',
                    self._remote_symbols.symbol_start_path)

    return self._remote_symbols

  def get_symbol_start_path(self):
    if self._is_local_build:
      # Use the local symbol paths for the build, assuming that the cwd is the
      # root of the Android checkout.
      symbol_start_path = 'out/target/product/cheets_%s/symbols' % self._abi
    else:
      symbol_start_path = self.get_remote_symbols().symbol_start_path

    return symbol_start_path


def get_fingerprint_from_log_content(content):
  """Parses the fingerprint out of |content|.

  Returns:
      An instance of Fingerprint, or None if the full fingerprint information
      was not parsed.
  """

  # pylint: disable=line-too-long
  #
  # Fingerprints lines are expected to look like:
  #
  #     <ignored-prefix> Build fingerprint: 'google/codename/codename_cheets:7.1.1/R58-9300.0.0/1234567:user/release-keys'
  #
  # Here 1234567 is the Android build id, which we need to identify the
  # build the produced it. We also need the variant, which here is 'user'
  # (could also be 'userdebug' or 'eng')
  #
  # pylint: enable=line-too-long

  # Look for the identifier text, and extract everything inside the single
  # quotes.
  m = re.search(r"Build fingerprint: '([^']+)'$", content, re.MULTILINE)
  if not m:
    return None
  fp = m.group(1)
  logging.debug('Got fingerprint: %s', fp)

  # Extract the build and variant strings.
  _, build_part, variant_part = fp.split(':')
  build_id = build_part.rsplit('/', 1)[1]
  variant = variant_part.split('/', 1)[0]

  m = re.search(r"ABI: '([^']+)'$", content, re.MULTILINE)
  if not m:
    return None
  abi = m.group(1)
  logging.debug('Got abi: %s', abi)

  # If the build_id begins with the first six characters of the current user's
  # username (per build/core/version_defaults.mk), we assume it is a local
  # build. Otherwise we expect it to be an automated build.
  username = os.environ.get('USER')
  is_local_build = build_id.startswith(username[:6])

  return Fingerprint(is_local_build, build_id, variant, abi)
