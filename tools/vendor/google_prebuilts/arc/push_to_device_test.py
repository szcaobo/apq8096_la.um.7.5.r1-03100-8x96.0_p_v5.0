#!/usr/bin/python3
# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import hashlib
import os
import shutil
import sys
import tempfile
import unittest

import push_to_device


class SysExitException(Exception):
  pass


class MockSysExit(object):
  def __init__(self):
    self.called = False

  def __call__(self, args):
    self.called = True
    raise SysExitException


class ScopedTempDir(object):
  def __init__(self):
    self._path = None

  def __enter__(self):
    self._path = tempfile.mkdtemp()
    return self._path

  def __exit__(self, exc_value, exc_type, traceback):
    shutil.rmtree(self._path, ignore_errors=True)


class ScopedMonkeyPatch(object):
  """Modify an attribute or method within the context.

  Example:
    with ScopedMonkeyPatch(os.path, 'isfile', lambda x: True):
      assertTrue(os.path.isfile('whatever'))
  """
  def __init__(self, obj, name, value):
    """Init with with attribute to override.

    Args:
      obj: An object or module to modify the attribute.
      name: The name of the attribute of |obj|.  Must not be None.
      value: The new value of |obj|'s |name| attribute.
    """
    assert name is not None
    self._orig = None
    self._obj = obj
    self._name = name
    self._value = value

  def __enter__(self):
    if hasattr(self._obj, self._name):
      self._orig = getattr(self._obj, self._name)
    else:
      self._orig = None
    setattr(self._obj, self._name, self._value)

  def __exit__(self, exc_value, exc_type, traceback):
    if self._orig is None:
      delattr(self._obj, self._name)
    else:
      setattr(self._obj, self._name, self._orig)


class PushToDeviceTest(unittest.TestCase):
  def test_verify_machine_arch(self):
    class MockRemoteProxy(object):
      def __init__(self, assert_command, uname):
        self._assert_command = assert_command
        self._uname = uname

      def check_output(self, command):
        self._assert_command(command)
        return self._uname

    x86_64_proxy = MockRemoteProxy(
        lambda command: self.assertEqual('uname -m', command), 'x86_64')
    arm_proxy = MockRemoteProxy(
        lambda command: self.assertEqual('uname -m', command), 'armv7l')
    dry_run = True

    # Passing test cases for dry runs.
    # pylint: disable=protected-access
    push_to_device._verify_machine_arch(
        x86_64_proxy, 'aosp_cheets_x86', dry_run)
    push_to_device._verify_machine_arch(
        x86_64_proxy, 'aosp_cheets_x86_64', dry_run)
    push_to_device._verify_machine_arch(x86_64_proxy, 'cheets_x86', dry_run)
    push_to_device._verify_machine_arch(x86_64_proxy, 'cheets_x86_64', dry_run)
    push_to_device._verify_machine_arch(
        x86_64_proxy, 'sdk_google_cheets_x86', dry_run)
    push_to_device._verify_machine_arch(
        x86_64_proxy, 'sdk_google_cheets_x86_64', dry_run)
    push_to_device._verify_machine_arch(arm_proxy, 'cheets_arm', dry_run)

    # Passing test cases.
    push_to_device._verify_machine_arch(
        x86_64_proxy, 'aosp_cheets_x86', not dry_run)
    push_to_device._verify_machine_arch(
        x86_64_proxy, 'aosp_cheets_x86_64', not dry_run)
    push_to_device._verify_machine_arch(x86_64_proxy, 'cheets_x86', not dry_run)
    push_to_device._verify_machine_arch(
        x86_64_proxy, 'cheets_x86_64', not dry_run)
    push_to_device._verify_machine_arch(
        x86_64_proxy, 'sdk_google_cheets_x86', not dry_run)
    push_to_device._verify_machine_arch(
        x86_64_proxy, 'sdk_google_cheets_x86_64', not dry_run)
    push_to_device._verify_machine_arch(arm_proxy, 'cheets_arm', not dry_run)

    # Failure cases.
    with self.assertRaisesRegex(AssertionError, 'Architecture mismatch'):
      push_to_device._verify_machine_arch(
          x86_64_proxy, 'cheets_arm', not dry_run)
    with self.assertRaisesRegex(AssertionError, 'Architecture mismatch'):
      push_to_device._verify_machine_arch(
          arm_proxy, 'aosp_cheets_x86', not dry_run)
    with self.assertRaisesRegex(AssertionError, 'Architecture mismatch'):
      push_to_device._verify_machine_arch(
          arm_proxy, 'aosp_cheets_x86_64', not dry_run)
    with self.assertRaisesRegex(AssertionError, 'Architecture mismatch'):
      push_to_device._verify_machine_arch(arm_proxy, 'cheets_x86', not dry_run)
    with self.assertRaisesRegex(AssertionError, 'Architecture mismatch'):
      push_to_device._verify_machine_arch(
          arm_proxy, 'cheets_x86_64', not dry_run)
    with self.assertRaisesRegex(AssertionError, 'Architecture mismatch'):
      push_to_device._verify_machine_arch(
          arm_proxy, 'sdk_google_cheets_x86', not dry_run)
    with self.assertRaisesRegex(AssertionError, 'Architecture mismatch'):
      push_to_device._verify_machine_arch(
          arm_proxy, 'sdk_google_cheets_x86_64', not dry_run)
    with self.assertRaisesRegex(AssertionError, 'Architecture mismatch'):
      push_to_device._verify_machine_arch(
          x86_64_proxy, 'INVALID_TARGET', not dry_run)
    with self.assertRaisesRegex(AssertionError, 'Architecture mismatch'):
      push_to_device._verify_machine_arch(
          arm_proxy, 'INVALID_TARGET', not dry_run)

  def test_convert_images(self):
    simg2img = push_to_device.Simg2img('/path/to/simg2img', True)
    push_vendor_image = True
    # pylint: disable=protected-access
    self.assertEqual(
        push_to_device._convert_images(simg2img, 'OUT', 'x86', push_vendor_image,
                                       '/path/to/mksquashfs',
                                       '/path/to/unsquashfs',
                                       '/path/to/shift_uid_py', dryrun=True),
        (['OUT/system.raw.img'], ['OUT/vendor.raw.img']))
    self.assertEqual(
        push_to_device._convert_images(simg2img, 'OUT', 'x86', not push_vendor_image,
                                       '/path/to/mksquashfs',
                                       '/path/to/unsquashfs',
                                       '/path/to/shift_uid_py', dryrun=True),
        (['OUT/system.raw.img'], []))

  def test_is_selinux_policy_updated(self):
    class MockRemoteProxy(object):
      def check_output(self, *_):
        return hashlib.sha1(b'ABCDE').hexdigest() + '  test_file_name'

    # pylint: disable=protected-access
    with ScopedTempDir() as tempdir:
      os.makedirs(os.path.join(tempdir, 'root'))
      with open(os.path.join(tempdir, 'root', 'sepolicy'), 'wb') as f:
        f.write(b'12345')
      # If the sepolicy contents are different, it is updated.
      self.assertTrue(push_to_device._is_selinux_policy_updated(
          MockRemoteProxy(), tempdir, False))

    with ScopedTempDir() as tempdir:
      os.makedirs(os.path.join(tempdir, 'root'))
      with open(os.path.join(tempdir, 'root', 'sepolicy'), 'wb') as f:
        f.write(b'ABCDE')
      # If the sepolicy contents are same, it is not updated.
      self.assertFalse(push_to_device._is_selinux_policy_updated(
          MockRemoteProxy(), tempdir, False))

  def test_reboot(self):
    class MockRemoteProxy(object):
      def __init__(self):
        self.command_history = []

      def check_call(self, command):
        self.command_history.append(command)

    # If selinux policy is updated, it should trigger the rebooting.
    remote_proxy = MockRemoteProxy()
    with push_to_device.ImageUpdateMode(remote_proxy, True, False, False, True):
      pass
    # The last command should be reboot.
    self.assertEqual(remote_proxy.command_history[-1], 'reboot')

    # If selinux policy is not updated, the last command is just restarting UI,
    # and not reboot the machine.
    remote_proxy = MockRemoteProxy()
    with push_to_device.ImageUpdateMode(
        remote_proxy, False, False, False, True):
      pass
    self.assertNotIn('reboot', remote_proxy.command_history)
    self.assertIn('start ui', remote_proxy.command_history[-1])

  def test_default_tool_path(self):
    host_dir = '/arc++/out/host/linux-x86'
    bin_path = host_dir + '/bin/simg2img'
    temp_env = os.environ.copy()
    temp_env['ANDROID_HOST_OUT'] = host_dir
    # pylint: disable=protected-access
    with ScopedMonkeyPatch(os, 'environ', temp_env):
      with ScopedMonkeyPatch(os.path, 'isfile', lambda x: x == bin_path):
        with ScopedMonkeyPatch(os, 'access', lambda x, y: True):
          path = push_to_device._default_tool_path('simg2img', allow_host=True)
          self.assertEqual(path, bin_path)

    with ScopedMonkeyPatch(os.path, 'isfile', lambda x: True):
      with ScopedMonkeyPatch(os, 'access', lambda x, y: True):
        path = push_to_device._default_tool_path('simg2img', allow_host=True)
        self.assertTrue(path.endswith('/simg2img'))

    with ScopedMonkeyPatch(os, 'environ', {'PATH': '/bin'}):
      with ScopedMonkeyPatch(os.path, 'isfile', lambda x: x.startswith('/bin')):
        with ScopedMonkeyPatch(os, 'access', lambda x, y: True):
          path = push_to_device._default_tool_path('simg2img', allow_host=True)
          self.assertEqual(path, '/bin/simg2img')
          path = push_to_device._default_tool_path('simg2img', allow_host=False)
          self.assertEqual(path, None)

    with ScopedMonkeyPatch(os, 'environ', {}):
      path = push_to_device._default_tool_path('simg2img', allow_host=True)
      self.assertEqual(path, None)

  def test_parse_prebuilt(self):
    # pylint: disable=protected-access
    self.assertEqual(push_to_device._parse_prebuilt('cheets_arm/user/123456'),
                     ('cheets_arm', 'user', '123456'))

    invalid_cases = ['cheets_arm/user', 'cheets_x86/eng/', '', '//',
                     'user/123456/cheets_arm']

    for case in invalid_cases:
      sys_exit = MockSysExit()
      with ScopedMonkeyPatch(sys, 'exit', sys_exit):
        try:
          push_to_device._parse_prebuilt(case)
        except SysExitException:
          pass
      self.assertTrue(sys_exit.called)

  def test_get_remote_device_android_sdk_version(self):
    class MockRemoteProxy(object):
      def __init__(self, assert_command, version_string):
        self._assert_command = assert_command
        self._version_string = version_string

      def set_version_string(self, version_string):
        self._version_string = version_string

      def check_output(self, command):
        self._assert_command(command)
        return self._version_string

    build_version_sdk = 25
    remote_proxy = MockRemoteProxy(
        lambda command: self.assertEqual(
            'grep ^CHROMEOS_ARC_ANDROID_SDK_VERSION= /etc/lsb-release',
            command), 'CHROMEOS_ARC_ANDROID_SDK_VERSION=%s' % build_version_sdk)
    dry_run = True

    # Check that the returned value is 25 if the remote device is running
    # Android SDK 25.
    # pylint: disable=protected-access
    self.assertEqual(push_to_device._get_remote_device_android_sdk_version(
        remote_proxy, not dry_run), build_version_sdk)
    # Check that the returned value is 1 for a dry run.
    self.assertEqual(push_to_device._get_remote_device_android_sdk_version(
        remote_proxy, dry_run), 1)
    # Check that the returned value is None if the version string format is
    # incorrect.
    remote_proxy.set_version_string('foo')
    self.assertEqual(push_to_device._get_remote_device_android_sdk_version(
        remote_proxy, not dry_run), None)

  def test_verify_android_sdk_version(self):
    class MockRemoteProxy(object):
      def __init__(self, assert_command, version_string):
        self._assert_command = assert_command
        self._version_string = version_string

      def check_output(self, command):
        self._assert_command(command)
        return self._version_string

    class MockProvider(push_to_device.BaseProvider):
      def __init__(self, build_version_sdk):
        super(MockProvider, self).__init__()
        self._build_version_sdk = build_version_sdk

      def prepare(self):
        pass

    build_version_sdk = 25
    remote_proxy = MockRemoteProxy(
        lambda command: self.assertEqual(
            'grep ^CHROMEOS_ARC_ANDROID_SDK_VERSION= /etc/lsb-release',
            command), 'CHROMEOS_ARC_ANDROID_SDK_VERSION=%d' % build_version_sdk)
    good_provider = MockProvider(build_version_sdk)
    bad_provider = MockProvider(-1)
    dry_run = True

    # Check that there is no error when when the Android SDK versions match.
    # pylint: disable=protected-access
    push_to_device._verify_android_sdk_version(
        remote_proxy, good_provider, not dry_run)
    # Check that no error is thrown in a dry run.
    push_to_device._verify_android_sdk_version(
        remote_proxy, good_provider, dry_run)

  def test_get_android_key_type_from_cert_key(self):
    self.assertEqual(push_to_device._get_android_key_type_from_cert_key(
        push_to_device._ANDROID_REL_KEY_SIGNATURE_SUBSTRING),
        push_to_device._APK_KEY_RELEASE)
    self.assertEqual(push_to_device._get_android_key_type_from_cert_key(''),
        push_to_device._APK_KEY_DEBUG)

  def test_get_apk_key_from_xml(self):
    self.assertEqual(push_to_device._get_apk_key_from_xml(
        '/dev/null/'), push_to_device._APK_KEY_UNKNOWN)

    with push_to_device.TemporaryDirectory() as tmp_dir:
      package_xml = os.path.join(tmp_dir.name, 'packages.xml')
      with open(package_xml, 'w') as f:
        f.write(
          '<packages>'
          '  <package name="com.google.android.gms">'
          '    <sigs count="1">'
          '      <cert index="12" '
          '          key='
          '             "55b390dd7fdb9418631895d5f759f30112687ff621410c069308a"'
          '          />'
          '    </sigs>'
          '  </package>'
          '</packages>')
      self.assertEqual(push_to_device._get_apk_key_from_xml(
          package_xml), push_to_device._APK_KEY_RELEASE)

    with push_to_device.TemporaryDirectory() as tmp_dir:
      package_xml = os.path.join(tmp_dir.name, 'packages.xml')
      with open(package_xml, 'w') as f:
        f.write(
          '<packages>'
          '  <package name="com.google.android.gms">'
          '    <sigs count="1">'
          '      <cert index="12" />'
          '    </sigs>'
          '  </package>'
          '  <package name="bar.foo">'
          '    <sigs count="1">'
          '      <cert index="12" '
          '          key='
          '             "55b390dd7fdb9418631895d5f759f30112687ff621410c069308a"'
          '          />'
          '    </sigs>'
          '  </package>'
          '</packages>')
      self.assertEqual(push_to_device._get_apk_key_from_xml(
          package_xml), push_to_device._APK_KEY_RELEASE)

    with push_to_device.TemporaryDirectory() as tmp_dir:
      package_xml = os.path.join(tmp_dir.name, 'packages.xml')
      with open(package_xml, 'w') as f:
        f.write(
          '<?xml version=\'1.0\' encoding=\'utf-8\' standalone=\'yes\' ?>'
          '<packages>'
          '  <package name="com.google.android.gms">'
          '    <sigs count="1">'
          '      <cert index="12" />'
          '    </sigs>'
          '  </package>'
          '</packages>')
      self.assertEqual(push_to_device._get_apk_key_from_xml(
          package_xml), push_to_device._APK_KEY_UNKNOWN)

    with push_to_device.TemporaryDirectory() as tmp_dir:
      package_xml = os.path.join(tmp_dir.name, 'packages.xml')
      with open(package_xml, 'w') as f:
        f.write(
          '<?xml version=\'1.0\' encoding=\'utf-8\' standalone=\'yes\' ?>'
          '<packages>'
          '  <package name="com.google.android.gms">'
          '    <sigs count="1">'
          '      <cert index="12" key="1234" />'
          '    </sigs>'
          '  </package>'
          '</packages>')
      self.assertEqual(push_to_device._get_apk_key_from_xml(
          package_xml), push_to_device._APK_KEY_DEBUG)

    with push_to_device.TemporaryDirectory() as tmp_dir:
      package_xml = os.path.join(tmp_dir.name, 'packages.xml')
      with open(package_xml, 'w') as f:
        f.write(
          '<?xml version=\'1.0\' encoding=\'utf-8\' standalone=\'yes\' ?>'
          '<packages>'
          '  <package name="com.google.android.gms">'
          '    <sigs count="1">'
          '      <cert index="12" />'
          '    </sigs>'
          '  </package>'
          '  <package name="bar.foo">'
          '    <sigs count="1">'
          '      <cert index="12" key="5678" />'
          '    </sigs>'
          '  </package>'
          '</packages>')
      self.assertEqual(push_to_device._get_apk_key_from_xml(
          package_xml), push_to_device._APK_KEY_DEBUG)

  def test_update_local_build_prop(self):
    with push_to_device.TemporaryDirectory() as tmp_dir:
      temp_env = os.environ.copy()
      temp_env['TARGET_BUILD_VARIANT'] = 'eng'
      temp_env['TARGET_PRODUCT'] = 'cheets_x86'
      temp_env['OUT'] = tmp_dir.name
      # pylint: disable=protected-access
      with ScopedMonkeyPatch(os, 'environ', temp_env):
        build_prop_path = os.path.join(tmp_dir.name, 'build.prop')
        build_prop_content = 'some.prop=some.value'

        with open(build_prop_path, 'w') as f:
          f.write(build_prop_content)

        # Test adding a new prop
        new_prop = 'some.prop.new=some.value'
        provider = push_to_device.LocalBuildProvider(
            'build_fingerprint',
            # skip_build_prop_update
            False,
            [new_prop],
            # dryrun
            True)
        provider._update_local_build_prop_file(build_prop_path)

        with open(build_prop_path, 'r') as f:
          new_build_prop_content = [line.rstrip() for line in f]
        self.assertTrue(new_prop in new_build_prop_content)

        # Test updating an existing prop
        new_prop = 'some.prop=some.value.new'
        provider = push_to_device.LocalBuildProvider(
            'build_fingerprint',
            # skip_build_prop_update
            False,
            [new_prop],
            # dryrun
            True)
        provider._update_local_build_prop_file(build_prop_path)

        with open(build_prop_path, 'r') as f:
          new_build_prop_content = [line.rstrip() for line in f]
        self.assertTrue(new_prop in new_build_prop_content)
        self.assertFalse(build_prop_content in new_build_prop_content)


if __name__ == '__main__':
  unittest.main()
