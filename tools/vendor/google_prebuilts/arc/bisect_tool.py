#!/usr/bin/python3

# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Tool to automate bisecting Android builds."""

from __future__ import division

import argparse
import json
import logging
import os
import subprocess
import sys

import lib.util
import push_to_device

EPILOG = """Example:
$ %(prog)s --product cheets_x86 --build-variant userdebug 3616080 3616293 samus)
"""

ANDROID_URL_BASE = ('https://android-build.googleplex.com/'
                    'builds/branch/git_%(_BRANCH)s')
BUILD_IDS_BETWEEN_URL_TEMPLATE = (ANDROID_URL_BASE
                                  + '/build-ids/between/%(_END)d/%(_START)d')
BUILD_INFO_URL_TEMPLATE = ANDROID_URL_BASE + '/builds?id=%(_BUILD_ID)s'


def _get_build_ids_between(branch, target_name, start, end):
  """Returns a list of build IDs where the build succeeded.

  Args:
    branch: The Android branch from which to retrieve the builds.
    target_name: Target name of the Android image in question.
        E.g. "cheets_x86-userdebug" or "cheets_arm-user".
    start: The starting point build ID.
    end: The ending point build ID.

  Returns:
    A list of build IDs where the build was successful.
  """
  build_ids = list()
  build_id_range = json.loads(_call_sso_client(
      BUILD_IDS_BETWEEN_URL_TEMPLATE
      % {'_BRANCH': branch, '_START': start, '_END': end}))
  build_id_range = [str(build_id) for build_id in build_id_range['ids']]
  logging.info('Found %d builds in the range.', len(build_id_range))
  logging.info('Extracting successful builds...')
  for build_id in build_id_range:
    logging.debug('Build ID: %s', build_id)
    build = json.loads(_call_sso_client(BUILD_INFO_URL_TEMPLATE
                                        % {'_BRANCH': branch,
                                           '_BUILD_ID': build_id
                                          }
                                       ))
    for target in build[0]['targets']:
      if (target['target']['name'] == target_name and
          target.get('successful')):
        build_ids.append(build_id)
        break
  return sorted(build_ids)


def _call_sso_client(url):
  """Invokes the sso_client command for the given url.

  Args:
    url: The URL to pass into the sso_client command.

  Returns:
    The response of the sso_client in bytes.
  """
  try:
    return lib.util.check_output(
        '/usr/bin/sso_client', '-location', url, '-connect_timeout=100')
  except subprocess.CalledProcessError:
    sys.exit('Failed to make the sso_client call.')


def _parse_args():
  """Parses the arguments."""
  parser = argparse.ArgumentParser(
      formatter_class=argparse.ArgumentDefaultsHelpFormatter,
      description=__doc__, epilog=EPILOG)
  parser.add_argument(
      '--branch', default='nyc-mr1-arc',
      help='The Android branch in question. Example: nyc-mr1-arc')
  parser.add_argument(
      '--product', default=os.environ.get('TARGET_PRODUCT'),
      help='Target product name of the Android image. Example: cheets_x86')
  parser.add_argument(
      '--build-variant', default=os.environ.get('TARGET_BUILD_VARIANT'),
      choices=('eng', 'user', 'userdebug'),
      help='The target build variant of the Android image. Example: userdebug')
  parser.add_argument(
      '--loglevel', default='INFO',
      choices=('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'),
      help='Logging level.')
  parser.add_argument(
      '--dryrun', action='store_true',
      help=('If set True, the Android images will not be pushed to the target '
            'device.'))
  parser.add_argument(
      '--arc-scripts-path', default=os.path.dirname(os.path.realpath(__file__)),
      help='Path to ARC++ scripts')
  parser.add_argument('start', type=int)
  parser.add_argument('end', type=int)
  parser.add_argument('target_device', metavar='target-device', type=str)
  args = parser.parse_args()

  if not args.product:
    sys.exit('Target product has not been defined. Please specify it in the '
             'command line or run the appropriate lunch command in the '
             'Android root directory.')
  if not args.build_variant:
    sys.exit('Build variant has not been defined. Please specify it in the '
             'command line or run the appropriate lunch command in the '
             'Android root directory.')

  return args


class BisectState(object):
  """Class that represents the state of the bisection."""

  def __init__(self, build_ids):
    self.build_ids = build_ids
    self.start_idx = 0
    self.end_idx = len(build_ids) - 1
    self.last_good_build_id = build_ids[self.start_idx]
    self.last_bad_build_id = build_ids[self.end_idx]
    self._update_mid_idx()

  def get_build_id(self, idx):
    """Returns the build id at idx."""
    return self.build_ids[idx]

  def is_done(self):
    return (self.end_idx - self.start_idx) <= 1

  def set_bad(self):
    self.end_idx = self.mid_idx
    self.last_bad_build_id = self.current_build_id
    self._update_mid_idx()

  def set_good(self):
    self.start_idx = self.mid_idx
    self.last_good_build_id = self.current_build_id
    self._update_mid_idx()

  def _update_mid_idx(self):
    self.mid_idx = (self.start_idx + self.end_idx) // 2
    self.current_build_id = self.build_ids[self.mid_idx]

  def __str__(self):
    build_str = 'start: %d mid: %d end: %d\n' % (self.start_idx, self.mid_idx,
                                                 self.end_idx)
    build_str += 'Builds: '
    build_str += ' '.join('[%d]' % b if b == self.current_build_id
                          else b for b in self.build_ids[
                              self.start_idx:self.end_idx])
    return build_str


def main():
  # Set up arguments
  args = _parse_args()
  logging.basicConfig(level=getattr(logging, args.loglevel))

  logging.info('Branch: %s', args.branch)
  logging.info('Product: %s', args.product)
  logging.info('Build variant: %s', args.build_variant)
  logging.info('Target device: %s', args.target_device)

  logging.info('Finding successful builds between %d and %d...', args.start,
               args.end)
  target_name = '%s-%s' % (args.product, args.build_variant)
  build_ids = _get_build_ids_between(args.branch, target_name, args.start,
                                     args.end)

  if len(build_ids) <= 2:
    sys.exit('No builds found to bisect')

  bisect_state = BisectState(build_ids)

  if logging.getLogger().isEnabledFor(logging.DEBUG):
    logging.debug('Builds for bisection: %s',
                  ','.join(str(e) for e in build_ids))
  logging.info('Starting bisection. Start: %s End: %s...',
               bisect_state.get_build_id(bisect_state.start_idx),
               bisect_state.get_build_id(bisect_state.end_idx))

  while not bisect_state.is_done():
    current_build_id = bisect_state.current_build_id
    logging.info('Trying build ID: %s...', current_build_id)
    logging.debug('Bisection state\n%s', bisect_state)

    if not args.dryrun:
      lib.util.check_call(
          os.path.join(args.arc_scripts_path, 'push_to_device.py'),
          # Skip and accept all prompts (i.e. disable rootfs verification)
          '--force',
          '--loglevel',
          '%(_LOGLEVEL)s' % {'_LOGLEVEL': args.loglevel},
          '--use-prebuilt',
          '%(_PRODUCT)s/%(_BUILD_VARIANT)s/%(_BUILD_ID)s'
          % {'_PRODUCT': args.product,
             '_BUILD_VARIANT': args.build_variant,
             '_BUILD_ID': current_build_id
            },
          args.target_device,
          dryrun=args.dryrun
      )

    if push_to_device.boolean_prompt('Did build ID %(_BUILD_ID)s work?'
                                     % {'_BUILD_ID': current_build_id}, False):
      bisect_state.set_good()
    else:
      bisect_state.set_bad()

  logging.info('******FINISHED*****')
  logging.info('Last Good Build: %s', bisect_state.last_good_build_id)
  logging.info('Last Bad Build:  %s', bisect_state.last_bad_build_id)
  return 0

if __name__ == '__main__':
  sys.exit(main())
