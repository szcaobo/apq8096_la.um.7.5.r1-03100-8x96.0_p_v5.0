#!/usr/bin/python3
#
# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import tempfile
import unittest

import arc_commit_message_format


class ArcCommitMessageFormatTest(unittest.TestCase):

  # pylint: disable=protected-access
  def test_is_merge_commit(self):
    # "ARC-Merge-From: " and "Topic: " fields are present
    commit_message = ('Test\nARC-Merge-From: 1234567\n\nTopic: Merge\n'
                      'Bug: None\nTest: Nothing\nChange-Id: 1234')
    self.assertTrue(arc_commit_message_format._is_merge_commit(commit_message))

    # Only "ARC-Merge-From: " field is present
    commit_message = ('Test\nARC-Merge-From: 1234567\n\n'
                      'Bug: None\nTest: Nothing\nChange-Id: 1234')
    self.assertTrue(arc_commit_message_format._is_merge_commit(commit_message))

    # The "ARC-Merge-From: " field is not present
    commit_message = ('Test\nTopic: Merge\n'
                      'Bug: None\nTest: Nothing\nChange-Id: 1234')
    self.assertFalse(arc_commit_message_format._is_merge_commit(commit_message))

  # pylint: disable=protected-access
  def test_contains_valid_topic_fields(self):
    with tempfile.TemporaryFile("w+", prefix='commit_message_') as topics_file:
      topics_file.write('Google\nArc\nChromeOS')

      # "Topic:" is not in commit message
      commit_message = ('Test\nBug: None\nTest: Nothing\nChange-Id: 1234')
      topics_file.seek(0)
      self.assertFalse(arc_commit_message_format._contains_valid_topic_fields(
          commit_message, topics_file))

      # Empty topic value
      commit_message = ('Test\nTopic: \nBug: None\nTest: Nothing\n'
                        'Change-Id: 1234')
      topics_file.seek(0)
      self.assertFalse(arc_commit_message_format._contains_valid_topic_fields(
          commit_message, topics_file))

      # Topic value is invalid
      commit_message = ('Test\nTopic: Foo\n'
                        'Bug: None\nTest: Nothing\nChange-Id: 1234')
      topics_file.seek(0)
      self.assertFalse(arc_commit_message_format._contains_valid_topic_fields(
          commit_message, topics_file))

      # Multiple topic values, where 1 is valid, while the other is invalid
      commit_message = ('Test\nTopic: Google,chrome os\nBug: None\n'
                        'Test: Nothing\nChange-Id: 1234')
      topics_file.seek(0)
      self.assertFalse(arc_commit_message_format._contains_valid_topic_fields(
          commit_message, topics_file))

      # Single topic value is valid
      commit_message = ('Test\nTopic: GooGle\nBug: None\nTest: Nothing\n'
                        'Change-Id: 1234')
      topics_file.seek(0)
      self.assertTrue(arc_commit_message_format._contains_valid_topic_fields(
          commit_message, topics_file))

      # Multiple topic values, all are valid
      commit_message = ('Test\nTopic: Google, chromeos, ARC\nBug: None\n'
                        'Test: Nothing\nChange-Id: 1234')
      topics_file.seek(0)
      self.assertTrue(arc_commit_message_format._contains_valid_topic_fields(
          commit_message, topics_file))

      # Topic value with the wrong case
      commit_message = ('Test\nTopic: GOOGLE\nBug: None\n'
                        'Test: Nothing\nChange-Id: 1234')
      topics_file.seek(0)
      self.assertTrue(arc_commit_message_format._contains_valid_topic_fields(
          commit_message, topics_file))


if __name__ == '__main__':
  unittest.main()
