#!/usr/bin/python3

# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Finds missing commits in one branch based on Change-Id."""

from __future__ import print_function

import argparse
import concurrent.futures
import datetime
import getpass
import logging
import multiprocessing
import os.path
import re
import subprocess
import sys

import lib.util
import lib.manifest_file


_COMMIT_RE = re.compile(r'^commit ', flags=re.MULTILINE)
_CHANGE_ID_RE = re.compile(r'Change-Id: (I[a-f0-9]+)')


def _parse_commit(commit_hash, cwd):
  output = lib.util.check_output('git', 'cat-file', 'commit', commit_hash,
                                 cwd=cwd).split('\n')
  commit = {
      'hash': commit_hash,
      'parent': [],
      'changeid': None,
      'message': ''
  }
  for idx, line in enumerate(output):
    if not line:
      commit['message'] = '\n'.join(output[idx+1:])
      changeid = _CHANGE_ID_RE.findall(commit['message'])
      if changeid:
        # Take the last Change-Id: field.
        commit['changeid'] = changeid[-1]
      break
    key, value = line.split(' ', maxsplit=1)
    if key == 'parent':
      commit[key].append(value)
    else:
      commit[key] = value
  return commit


def _find_non_merge_commits(commit_hash, cwd):
  # Some branches are configured 'Merge if Possible', which causes lots of
  # merge-commits with no Change-Id: label. When one such merge-commit is found,
  # recursively diff both of its parents until we enumerate all non-merge
  # commits that it is merging.
  commit = _parse_commit(commit_hash, cwd)
  if commit['changeid']:
    yield commit
    return
  if len(commit['parent']) != 2:
    logging.warning('Change-Id not found in non-merge commit %s', commit_hash)
    return
  for commit in _find_non_merge_commits_between(commit['parent'][0],
                                                commit['parent'][1], cwd):
    yield commit


def _find_non_merge_commits_between(range_from, range_to, cwd):
  git_log_output = lib.util.check_output('git', 'log', '--first-parent',
                                         '--pretty=format:%H',
                                         '%s..%s' % (range_from, range_to),
                                         cwd=cwd)
  if not git_log_output:
    return
  for commit_hash in git_log_output.split('\n'):
    for commit in _find_non_merge_commits(commit_hash, cwd):
      yield commit


def _get_change_id_mapping(branch, merge_base, cwd):
  change_id_mapping = []
  for commit in _find_non_merge_commits_between(merge_base,
                                                'goog/%s' % branch, cwd):
    assert commit['changeid']
    change_id_mapping.append((commit['changeid'], commit['hash']))
  return change_id_mapping


def _has_missing_commits(args, project_path):
  logging.debug('Processing %s...', project_path)
  if not args.skip_fetch:
    stderr = subprocess.DEVNULL
    if logging.getLogger().isEnabledFor(logging.DEBUG):
      # Allow stderr only in debug mode.
      stderr = None
    lib.util.check_call('git', 'fetch', 'goog', args.source_branch,
                        stderr=stderr, cwd=project_path)
    lib.util.check_call('git', 'fetch', 'goog', args.target_branch,
                        stderr=stderr, cwd=project_path)
  merge_base = lib.util.check_output('git', 'merge-base',
                                     'goog/%s' % args.target_branch,
                                     'goog/%s' % args.source_branch,
                                     cwd=project_path).strip()
  source_commits = _get_change_id_mapping(args.source_branch, merge_base,
                                          project_path)
  target_commits = _get_change_id_mapping(args.target_branch, merge_base,
                                          project_path)
  diffs = (set(change_id for change_id, _ in source_commits) -
           set(change_id for change_id, _ in target_commits))
  if not diffs:
    # Everything is OK.
    return False

  print('Found the %d commits in goog/%s but not in goog/%s for \"%s\"' %
        (len(diffs), args.source_branch, args.target_branch, project_path))
  if args.reverse:
    source_commits.reverse()
  if args.cherry_pick:
    lib.util.check_call('repo', 'start', args.cherry_pick_topic, '.',
                        cwd=project_path)
  for change_id, commit_hash in source_commits:
    if change_id not in diffs:
      continue
    print('http://go/ag/%s' % change_id)
    lib.util.check_call('git', 'show', commit_hash, '--pretty=oneline',
                        '--no-patch', cwd=project_path)
    if args.cherry_pick:
      lib.util.check_call('git', 'cherry-pick', '-x', commit_hash,
                          cwd=project_path)
  return True


def main():
  parser = argparse.ArgumentParser(
      description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument(
      '-v', '--verbose', action='count', default=0, help='Shows more messages.')
  parser.add_argument('--reverse', action='store_true',
                      help='Reverse the list of commits.')
  parser.add_argument('--skip-fetch', action='store_true',
                      help='Skips fetching from upstream.')
  parser.add_argument('--manifest-path', default='.repo/manifests/default.xml',
                      help='Path for the repo manifest file')
  parser.add_argument('--source-branch', default='nyc-mr1-arc',
                      help='Source branch.')
  parser.add_argument('--target-branch', default='nyc-mr1-arc-m58',
                      help='Target branch.')
  parser.add_argument('--cherry-pick', action='store_true',
                      help='Cherry picks all the missing commits. ')
  parser.add_argument('--cherry-pick-topic', type=str,
                      help='The branch name to use for the cherry-picks')
  parser.add_argument('-j', '--jobs', metavar='N', type=int,
                      default=min(20, multiprocessing.cpu_count()),
                      help='Use N parallel tasks. 20 is a good round number.')
  parser.add_argument('project', help='Path of the project to verify')
  args = parser.parse_args()

  logging.basicConfig(
      format='%(levelname)s: %(message)s',
      level=(logging.INFO, logging.DEBUG)[args.verbose])

  if args.cherry_pick:
    if not args.cherry_pick_topic:
      args.cherry_pick_topic = ('%s-cherry_picks-%s-%s' %
                                (args.target_branch,
                                 datetime.datetime.now().date(),
                                 getpass.getuser()))
    logging.info('Using "%s" as the branch name for all cherry-picks.',
                 args.cherry_pick_topic)

  project_paths = []
  if args.project == 'all':
    manifest = lib.manifest_file.ManifestFile(args.manifest_path)
    for project in manifest.projects:
      if project.revision is not None:
        # This is a project with a pinned revision, so it won't have any diffs.
        continue
      if not os.path.isdir(project.path):
        # Some projects are not checked out.
        continue
      if project.clone_depth:
        # Some projects have only a shallow checkout.
        logging.warning('Project %s is shallow, cannot find diffs',
                        project.path)
        continue
      project_paths.append(project.path)
  else:
    project_paths.append(args.project)

  with concurrent.futures.ProcessPoolExecutor(max_workers=args.jobs) as pool:
    # Fetch all git repositories and do a diff on all, just ignore any
    # failure because even if a git repository failed to load that
    # shouldn't mean we should abort looking at other repositories.
    #
    # Note: Ctrl-C doesn't really stop the job, and fixing that
    # properly is an excercise left to the reader.
    async_results = [pool.submit(
        _has_missing_commits, args, project_path)
                     for project_path in project_paths]
    has_diffs = any(res.result() for res in async_results)

  if has_diffs:
    sys.exit(1)

if __name__ == '__main__':
  main()
