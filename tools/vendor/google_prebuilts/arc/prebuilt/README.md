The prebuilt files were built from the Chromium tree.

To rebuild them, in the Chromium checkout do:

    gn gen out/Release --args='target_cpu="x86"'
    ninja -C out/Release minidump-2-core \
        minidump_stackwalk minidump_dump dump_syms
    cp out/Release/{minidump-2-core,minidump_stackwalk,minidump_dump,dump_syms} \
        path/to/prebuilt/x86-linux/

    gn gen out/Release --args='target_cpu="x64"'
    ninja -C out/Release minidump-2-core
    cp out/Release/minidump-2-core \
        path/to/prebuilt/x86-linux/x86_64-linux-gnu-minidump-2-core

    build/linux/sysroot_scripts/install-sysroot.py --arch=arm
    gn gen out/Release --args='target_cpu="arm"'
    ninja -C out/Release minidump-2-core
    cp out/Release/minidump-2-core path/to/prebuilt/armeabi-v7a-linux/usr/bin/
