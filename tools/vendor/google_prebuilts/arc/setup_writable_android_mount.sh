#!/bin/bash

# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if (( $# != 1 )); then
  echo "Usage: $0 <chromebook host name>" >&2
  exit 1
fi

remote="$1"

if ! ssh $remote \
  grep -q ':eng/' /opt/google/containers/android/rootfs/root/default.prop; then
  echo "Cannot setup writable mount for non-eng Android image." >&2
  exit 1
fi

ssh $remote 'mount -o remount,rw /;
             if [ -e /etc/arc-setup-env ]; then
               ARC_SETUP_ENV=/etc/arc-setup-env
             elif [ -e /etc/init/arc-setup-env ]; then
               ARC_SETUP_ENV=/etc/init/arc-setup-env
             else
               echo "ERROR: Could not find /etc/arc-setup-env or /etc/init/arc-setup-env"
               return -1
             fi
             if grep --quiet "WRITABLE_MOUNT=0" ${ARC_SETUP_ENV}; then
               sed -i -e s/WRITABLE_MOUNT=0/WRITABLE_MOUNT=1/ ${ARC_SETUP_ENV};
               reboot;
             else
               echo "Skipping. Android mount is already writable"
             fi'
